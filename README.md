# BitPrism

The ZX Spectrum Emulator with macOS Metal Integration

BitPrism is a unique ZX Spectrum emulator that combines the nostalgic charm of an 8-bit machine with modern advancements. Unlike other emulators, BitPrism is not an attempt at code-completeness but introduces innovative features to ZX Basic.

Over the last few years, I've been working on a ZX Spectrum emulator for macOS, driven by a desire to scratch an old itch and learn more about Swift. This project was a ground-up endeavor, involving the creation of Z80 emulation from scratch, drawing inspiration from existing emulators whenever multiple approaches seemed promising.

The primary objective wasn't to achieve a timing-perfect emulator, which allowed me to focus on other areas of interest. Consequently, I integrated a Metal renderer into the emulator as its screen output, opening up a world of new possibilities. An intriguing idea emerged from this process: What if we could harness the power of ZX Basic as a simple scripting language to manipulate the renderer?

BitPrism faithfully runs the original ROM and can load and execute many original games in SNA and TZX formats. It does not support most TZX files containing fast loader data blocks due to the sheer variety of TZX data blocks and a lack of documentation for many of them. Running slightly faster than the original ZX Spectrum, BitPrism operates at approximately 4-5MHz instead of the original 3.5MHz, enhancing gameplay in most cases.

Sound is currently not supported, as the timing intricacies of my Z80 emulation made it a challenging task. Additionally, a few games may crash due to the lack of implementation for some undocumented opcodes. Nevertheless, most games work, and the Spectrum ROM runs as expected.

What truly sets BitPrism apart is its integration of ZX Basic with the Apple Metal renderer that drives the screen. Through various IO ports, the emulator can manipulate shader settings, introduce simple 3D objects, move them around, and even apply vectors for animations. In essence, ZX Basic has learned an array of 21st-century tricks!

BitPrism is an ongoing project, and I will continue to make changes and add new features. However, the project is now "good enough" to share.

### License

BitPrism code and binary are distributed under Attribution-NonCommercial-ShareAlike 4.0 [(CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

The Sinclair Spectrum 48K ROM is (C) Sky In-Home Service Limited (née Amstrad) and is included for emulation purposes as traditionally accepted by these rights holders.

### Download

You can find a Mac OSX binary in the release folder. Alternatively you can download the source and attempt your own build with Xcode.

## Z80 Emulation

Once the simplest possible CPU functionality was implemented, the author ran the emulation against the ZX Spectrum ROM and implemented missing opcodes as they occurred. Once the ROM ran without errors, the same technique was used against a variety of Spectrum games. 

Once most games ran, whatever documented instructions were still missing were implemented. Many undocumented Z80 opcodes were also implemented as they were being used in games, but some undocumented opcodes remain missing, since no software could be found that ever used them.

In this way the Z80 opcodes were re-implemented by the author, but naturally research in existing Z80 emulation code influenced many of the decisions taken in how to build the CPU architecture. 

The author rests on the shoulders of giants. For an incomplete list of online resources, see below.

## Keyboard

BitPrism uses the standard Spectrum method of keyboard entry with individual keys triggering Basic commands. The following special Spectrum keys are mapped to Mac keys:

shift -> CAPS SHIFT\
option -> SYMBOL SHIFT

In addition, the Mac's cursor keys map to the Spectrum's cursor keys. In addition, delete and the dedicated punctuation keys map to the correct Spectrum equivalents.

A useful key that has been mapped is the Mac's tab key, which maps to CAPS-1 to switch the editor to EDIT mode for the currently marked line in the BASIC listing.

## Loading and Saving Spectrum Files

The emulator supports .SNA and .TZX files, although for .TZX only files without any kind of custom loader code, so no fastloader games. It does allow to save ZX Basic programs as .TZX and load them again.

### SNA

At any time while the emulator is running, select Fastload (SNA/TZX) from the Emulator menu and pick a .SNA. It will completely overwrite the RAM of the emulator and execute the code in the SNA.

SNA files can not be saved by the emulator.

### TZX

Reset the emulated Spectrum and in the ZX Basic press "J" followed by Option-"P" twice. You should see LOAD "" on the screen. Hit Return. 

Fast Load: \
Select "Fastload (SNA/TZX)" from the emulator menu and pick a .TZX file. The Spectrum will load the files in sequence as if from a cassette tape, but the emulator will override the ROM loader routine and copy the data directly into RAM.

Slow Load: \
Select "Cassette Load (TZX)". The emulator will feed the data from the TZX file to the EAR input of the emulated computer and any data loader code can load the data at its own pace. The cassette speed is slower than on a real Spectrum due to the way BitPrism runs its internal timers. I will probably revisit this in the future to get the cassette speed up to about the same as a real Spectrum.

Saving a ZX Basic program as a TZX file: \
Press "S" and then give the program a short name in double quotes. Hit Return, and a file dialog will open to allow you to save the file to a location on your computer.

## ZX Basic to Metal Bridge for 3D Graphics

The emulator intercepts a number of OUT addresses and will use that data together with the BASIC PLOT command to render 3D voxels in front of the emulated Spectrum screen - in fact, the Spectrum screen itself is a 3D object with the pixel output from the ULA as texture. 

Here is a simple example of drawing a red voxel in front of the Spectrum screen:
```
10 OUT 157,2
20 OUT 155,100
30 PLOT 127,80
```
After running this program, you can select "Perspective Left" from the View menu to see that that voxel is in fact in front of the screen output.
The View menu also has an option to reset the Spectrum screen and to remove all 3D voxels.

A note about the OUT command: \
The ZX Basic OUT command originally only supports 8-bit output to a single IO port. BitPrism overrides this behavior and the ZX Basic in the emulator allows 16-bit values on OUT for the IO addresses listed below.

**OUT Ports**
```
139:  0 -> screen in view; 1 -> screen out of view
141:  0 -> shader with flat lighting; 1 -> shader with realistic lighting
143:  camera position x, default is 32768
145:  camera position y, default is 32768
147:  camera position z, default is 25600
149:  0 -> clears all voxels
151:  camera angle along X axis, 32768 is centered over the screen rectangle
153:  camera angle along Y axis, 32768 is centered over the screen rectangle
155:  a value >0 primes the voxel PLOT and sets the z-axis value
157:  0...15 for voxel color
159:  0 (allow) or 1 to block PLOT on ULA canvas (default is 0)
161:  voxel width (default is 3)
163:  voxel height (default is 3)
165:  voxel depth (default is 3)
169:  voxel ID, the next PLOT will change the values for this voxel instead of creating a new one
171:  voxel vector x (0-255, 128 is no movement)
173:  voxel vector y (0-255, 128 is no movement)
175:  voxel vector z (0-255, 128 is no movement)
177:  voxel countdown (0 is no countdown)
179:  vector speed (default is 10, 0 inhibits all vectors, <10 slow down, >10 speed up)
181:  voxel countdown behavior (0: delete; 1-3: reverse x,y,z vector)
191:  1 -> extended pixel canvas 320x256 with 16 colors per pixel (use 157 to set color)
```
**IN Ports**
```
167:  returns the ID of the last PLOTed voxel
```
### ZX Basic helper library

The snapshots folder in the BitPrism source folder contains a number of BASIC examples in .tzx format. There is one file "init.tzx" that contains a simple starter kit to work with the Metal Bridge. You can add your own BASIC code starting at line number 200. At the end of your 3D voxel code you can call GOSUB 100 to return the Spectrum screen to its default.

## Extended PLOT Canvas

Since the Metal Renderer uses a 320x256 pixel canvas with 15 colors per individual pixel, it is possible for the emulator to address this canvas. If OUT 191,1 is called, the ZX Basic PLOT command will accept coordinates for the full 320x256 canvas. The color for canvas pixels can be set with OUT 157,<color> with values from 0 to 15, matching the standard Spectrum color space. 

Since the Spectrum's screen memory is not altered in this mode, the graphics will be overwritten and destroyed if a PRINT command or standard PLOT is executed. Also, BORDER changes will destroy any pixels outside of the standard Spectrum screen area. Graphics generated in this way can not be saved with a SCREEN$ command.

## Known Emulator Limitations

- Not cycle accurate. This emulation runs slightly faster than a real Z80 machine
- No sound. Since the emulation is not cycle accurate I didn't see a point (yet) to attempt sound
- Not all undocumented Z80 opcodes are implemented
- The keyboard emulation is not fully compatible. There are a few games where key clicks are not detected, but I have not been able yet to find the root cause. One of the games is Elite, where there is no way to start the game on the first Y/N question. Bummer.

## Screenshots

![Mandelbrot rendered on extended 320x256 canvas](screenshots/BitPrism-extended-mandelbrot-1.png)
Mandelbrot rendered on extended 320x256 canvas

![BASIC listing overlays extended canvas with remnant of Mandelbrot set in the border region of the screen](screenshots/BitPrism-extended-mandelbrot-2.png)
BASIC listing overlays extended canvas with remnant of Mandelbrot set in the border region of the screen

## Videos

[![](http://img.youtube.com/vi/baf_srjSzkE/0.jpg)](https://www.youtube.com/embed/baf_srjSzkE?list=PLJgmWWHufVoxISKYF689xYlmnf-pK0Vwg "BitPrism redsine2 Test")

redsine2 Test

[![](http://img.youtube.com/vi/hyd-RRUJH9k/0.jpg)](https://www.youtube.com/embed/hyd-RRUJH9k?list=PLJgmWWHufVoxISKYF689xYlmnf-pK0Vwg "BitPrism Island Test")

BitPrism Island Test

[![](http://img.youtube.com/vi/AaL4Xs_c3Mk/0.jpg)](https://www.youtube.com/embed/AaL4Xs_c3Mk?list=PLJgmWWHufVoxISKYF689xYlmnf-pK0Vwg "BitPrism Vector Test")

Vector Test

## Online Resources

Invaluable online resources for the Z80 and the Spectrum hardware:
- [ClrHome's Z80 Opcode Table](https://clrhome.org/table/)
- [Zilog's original Z80 Book - beware of the typos](http://www.zilog.com/docs/z80/um0080.pdf)
- [Rodney Zaks' classic "Programming the Z80"](https://sam.speccy.cz/asm/prog-z80_2rd-ed.pdf)
- [Spectrum Next's table of Spectrum ports](https://wiki.specnext.dev/Board_feature_control)
- [The Complete Spectrum ROM Disassembly](https://skoolkid.github.io/rom/index.html)
- [Sinclair ZX Spectrum Character Set](http://www.geocities.ws/peterochocki/computers/sinclair/specchar.html)
- [World of Spectrum Hardware Reference](https://worldofspectrum.org/faq/reference/48kreference.htm)
- [L Break Into Program's Spectrum Memory Maps](http://www.breakintoprogram.co.uk/hardware/computers/zx-spectrum/memory-map)
- [TZX Format Specification](https://k1.spdns.de/Develop/Projects/zasm/Info/TZX%20format.html)
- [Spectrum TZX Format Details from ProblemKaputt](https://problemkaputt.de/zxdocs.htm#spectrumcassettetzxformat)
- [TAP Format Specification](https://sinclair.wiki.zxnet.co.uk/wiki/TAP_format)
- [Spectrum Tape Load Routine Details from shred.zone](https://shred.zone/cilla/page/440/r-tape-loading-error.html)
- [Spectrum Tape Load Routine Details from SoftSpectrum](https://softspectrum48.weebly.com/notes/tape-loading-routines)

Many, many others were also used, but these were the most visited resources during the development of this emulator.

