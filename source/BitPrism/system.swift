//
//  system.swift
//  OSXZX
//
//  Created by Thomas Sturm on 1/9/22.
//  Distributed under Attribution-NonCommercial-ShareAlike 4.0 (CC BY-NC-SA 4.0)
//

import Foundation

public final class System {
 
    func fetch() -> Byte {
        let tmp = mem[Int(regPC)]
        regPC += 1
        return tmp
    }

    func fetchWord() -> Word {
        var tmp = Int(mem[Int(regPC)])
        regPC += 1
        let hi = Int(mem[Int(regPC)])
        regPC += 1
        tmp = tmp + (hi * 256)
        return UInt16(tmp)
    }

    func fetchFrom(adr : Word) -> Byte {
        return UInt8(mem[Int(adr)])
    }

    func fetchWordFrom(adr : Word) -> Word {
        return UInt16(Int(mem[Int(adr)]) + Int(mem[Int(adr+1)])*256)
    }

    func setMem(adr: Word, val: Byte) {
        if (adr >= 0x4000) {    // prevent write into ROM
            mem[Int(adr)] = val
            if (adr >= 16834  &&  adr < 23296 ){
                screenModified = true
                if (adr < 18432 || (adr >= 22978 && adr < 22978+32*8)){
                    screenSection0 = true
                }
                if ((adr >= 18432  &&  adr < 20480) || (adr >= 22978+32*8 && adr < 22978+32*16)){
                    screenSection1 = true
                }
                if ((adr >= 20480 && adr < 22978) || (adr >= 22978+32*16 && adr < 22978+32*24)){
                    screenSection2 = true
                }
            }
        }
    }
    
    func setMemWord(adr: Word, val: Word) {
        if (adr >= 0x4000) {    // prevent write into ROM
            mem[Int(adr)] = UInt8(val & 0x00ff)
            mem[Int(adr+1)] = UInt8((val & 0xff00) >> 8)
            if (adr >= 16834  &&  adr < 23296 ){
                screenModified = true
                if (adr < 18432 || (adr >= 22978 && adr < 22978+32*8)){
                    screenSection0 = true
                }
                if ((adr >= 18432  &&  adr < 20480) || (adr >= 22978+32*8 && adr < 22978+32*16)){
                    screenSection1 = true
                }
                if ((adr >= 20480 && adr < 22978) || (adr >= 22978+32*16 && adr < 22978+32*24)){
                    screenSection2 = true
                }
            }
        }
    }
    
    func setIOPort(adr: Word, val: Byte) {
        if (adr != 0x00FE) {            // prevent ULA writes from messing with our keyboard matrix catch-all
            IOPorts[Int(adr)] = val
        }

        if (adr == 0x008B) {            // OUT to port 139 (0x8B) screen in view 0 or 1
            if (val == 0) {
                metaScreenPosY = 0
            } else {
                metaScreenPosY = 100
            }
        }

        if (adr == 0x008D) {            // OUT to port 141 (0x8D) shader type 0 or 1
            metaShaderType = Int(val)
        }

        if (adr == 0x008F) {            // OUT to port 143 (0x8F) camera position x - 16 bit call
            metaCameraPosX = Int(out16bit)
        }

        if (adr == 0x0091) {            // OUT to port 145 (0x91) camera position y - 16 bit call
            metaCameraPosY = Int(out16bit)
        }

        if (adr == 0x0093) {            // OUT to port 147 (0x93) camera position z - 16 bit call
            metaCameraPosZ = Int(out16bit)
        }

        if (adr == 0x0095) {            // OUT to port 149 (0x95) with value > 0 clears all voxels
            metaClearVoxels = Int(val)
        }

        if (adr == 0x0097) {            // OUT to port 151 (0x97) changes angle of camera along X axis
            metaScreenAngleX = Int(out16bit)
            metaScreenAngleXTarget = metaScreenAngleX
        }

        if (adr == 0x0099) {            // OUT to port 153 (0x99) changes angle of camera along Y axis
            metaScreenAngleY = Int(out16bit)
            metaScreenAngleYTarget = metaScreenAngleY
        }

        if (adr == 0x009b) {            // OUT to port 155 (0x9B) with a value > 0 primes the PLOT redirect and sets the z axis for the voxels
            metaPlotDepth = Int(val)
            if (val == 0) {
                metaNoZXPlot = 0        // if we set depth to 0, we enable ULA canvas PLOT
            }
        }

        if (adr == 0x009d) {            // OUT to port 157 (0x9D) with a value 0...15 for voxel color
            var tmp = Int(val)
            if (tmp > 15) {
                tmp = 0
            }
            metaPlotColor = tmp
        }

        if (adr == 0x009f) {            // OUT to port 159 (0x9F) with a value 0 or 1 to block ZX PLOT
            var tmp = Int(val)
            if (tmp > 1) {
                tmp = 1
            }
            metaNoZXPlot = tmp
        }
        
        if (adr == 0x00a1) {            // OUT to port 161 (0xA1) voxel default width
            metaDefaultWidth = Int(val)
        }
        if (adr == 0x00a3) {            // OUT to port 163 (0xA3) voxel default height
            metaDefaultHeight = Int(val)
        }
        if (adr == 0x00a5) {            // OUT to port 165 (0xA5) voxel default depth
            metaDefaultDepth = Int(val)
        }

        // 167 is return port for voxel ID in PLOT command

        if (adr == 0x00a9) {            // OUT to port 169 (0xA9) signal next PLOT is a voxel change
            metaChangeVoxel = Int(val)
        }
        
        if (adr == 0x00ab) {            // OUT to port 171 (0xAB) voxel vector x
            metaDefaultVectorX = (Int(val)-128)
        }
        if (adr == 0x00ad) {            // OUT to port 173 (0xAD) voxel vector y
            metaDefaultVectorY = (Int(val)-128)
        }
        if (adr == 0x00af) {            // OUT to port 175 (0xAF) voxel vector z
            metaDefaultVectorZ = (Int(val)-128)
        }
        if (adr == 0x00b1) {            // OUT to port 177 (0xB1) voxel countdown
            metaVoxelCountdown = Int(val)
        }
        if (adr == 0x00b3) {            // OUT to port 179 (0xB3) voxel global vector hold
            metaVectorHold = Int(val)
        }
        if (adr == 0x00b5) {            // OUT to port 181 (0xB5) voxel countdown behavior
            metaVoxelBehavior = Int(val)
        }

        if (adr == 0x00bf) {            // OUT to port 191 (0x) plot canvas type
            metaPlotCanvas = Int(val)
        }
        
    }
    
    
    // AntAttack:
    // IN A, (0x7FFB)
    // IN A, (0x7FFE)
    
    func getIOPort(adr: Word) -> Byte {
        if (adr & 0x0001 == 0) {    // somebody's querying the keyboard
            var keyMatrixCode: Byte = 0xff  // output defaults to FF, so no key pressed (lower 5 bits), bit 5 & 7 are always 1 and bit 6 is EAR input
            // we check the key matrix based on the upper byte in the request address; the IOPorts for each of the individual key half-rows are written to by manageKeyboard()
            if (adr & 0x0100 == 0) {
                keyMatrixCode = keyMatrixCode & IOPorts[0xFEFE]
            }
            if (adr & 0x0200 == 0) {
                keyMatrixCode = keyMatrixCode & IOPorts[0xFDFE]
            }
            if (adr & 0x0400 == 0) {
                keyMatrixCode = keyMatrixCode & IOPorts[0xFBFE]
            }
            if (adr & 0x0800 == 0) {
                keyMatrixCode = keyMatrixCode & IOPorts[0xF7FE]
            }
            if (adr & 0x1000 == 0) {
                keyMatrixCode = keyMatrixCode & IOPorts[0xEFFE]
            }
            if (adr & 0x2000 == 0) {
                keyMatrixCode = keyMatrixCode & IOPorts[0xDFFE]
            }
            if (adr & 0x4000 == 0) {
                keyMatrixCode = keyMatrixCode & IOPorts[0xBFFE]
            }
            if (adr & 0x8000 == 0) {
                keyMatrixCode = keyMatrixCode & IOPorts[0x7FFE]
            }
            return keyMatrixCode
        } else {
            return IOPorts[Int(adr)]
        }
    }
    

}
