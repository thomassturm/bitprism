//
//  z80-opcodes.swift
//  OSXZX
//
//  Created by Thomas Sturm on 1/10/22.
//  Distributed under Attribution-NonCommercial-ShareAlike 4.0 (CC BY-NC-SA 4.0)
//

import Foundation


struct aModeReg {
        var value: Byte
        var name: String

        init(value: Byte, name: String) {
            self.value = value
            self.name = name
        }
}

struct dblReg {
        var value: Word
        var name: String

        init(value: Word, name: String) {
            self.value = value
            self.name = name
        }
}


public final class Z80 {
    
    func rollOver(val: Int) -> UInt16 {
        return UInt16(val & 0xFFFF)
    }

    
    func rollOverUp(val: Int) -> UInt16 {
        return UInt16(val & 0xFFFF)
    }

    
    // 0x38 = 00111000 -> ttttftfff = f
    
    func Parity(val : uint16) -> Bool {
        var parity = true;
        var tempVal = val;
        while (tempVal > 0) {
            if ((tempVal & 1) == 1) {parity = !parity}
            tempVal = tempVal >> 1
        }
        return parity
    }

    
    func getAddressModeReg(code: Byte) -> aModeReg {
        var aMode = aModeReg(value: regA, name: "A")
        let regCode = code & 0x07
        switch (regCode) {
        case 0:
            aMode.value = regB
            aMode.name = "B"
        case 1:
            aMode.value = regC
            aMode.name = "C"
        case 2:
            aMode.value = regD
            aMode.name = "D"
        case 3:
            aMode.value = regE
            aMode.name = "E"
        case 4:
            aMode.value = regH
            aMode.name = "H"
        case 5:
            aMode.value = regL
            aMode.name = "L"
        default:
            aMode.value = regA
            aMode.name = "A"
        }
        return aMode
    }

    
    func getUndocumentedAddressModeReg(code: Byte, xory: String) -> aModeReg {
        var aMode = aModeReg(value: regA, name: "A")
        let regCode = code & 0x07
        switch (regCode) {
        case 0:
            aMode.value = regB
            aMode.name = "B"
        case 1:
            aMode.value = regC
            aMode.name = "C"
        case 2:
            aMode.value = regD
            aMode.name = "D"
        case 3:
            aMode.value = regE
            aMode.name = "E"
        case 4:
            if (xory == "x") {
                aMode.value = UInt8(regIX >> 8)
                aMode.name = "IX(h)"
            } else {
                aMode.value = UInt8(regIY >> 8)
                aMode.name = "IY(h)"
            }
        case 5:
            if (xory == "x") {
                aMode.value = UInt8(regIX & 0x00ff)
                aMode.name = "IX(l)"
            } else {
                aMode.value = UInt8(regIY & 0x00ff)
                aMode.name = "IY(l)"
            }
        default:
            aMode.value = regA
            aMode.name = "A"
        }
        return aMode
    }

    
    func setRegByte(name: String, value: Byte) {
        switch (name) {
        case "B":
            regB = value
        case "C":
            regC = value
        case "D":
            regD = value
        case "E":
            regE = value
        case "H":
            regH = value
        case "L":
            regL = value
        default:
            regA = value
        }
    }
    
    
    func getDoubleReg(regIndex: Byte) -> dblReg {
        var reg = dblReg(value: getHL(), name: "HL")
        switch (regIndex) {
        case 0:
            reg.value = getBC()
            reg.name = "BC"
        case 1:
            reg.value = getDE()
            reg.name = "DE"
        case 2:
            reg.value = getHL()
            reg.name = "HL"
        default:
            reg.value = regSP
            reg.name = "SP"
        }
        return reg
    }
    

    func addOffset(adr: Word, offset: Byte) -> Word {
        var newAdr :Word = 0
        var hi :Byte = UInt8(adr >> 8)
        var lo :Byte = UInt8(adr & 0x00ff)
        let loTmp = lo
        lo = lo &+ offset
        if (offset > 0x7F && lo > loTmp) {
            hi = hi &- 1
        }
        if (offset < 0x80 && lo < loTmp) {
            hi = hi &+ 1
        }
        newAdr = (UInt16(hi) << 8) + UInt16(lo)
        return newAdr
    }
    

    func setBit(value: Byte, bit: Int)-> Byte {
        return UInt8(value | UInt8(1 << bit))
    }


    func resBit(value: Byte, bit: Int)-> Byte {
//        return UInt8(value & UInt8((1 << bit) ^ 0xFF))
        return UInt8(value & ~UInt8((1 << bit)))
    }
    

//    S is set if result is negative; otherwise, it is reset.
//    Z is set if result is 0; otherwise, it is reset.
//    H is set if carry from bit 3; otherwise, it is reset.
//    P/V is set if r was 7Fh before operation; otherwise, it is reset
//    N is reset.
//    C is not affected.
// SZXHXPNC, preserve 00101001 -> 0x29
    func doINC(s : Byte) -> Byte {
        var res = s
        res = res &+ 1
        // deal with flags
        var f = regF & 0x29 // erase all relevant bits
        if ((res & 0x80) == 0x80) {f |= UInt8(flagS)}
        if (res == 0) {f |= UInt8(flagZ)}
        if ((s & 0x0F) == 0x0f) {f |= UInt8(flagH)}
        if (s == 0x7F) {f |= UInt8(flagPV)}
        regF = f
        return res
    }

    
    func doDEC(s : Byte) -> Byte {
        var res = s
        res = res &- 1
        // deal with flags
        var f = regF & 0x29 // erase all relevant bits
        if ((res & 0x80) > 0) {f |= UInt8(flagS)}
        if (res == 0) {f |= UInt8(flagZ)}
        if ((s & 0x0F) == 0x00) {f |= UInt8(flagH)}
//        if (Parity(val: uint16(res))) {f |= UInt8(flagPV)}
        if (s & 0x80 == 1 && res & 0x80 == 0) {f |= UInt8(flagPV)}
        f |= UInt8(flagN)
        regF = f
        return res
    }

    
    func doAND(s : Byte) {
        regA = regA & s
        // deal with flags
        let res = regA
        var f = regF & 0x28 // erase all relevant bits 00101000 & sz_h_pnc
        if ((res & 0x80) > 0) {f |= UInt8(flagS)}
        if (res == 0) {f |= UInt8(flagZ)}
        f |= UInt8(flagH)
        if (Parity(val: uint16(res))) {f |= UInt8(flagPV)}
        regF = f
    }

    
    func doOR(s : Byte) {
        regA = regA | s
        // deal with flags
        let res = regA
        var f = regF & 0x28 // erase all relevant bits
        if ((res & 0x80) > 0) {f |= UInt8(flagS)}
        if (res == 0) {f |= UInt8(flagZ)}
        if (Parity(val: uint16(res))) {f |= UInt8(flagPV)}
        regF = f
    }

    
    func doXOR(s : Byte) {
        regA = regA ^ s
        // deal with flags
        let res = regA
        var f = regF & 0x28 // erase all relevant bits
        if ((res & 0x80) > 0) {f |= UInt8(flagS)}
        if (res == 0) {f |= UInt8(flagZ)}
        if (Parity(val: uint16(res))) {f |= UInt8(flagPV)}
        regF = f
    }

    
    func doCP(s : Byte) {
        let oldA = regA
        var res = Int(regA) - Int(s)
        var f = regF & 0x28 // erase all relevant bits
        if (res < 0) {
            res = res & 0x00FF
            f |= UInt8(flagC)
        }
        // deal with flags on a 8 bit result
        if ((res & 0x80) > 0) {f |= UInt8(flagS)}
        if (res == 0) {f |= UInt8(flagZ)}
        if ((res & 0x0F) > (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been reduced
        if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
        f |= UInt8(flagN)
        regF = f
    }

    
    
    
    // ==================================================================================
    // ==================================================================================
    // ==================================================================================
    let system = System()

    func opcode() -> Int {

        let op = system.fetch()   // main instruction fetch
        
        var tStates : Int = 0

        // check for LD r1,r2 - 01rr rrrr
        // exc   01xx x110
        // exc   0111 0xxx
        // 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F
        // 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x57, 0x58, 0x59, 0x5A, 0x5B, 0x5C, 0x5D, 0x5E, 0x5F
        // 0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x67, 0x68, 0x69, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E, 0x6F
        // 0x78, 0x79, 0x7A, 0x7B, 0x7C, 0x7D, 0x7E, 0x7F
        if ((op & 0xc0) == 0x40  &&  (op & 0xC7) != 0x46  &&  (op & 0xF8) != 0x70  &&  tStates == 0) {
            let reg2 = getAddressModeReg(code: op)
            let tmp = op >> 3
            let reg1 = getAddressModeReg(code: tmp)
            setRegByte(name: reg1.name, value: reg2.value)
            setThisOpcode(opc: String(op.hex) + " LD " + reg1.name + "," + reg2.name, target: 0x0000)
            tStates = 4
        }
        
      
        // ############################################################################
//        if (tStates == 0) { // nothing got executed yet
        if (tStates == 0 && !emulationStop) {       // tStates==0 check here prevents the opcode parser from running twice if the timer is faster than the parser
            switch(op) {   // begin of master switch statement

//            case 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x57, 0x58, 0x59, 0x5A, 0x5B, 0x5C, 0x5D, 0x5E, 0x5F, 0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x67, 0x68, 0x69, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E, 0x6F, 0x78, 0x79, 0x7A, 0x7B, 0x7C, 0x7D, 0x7E, 0x7F:
//                let reg2 = getAddressModeReg(code: op)
//                let tmp = op >> 3
//                let reg1 = getAddressModeReg(code: tmp)
//                setRegByte(name: reg1.name, value: reg2.value)
//                setThisOpcode(opc: String(op.hex) + " LD " + reg1.name + "," + reg2.name, target: 0x0000)
//                tStates = 4
                
                
            case 0x00:
                setThisOpcode(opc: String(op.hex) + " NOP", target: 0x0000)
                tStates = 4


            case 0x01, 0x11, 0x21, 0x31:
                let rg = UInt8(op & 0x30) >> 4
                let dblReg = getDoubleReg(regIndex: rg)
                let tmp = system.fetchWord()
                switch (dblReg.name) {
                case "BC":
                    setBC(newValue: tmp)
                case "DE":
                    setDE(newValue: tmp)
                case "HL":
                    setHL(newValue: tmp)
                default:
                    regSP = tmp
                }
                setThisOpcode(opc: String(op.hex) + " LD " + dblReg.name + ",$" + String(tmp.hex), target: 0x0000)
                tStates = 10

                
            case 0x02:
                let adr = getBC()
                system.setMem(adr: adr, val: regA)
                setThisOpcode(opc: String(op.hex) + " LD (BC), A", target: 0x0000)
                tStates = 7


            case 0x03, 0x13, 0x23, 0x33:
                let rg = UInt8(op & 0x30) >> 4
                let dblReg = getDoubleReg(regIndex: rg)
                switch (dblReg.name) {
                case "BC":
                    setBC(newValue: rollOverUp(val: Int(getBC())+1))
                case "DE":
                    setDE(newValue: rollOverUp(val: Int(getDE())+1))
                case "HL":
                    setHL(newValue: rollOverUp(val: Int(getHL())+1))
                default:
                    regSP = rollOverUp(val: Int(regSP)+1)
                }
                setThisOpcode(opc: String(op.hex) + " INC " + dblReg.name, target: 0x0000)
                tStates = 6

                
            case 0x04, 0x0C, 0x14, 0x1C, 0x24, 0x2C, 0x3C:
                // look for 00xxx000
                let aMode = getAddressModeReg(code: (op >> 3))
                let res = doINC(s: aMode.value)
                setRegByte(name: aMode.name, value: res)
                setThisOpcode(opc: String(op.hex) + " INC " + aMode.name, target: 0x0000)
                tStates = 4

                
            case 0x05, 0x0D, 0x15, 0x1D, 0x25, 0x2D, 0x3D:
                let aMode = getAddressModeReg(code: (op >> 3))
                let res = doDEC(s: aMode.value)
                setRegByte(name: aMode.name, value: res)
                setThisOpcode(opc: String(op.hex) + " DEC " + aMode.name, target: 0x0000)
                tStates = 4

                
            case 0x06, 0x0E, 0x16, 0x1E, 0x26, 0x2E, 0x3E:
                let tmp = op >> 3
                let reg1 = getAddressModeReg(code: tmp)
                let dirData = system.fetch()
                setRegByte(name: reg1.name, value: dirData)
                setThisOpcode(opc: String(op.hex) + " LD " + reg1.name + ",$" + String(dirData.hex), target: 0x0000)
                tStates = 7

            //    SZXHXPNC
            case 0x07:
                var oldtmp: UInt8
                oldtmp = regA
                regA = regA << 1
                regA |= ((oldtmp & 0x80) >> 7) // bit 7 moves into bit 0
                var f = regF & 0xEC // erase all relevant bits
                f |= ((oldtmp & 0x80) >> 7)   // bit 7 sets carry flag
                regF = f
                setThisOpcode(opc: String(op.hex) + " RLCA", target: 0x0000)
                tStates = 4

                
            case 0x08:
                let tmpA = regA
                let tmpF = regF
                regA = regAshadow
                regF = regFshadow
                regAshadow = tmpA
                regFshadow = tmpF
                setThisOpcode(opc: String(op.hex) + " EX AF, AF'", target: 0x0000)
                tStates = 4

                
            case 0x09, 0x19, 0x29, 0x39:
                let rg = UInt8(op & 0x30) >> 4
                let dblReg = getDoubleReg(regIndex: rg)
                let oldHL = getHL()
                var res = Int(oldHL)
                switch (dblReg.name) {
                case "BC":
                    res = res + Int(getBC())
                case "DE":
                    res = res + Int(getDE())
                case "HL":
                    res = res + Int(oldHL)
                default:
                    res = res + Int(regSP)
                }
                var f = regF & 0xEC // erase all relevant bits
                if (res > 0xFFFF) {
                    res = res & 0xFFFF
                    f |= UInt8(flagC)
                }
                setHL(newValue: UInt16(res))
                // deal with flags on a 16 bit result
                if ((res & 0x0FFF) < (oldHL & 0x0FFF)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
                regF = f
                setThisOpcode(opc: String(op.hex) + " ADD HL, " + dblReg.name, target: 0x0000)
                tStates = 11

                
            case 0x0F:
                let carryFromRR = regA & 0x01
                var res = regA >> 1
                var f = regF & 0xEC // erase all relevant bits
                if ( carryFromRR == 1) {
                    res |= 0x80
                    f |= UInt8(flagC)
                }
                regA = UInt8(res)
                regF = f
                setThisOpcode(opc: String(op.hex) + " RRCA", target: 0x0000)
                tStates = 4

                
            case 0x0A:
                let adr = getBC()
                regA = system.fetchFrom(adr: adr)
                setThisOpcode(opc: String(op.hex) + " LD A, (BC)", target: 0x0000)
                tStates = 7


            case 0x10:
                let offset = system.fetch()
                var newPC = regPC
                newPC = addOffset(adr: newPC, offset: offset)
                regB = regB &- 1
                if (regB != 0) {
                    regPC = newPC
                    tStates = 13
                } else {
                    tStates = 8
                }
                setThisOpcode(opc: String(op.hex) + " DJNZ $" + String(newPC.hex), target: newPC)

                
            case 0x1A:
                let adr = getDE()
                regA = system.fetchFrom(adr: adr)
                setThisOpcode(opc: String(op.hex) + " LD A, (DE)", target: 0x0000)
                tStates = 7

                
            case 0x0B, 0x1B, 0x2B, 0x3B:
                let rg = UInt8(op & 0x30) >> 4
                let dblReg = getDoubleReg(regIndex: rg)
                switch (dblReg.name) {
                case "BC":
                    setBC(newValue: rollOver(val: Int(getBC())-1))
                case "DE":
                    setDE(newValue: rollOver(val: Int(getDE())-1))
                case "HL":
                    setHL(newValue: rollOver(val: Int(getHL())-1))
                default:
                    regSP = rollOver(val: Int(regSP)-1)
                }
                setThisOpcode(opc: String(op.hex) + " DEC " + dblReg.name, target: 0x0000)
                tStates = 6
                
                
            case 0x12:
                let adr = getDE()
                system.setMem(adr: adr, val: regA)
                setThisOpcode(opc: String(op.hex) + " LD (DE), A", target: 0x0000)
                tStates = 7
                
                
            case 0x17:
                let tmp = regA
                regA = regA << 1
                regA |= (regF & 0x01)
                regF &= 0xEC
                regF |= ((tmp & 0x80) >> 7)
                tStates = 4
                setThisOpcode(opc: String(op.hex) + " RLA", target: 0x0000)


            case 0x18:
                let offset = system.fetch()
                var newPC = regPC
                newPC = addOffset(adr: newPC, offset: offset)
                regPC = newPC
                tStates = 12
                setThisOpcode(opc: String(op.hex) + " JR $" + String(newPC.hex), target: newPC)


            case 0x1F:
                let tmp = regA
                regA = regA >> 1
                regA |= (regF << 7)
                regF &= 0xEC
                regF |= (tmp & 0x01)
                tStates = 4
                setThisOpcode(opc: String(op.hex) + " RRA", target: 0x0000)

                
            case 0x20:
                let offset = system.fetch()
                if (regF & UInt8(flagZ) == 0) {
                    var newPC = regPC
                    newPC = addOffset(adr: newPC, offset: offset)
                    regPC = newPC
                    tStates = 12
                } else {
                    tStates = 7
                }
                setThisOpcode(opc: String(op.hex) + " JR NZ, $" + String(offset), target: regPC)

                
            case 0x22:
                let val = getHL()
                let adr = system.fetchWord()
                system.setMemWord(adr: adr, val: val)
                setThisOpcode(opc: String(op.hex) + " LD (" + String(adr.hex) + "), HL", target: adr)
                tStates = 16

                
            case 0x28:
                let offset = system.fetch()
                if (regF & UInt8(flagZ) == flagZ) {
                    var newPC = regPC
                    newPC = addOffset(adr: newPC, offset: offset)
                    regPC = newPC
                    tStates = 12
                } else {
                    tStates = 7
                }
                setThisOpcode(opc: String(op.hex) + " JR Z, $" + String(offset), target: regPC)

                
            case 0x27:
                var res = Int(regA)
                var f = regF
                let tmpC = (regF & UInt8(flagC)) == flagC
                let tmpH = (regF & UInt8(flagH)) == flagH
                let tmpN = (regF & UInt8(flagN)) == flagN
                var newC = false
                var newH = false
                // solution based on Rui Ribeiro's answer here: https://stackoverflow.com/questions/8119577/z80-daa-instruction
                var t = 0
                if (tmpH || ((regA & 0x0F) > 9)) {
                     t += 1
                }
                if (tmpC || (regA > 0x99)) {
                     t += 2
                     newC = true
                }
                // builds final H flag
                if (tmpN && !tmpH) {
                   newH = true
                } else {
                    if (tmpN && tmpH) {
                       newH = (((regA & 0x0F)) < 6)
                    } else {
                        newH = ((regA & 0x0F) >= 0x0A)
                    }
                }
                switch(t) {
                    case 1:
                        if (tmpN) { // -6:6
                            res -= 0x06
                        } else {
                            res += 0x06
                        }
                     case 2:
                        if (tmpN) { // -0x60:0x60
                            res -= 0x60
                        } else {
                            res += 0x60
                        }
                     default:
                        if (t == 3) {
                            if (tmpN) { // -0x66:0x66
                                res -= 0x66
                            } else {
                                res += 0x66
                            }
                        }
                }

                if (res > 255) {
                    res = res & 0x00FF
                }
                if (res < 0) {
                    res = res & 0x00FF
                    newC = true
                }
                regA = UInt8(res)
                // deal with flags on a 8 bit result
                f = regF & 0x2a // erase all relevant bits - for DAA reset S, Z, P, H & C; ignore X & N
                if (newC) {f |= UInt8(flagC)}
                if (newH) {f |= UInt8(flagH)}
                if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                if (res == 0) {f |= UInt8(flagZ)}
                if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                regF = f
                setThisOpcode(opc: String(op.hex) + " DAA", target: 0x0000)
                tStates = 4

                
            case 0x2A:
                let adr = system.fetchWord()
                let val = system.fetchWordFrom(adr: adr)
                setHL(newValue: val)
                setThisOpcode(opc: String(op.hex) + " LD HL, (" + String(adr.hex) + ")", target: adr)
                tStates = 16

                
            case 0x2F:
                regA = ~regA
                var f = regF & 0xED // 11101101
                f |= UInt8(flagH)
                f |= UInt8(flagN)
                regF = f
                setThisOpcode(opc: String(op.hex) + " CPL", target: 0x0000)
                tStates = 4


            case 0x30:
                let offset = system.fetch()
                if (regF & UInt8(flagC) == 0) {
                    var newPC = regPC
                    newPC = addOffset(adr: newPC, offset: offset)
                    regPC = newPC
                    tStates = 12
                } else {
                    tStates = 7
                }
                setThisOpcode(opc: String(op.hex) + " JR NC, $" + String(offset), target: regPC)

                
            case 0x32:
                let adr = system.fetchWord()
                system.setMem(adr: adr, val: regA)
                setThisOpcode(opc: String(op.hex) + " LD (" + String(adr.hex) + "), A", target: adr)
                tStates = 13

                
            case 0x34:
                let adr = getHL()
                let tmp = system.fetchFrom(adr: adr)
                let res = doINC(s: tmp)
                system.setMem(adr: adr, val: res)
                setThisOpcode(opc: String(op.hex) + " INC (HL)", target: 0x0000)
                tStates = 11

                
            case 0x35:
                let adr = getHL()
                let tmp = system.fetchFrom(adr: adr)
                let val = doDEC(s: tmp)
                system.setMem(adr: adr, val: val)
                setThisOpcode(opc: String(op.hex) + " DEC (HL)", target: 0x0000)
                tStates = 11

                
            case 0x36:
                let adr = getHL()
                let val = system.fetch()
                system.setMem(adr: adr, val: val)
                setThisOpcode(opc: String(op.hex) + " LD (HL), $" + String(val.hex), target: 0x0000)
                tStates = 10

                
            case 0x37:
                var f = regF & 0xEC // reset H & N, prepare C to be set
                f |= UInt8(flagC)
                regF = f
                setThisOpcode(opc: String(op.hex) + " SCF", target: 0x0000)
                tStates = 4
                
                
            case 0x38:
                let offset = system.fetch()
                if (regF & UInt8(flagC) == flagC) {
                    var newPC = regPC
                    newPC = addOffset(adr: newPC, offset: offset)
                    regPC = newPC
                    tStates = 12
                } else {
                    tStates = 7
                }
                setThisOpcode(opc: String(op.hex) + " JR C, $" + String(offset), target: regPC)


            case 0x3A:
                let adr = system.fetchWord()
                regA = system.fetchFrom(adr: adr)
                setThisOpcode(opc: String(op.hex) + " LD A, (" + String(adr.hex) + ")", target: adr)
                tStates = 13


//    S is not affected.
//    Z is not affected.
//    H, previous carry is copied.
//    P/V is not affected.
//    N is reset.
//    C is set if CY was 0 before operation; otherwise, it is reset.
//    SZXHXPNC
            case 0x3F:
                var f = regF & 0xEC // 11101100
                if (regF & 0x01 == 0x01) {
                    f |= UInt8(flagH)
                } else {
                    f |= UInt8(flagC)
                }
                regF = f
                setThisOpcode(opc: String(op.hex) + " CCF", target: 0x0000)
                tStates = 4

                
            case 0x46, 0x4E, 0x56, 0x5E, 0x66, 0x6E, 0x7E:
                let tmp = op >> 3
                let reg1 = getAddressModeReg(code: tmp)
                let adr = getHL()
                let val = system.fetchFrom(adr: adr)
                setRegByte(name: reg1.name, value: val)
                setThisOpcode(opc: String(op.hex) + " LD " + reg1.name + ", (HL)", target: 0x0000)
                tStates = 7

                
            case 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x77:
                let reg1 = getAddressModeReg(code: op)
                let adr = getHL()
                system.setMem(adr: adr, val: reg1.value)
                setThisOpcode(opc: String(op.hex) + " LD (HL), " + reg1.name, target: 0x0000)
                tStates = 7


            case 0x76:
                CPUHalt = true
                regPC -= 1
                setThisOpcode(opc: String(op.hex) + " HALT", target: 0x0000)
                tStates = 4

                
            case 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x87:
                let aMode = getAddressModeReg(code: op)
                let oldA = regA
                var res = Int(regA) + Int(aMode.value)
                var f = regF & 0x28 // erase all relevant bits
                if (res > 255) {
                    res = res & 0x00FF
                    f |= UInt8(flagC)
                }
                regA = UInt8(res)
                // deal with flags on a 8 bit result
                if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                if (res == 0) {f |= UInt8(flagZ)}
                if ((res & 0x0F) < (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
                if (oldA & 0x80 == aMode.value & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                f |= UInt8(flagN)
                regF = f
                setThisOpcode(opc: String(op.hex) + " ADD A, " + aMode.name, target: 0x0000)
                tStates = 4


            case 0x86:
                let oldA = regA
                let adr = getHL()
                let val = system.fetchFrom(adr: adr)
                var res = Int(regA) + Int(val)
                var f = regF & 0x28 // erase all relevant bits
                if (res > 255) {
                    res = res & 0x00FF
                    f |= UInt8(flagC)
                }
                regA = UInt8(res)
                // deal with flags on a 8 bit result
                if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                if (res == 0) {f |= UInt8(flagZ)}
                if ((res & 0x0F) < (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
//                if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                if (oldA & 0x80 == val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                f |= UInt8(flagN)
                regF = f
                setThisOpcode(opc: String(op.hex) + " ADD A, (HL)", target: 0x0000)
                tStates = 7
                
                
            case 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8F:
                let aMode = getAddressModeReg(code: op)
                let oldA = regA
                var res = Int(regA) + Int(aMode.value) + Int(regF & UInt8(flagC))
                var f = regF & 0x28 // erase all relevant bits
                if (res > 255) {
                    res = res & 0x00FF
                    f |= UInt8(flagC)
                }
                regA = UInt8(res)
                // deal with flags on a 8 bit result
                if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                if (res == 0) {f |= UInt8(flagZ)}
                if ((res & 0x0F) < (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
//                if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                if (oldA & 0x80 == aMode.value & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                regF = f
                setThisOpcode(opc: String(op.hex) + " ADC A, " + aMode.name, target: 0x0000)
                tStates = 4


            case 0x8E:
                let oldA = regA
                let adr = getHL()
                let val = system.fetchFrom(adr: adr)
                var res = Int(regA) + Int(val) + Int(regF & UInt8(flagC))
                var f = regF & 0x28 // erase all relevant bits
                if (res > 255) {
                    res = res & 0x00FF
                    f |= UInt8(flagC)
                }
                regA = UInt8(res)
                // deal with flags on a 8 bit result
                if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                if (res == 0) {f |= UInt8(flagZ)}
                if ((res & 0x0F) < (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
//                if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                if (oldA & 0x80 == val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                regF = f
                setThisOpcode(opc: String(op.hex) + " ADC A, (HL)", target: 0x0000)
                tStates = 7

                
            case 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x97:
                let aMode = getAddressModeReg(code: op)
                let oldA = regA
                var res = Int(regA) - Int(aMode.value)
                var f = regF & 0x28 // erase all relevant bits
                if (res < 0) {
                    res = res & 0xff
                    f |= UInt8(flagC)
                }
                regA = UInt8(res)
                // deal with flags on a 8 bit result
                if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                if (res == 0) {f |= UInt8(flagZ)}
                if ((res & 0x0F) > (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been reduced
                if (oldA & 0x80 != aMode.value & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                f |= UInt8(flagN)
                regF = f
                setThisOpcode(opc: String(op.hex) + " SUB A, " + aMode.name, target: 0x0000)
                tStates = 4


            case 0x96:
                let adr = getHL()
                let val = system.fetchFrom(adr: adr)
                let oldA = regA
                var res = Int(regA) - Int(val)
                var f = regF & 0x28 // erase all relevant bits
                if (res < 0) {
                    res = res & 0xff
                    f |= UInt8(flagC)
                }
                regA = UInt8(res)
                // deal with flags on a 8 bit result
                if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                if (res == 0) {f |= UInt8(flagZ)}
                if ((res & 0x0F) > (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been reduced
//                if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                if (oldA & 0x80 != val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                f |= UInt8(flagN)
                regF = f
                setThisOpcode(opc: String(op.hex) + " SUB A, (HL)", target: 0x0000)
                tStates = 7

                
            case 0x98, 0x99, 0x9A, 0x9B, 0x9C, 0x9D, 0x9F:
                let aMode = getAddressModeReg(code: op)
                let oldA = regA
                var res = Int(regA) - Int(aMode.value) - Int(regF & 0x01)
                var f = regF & 0x28 // erase all relevant bits
                if (res < 0) {
                    res = res & 0xff
                    f |= UInt8(flagC)
                }
                regA = UInt8(res)
                // deal with flags on a 8 bit result
                if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                if (res == 0) {f |= UInt8(flagZ)}
                if ((res & 0x0F) > (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been reduced
                if (oldA & 0x80 != aMode.value & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                f |= UInt8(flagN)
                regF = f
                setThisOpcode(opc: String(op.hex) + " SBC A, " + aMode.name, target: 0x0000)
                tStates = 4

                
            case 0x9E:
                let adr = getHL()
                let val = system.fetchFrom(adr: adr)
                let oldA = regA
                var res = Int(regA) - Int(val) - Int(regF & 0x01)
                var f = regF & 0x28 // erase all relevant bits
                if (res < 0) {
                    res = res & 0xff
                    f |= UInt8(flagC)
                }
                regA = UInt8(res)
                // deal with flags on a 8 bit result
                if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                if (res == 0) {f |= UInt8(flagZ)}
                if ((res & 0x0F) > (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been reduced
//                if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                if (oldA & 0x80 != val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                f |= UInt8(flagN)
                regF = f
                setThisOpcode(opc: String(op.hex) + " SBC A, (HL)", target: 0x0000)
                tStates = 7


            case 0xA6:
                let adr = getHL()
                let tmp = system.fetchFrom(adr: adr)
                doAND(s: tmp)
                setThisOpcode(opc: String(op.hex) + " AND (HL)", target: 0x0000)
                tStates = 7

                
            case 0xA0, 0xA1, 0xA2, 0xA3, 0xA4, 0xA5, 0xA7:
                let aMode = getAddressModeReg(code: op)
                doAND(s: aMode.value)
                setThisOpcode(opc: String(op.hex) + " AND " + aMode.name, target: 0x0000)
                tStates = 4


            case 0xA8, 0xA9, 0xAA, 0xAB, 0xAC, 0xAD, 0xAF:
                let aMode = getAddressModeReg(code: op)
                doXOR(s: aMode.value)
                setThisOpcode(opc: String(op.hex) + " XOR " + aMode.name, target: 0x0000)
                tStates = 4

                
            case 0xAE:
                let adr = getHL()
                let tmp = system.fetchFrom(adr: adr)
                doXOR(s: tmp)
                setThisOpcode(opc: String(op.hex) + " XOR (HL)", target: 0x0000)
                tStates = 7

                
            case 0xB6:
                let adr = getHL()
                let tmp = system.fetchFrom(adr: adr)
                doOR(s: tmp)
                setThisOpcode(opc: String(op.hex) + " OR (HL)", target: 0x0000)
                tStates = 7

                
            case 0xB0, 0xB1, 0xB2, 0xB3, 0xB4, 0xB5, 0xB7:
                let aMode = getAddressModeReg(code: op)
                doOR(s: aMode.value)
                setThisOpcode(opc: String(op.hex) + " OR " + aMode.name, target: 0x0000)
                tStates = 4

                
            case 0xB8, 0xB9, 0xBA, 0xBB, 0xBC, 0xBD, 0xBF:
                let aMode = getAddressModeReg(code: op)
                doCP(s: aMode.value)
                setThisOpcode(opc: String(op.hex) + " CP " + aMode.name, target: 0x0000)
                tStates = 4


            case 0xBE:
                let adr = getHL()
                let tmp = system.fetchFrom(adr: adr)
                doCP(s: tmp)
                setThisOpcode(opc: String(op.hex) + " CP (HL)", target: 0x0000)
                tStates = 7

                
            case 0xC0:
                if (regF & UInt8(flagZ) == 0) {
                    regPC = system.fetchWordFrom(adr: regSP)
                    regSP = regSP &+ 2
                    tStates = 11
                } else {
                    tStates = 5
                }
                setThisOpcode(opc: String(op.hex) + " RET NZ", target: regPC)

                
            case 0xC1, 0xD1, 0xE1, 0xF1:
                let rg = UInt8(op & 0x30) >> 4
                let dblReg = getDoubleReg(regIndex: rg) // in this case "SP" indicates "AF"
                let tmp = system.fetchWordFrom(adr: regSP)
                regSP = regSP &+ 2
                var tmpName = dblReg.name
                if (tmpName == "SP") {
                    tmpName = "AF"
                }
                switch (dblReg.name) {
                    case "BC":
                        setBC(newValue: tmp)
                    case "DE":
                        setDE(newValue: tmp)
                    case "HL":
                        setHL(newValue: tmp)
                    default:    // deal with AF
                        regA = UInt8(tmp >> 8)
                        regF = UInt8(tmp & 0x00ff)
                }
                setThisOpcode(opc: String(op.hex) + " POP " + tmpName, target: 0x0000)
                tStates = 10

                
            case 0xC2:
                let adr = system.fetchWord()
                if (regF & UInt8(flagZ) == 0) {
                    regPC = adr
                }
                setThisOpcode(opc: String(op.hex) + " JP NZ, $" + String(adr.hex), target: adr)
                tStates = 10

                
            case 0xC3:
                let adr = system.fetchWord()
                regPC = adr
                setThisOpcode(opc: String(op.hex) + " JP $" + String(adr.hex), target: adr)
                tStates = 10

                
            case 0xC5, 0xD5, 0xE5, 0xF5:
                let rg = UInt8(op & 0x30) >> 4
                let dblReg = getDoubleReg(regIndex: rg) // in this case "SP" indicates "AF"
                var tmpName = dblReg.name
                if (tmpName == "SP") {
                    tmpName = "AF"
                }
                regSP = regSP &- 2
                switch (dblReg.name) {
                case "BC":
                    system.setMemWord(adr: regSP, val: getBC())
                case "DE":
                    system.setMemWord(adr: regSP, val: getDE())
                case "HL":
                    system.setMemWord(adr: regSP, val: getHL())
                default:    // deal with AF
                    let tmp = UInt16(Int(regA) * 256 + Int(regF))
                    system.setMemWord(adr: regSP, val: tmp)
                }
                setThisOpcode(opc: String(op.hex) + " PUSH " + tmpName, target: 0x0000)
                tStates = 11


            case 0xC6:
                let val = system.fetch()
                let oldA = regA
                var res = Int(regA) + Int(val)
                var f = regF & 0x28 // erase all relevant bits
                if (res > 255) {
                    res = res & 0x00FF
                    f |= UInt8(flagC)
                }
                regA = UInt8(res)
                // deal with flags on a 8 bit result
                if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                if (res == 0) {f |= UInt8(flagZ)}
                if ((res & 0x0F) < (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
//                if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                if (oldA & 0x80 == val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                regF = f
                setThisOpcode(opc: String(op.hex) + " ADD A, $" + String(val.hex), target: 0x0000)
                tStates = 7

                
                
            case 0xC7, 0xCF, 0xD7, 0xDF, 0xE7, 0xEF, 0xF7, 0xFF:
                regSP = regSP &- 2
                system.setMemWord(adr: regSP, val: regPC)
                let rstP = (op & 0x38) >> 3
                regPC = UInt16(rstP * 8)
                setThisOpcode(opc: String(op.hex) + " RST " + String(rstP), target: regPC)
                tStates = 11
                
                
            case 0xC8:
                if (regF & UInt8(flagZ) == flagZ) {
                    regPC = system.fetchWordFrom(adr: regSP)
                    regSP = regSP &+ 2
                    tStates = 11
                } else {
                    tStates = 5
                }
                setThisOpcode(opc: String(op.hex) + " RET Z", target: regPC)


            case 0xCA:
                let adr = system.fetchWord()
                if (regF & UInt8(flagZ) == flagZ) {
                    regPC = adr
                }
                setThisOpcode(opc: String(op.hex) + " JP Z, $" + String(adr.hex), target: adr)
                tStates = 10

                
                // ====================================================================
                // ====================================================================
            case 0xCB:                      // ================== CB ==================
                let mode = system.fetch()
                let reg = mode & 0x07
                var bitNum = (mode & 0x38) >> 3
                var modeName = "SET"
                var modeString = "(HL)"
                let modeLow = mode & 0x0F
                switch(mode & 0xF0) {
                case 0x00:      // RRC or RLC
                    if (modeLow > 0x07) {
                        modeName = "RRC"
                        bitNum = 255 // for logger display
                        var oldtmp: UInt8
                        var tmp: UInt8
                        if (reg == 0x06) {  // (HL)
                            let adr = getHL()
                            tmp = system.fetchFrom(adr: adr)
                            oldtmp = tmp
                            tmp = tmp >> 1
                            tmp |= ((oldtmp & 0x01) << 7) // bit 0 moves into bit 7
                            system.setMem(adr: adr, val: tmp)
                            tStates = 15
                        } else {    // reg
                            let tmpreg = getAddressModeReg(code: reg)
                            tmp = tmpreg.value
                            oldtmp = tmp
                            tmp = tmp >> 1
                            tmp |= ((oldtmp & 0x01) << 7) // bit 0 moves into bit 7
                            setRegByte(name: tmpreg.name, value: tmp)
                            modeString = tmpreg.name
                            tStates = 8
                        }
                        var f = regF & 0x28 // erase all relevant bits
                        f |= oldtmp & 0x01  // bit 0 sets carry flag
                        if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                        if (tmp == 0) {f |= UInt8(flagZ)}
                        if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                        regF = f
                    } else {
                        modeName = "RLC"
                        bitNum = 255 // for logger display
                        var oldtmp: UInt8
                        var tmp: UInt8
                        if (reg == 0x06) {  // (HL)
                            let adr = getHL()
                            tmp = system.fetchFrom(adr: adr)
                            oldtmp = tmp
                            tmp = tmp << 1
                            tmp |= ((oldtmp & 0x80) >> 7) // bit 7 moves into bit 0
                            system.setMem(adr: adr, val: tmp)
                            tStates = 15
                        } else {    // reg
                            let tmpreg = getAddressModeReg(code: reg)
                            tmp = tmpreg.value
                            oldtmp = tmp
                            tmp = tmp << 1
                            tmp |= ((oldtmp & 0x80) >> 7) // bit 7 moves into bit 0
                            setRegByte(name: tmpreg.name, value: tmp)
                            modeString = tmpreg.name
                            tStates = 8
                        }
                        var f = regF & 0x28 // erase all relevant bits
                        f |= ((oldtmp & 0x80) >> 7)   // bit 7 sets carry flag
                        if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                        if (tmp == 0) {f |= UInt8(flagZ)}
                        if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                        regF = f
                    }
                case 0x10:      // RR or RL
                    switch(mode) {
                        case 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1f:      // RR r
                            // this is wrong in the Zilog manual - it shows 0x08-0x1f
                            modeName = "RR"
                            bitNum = 255 // for logger display
                            var oldtmp: UInt8
                            var tmp: UInt8
                            let tmpreg = getAddressModeReg(code: reg)
                            tmp = tmpreg.value
                            oldtmp = tmp
                            tmp = tmp >> 1
                            tmp |= ((regF & 0x01) << 7) // carry moves into bit 7
                            setRegByte(name: tmpreg.name, value: tmp)
                            modeString = tmpreg.name
                            tStates = 8
                            var f = regF & 0x28 // erase all relevant bits
                            f |= oldtmp & 0x01  // bit 0 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        case 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x17:      // RL r
                            modeName = "RL"
                            bitNum = 255 // for logger display
                            var oldtmp: UInt8
                            var tmp: UInt8
                            let tmpreg = getAddressModeReg(code: reg)
                            tmp = tmpreg.value
                            oldtmp = tmp
                            tmp = tmp << 1
                            tmp |= regF & 0x01 // carry moves into bit 0
                            setRegByte(name: tmpreg.name, value: tmp)
                            modeString = tmpreg.name
                            tStates = 8
                            var f = regF & 0x28 // erase all relevant bits
                            f |= ((oldtmp & 0x80) >> 7)   // bit 7 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        case 0x16:      // RL (HL)
                            modeName = "RL"
                            bitNum = 255 // for logger display
                            var oldtmp: UInt8
                            var tmp: UInt8
                            let adr = getHL()
                            tmp = system.fetchFrom(adr: adr)
                            oldtmp = tmp
                            tmp = tmp << 1
                            tmp |= regF & 0x01 // carry moves into bit 0
                            system.setMem(adr: adr, val: tmp)
                            tStates = 15
                            var f = regF & 0x28 // erase all relevant bits
                            f |= ((oldtmp & 0x80) >> 7)   // bit 7 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        default:      // 0x1E: RR (HL)
                            // TODO: make sure this is complete
                            modeName = "RR"
                            bitNum = 255 // for logger display
                            var oldtmp: UInt8
                            var tmp: UInt8
                            let adr = getHL()
                            tmp = system.fetchFrom(adr: adr)
                            oldtmp = tmp
                            tmp = tmp >> 1
                            tmp |= ((regF & 0x01) << 7) // carry moves into bit 7
                            system.setMem(adr: adr, val: tmp)
                            tStates = 15
                            var f = regF & 0x28 // erase all relevant bits
                            f |= oldtmp & 0x01  // bit 0 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                    }
                case 0x20:      // SRA or SLA
                    if (modeLow > 0x07) {
                        modeName = "SRA"
                        bitNum = 255 // for logger display
                        var oldtmp: UInt8
                        var tmp: UInt8
                        if (reg == 0x06) {  // (HL)
                            let adr = getHL()
                            tmp = system.fetchFrom(adr: adr)
                            oldtmp = tmp
                            tmp = tmp >> 1
                            tmp |= (oldtmp & 0x80)
                            system.setMem(adr: adr, val: tmp)
                            tStates = 15
                        } else {    // reg
                            let tmpreg = getAddressModeReg(code: reg)
                            tmp = tmpreg.value
                            oldtmp = tmp
                            tmp = tmp >> 1
                            tmp |= (oldtmp & 0x80)
                            setRegByte(name: tmpreg.name, value: tmp)
                            modeString = tmpreg.name
                            tStates = 8
                        }
                        var f = regF & 0x28 // erase all relevant bits
                        f |= oldtmp & 0x01  // bit 0 sets carry flag
                        if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                        if (tmp == 0) {f |= UInt8(flagZ)}
                        if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                        regF = f
                    } else {
                        modeName = "SLA"
                        bitNum = 255 // for logger display
                        var oldtmp: UInt8
                        var tmp: UInt8
                        if (reg == 0x06) {  // (HL)
                            let adr = getHL()
                            tmp = system.fetchFrom(adr: adr)
                            oldtmp = tmp
                            tmp = tmp << 1
                            tmp &= 0xfe
                            system.setMem(adr: adr, val: tmp)
                            tStates = 15
                        } else {    // reg
                            let tmpreg = getAddressModeReg(code: reg)
                            tmp = tmpreg.value
                            oldtmp = tmp
                            tmp = tmp << 1
                            tmp &= 0xfe
                            setRegByte(name: tmpreg.name, value: tmp)
                            modeString = tmpreg.name
                            tStates = 8
                        }
                        var f = regF & 0x28 // erase all relevant bits
                        f |= ((oldtmp & 0x80) >> 7)   // bit 7 sets carry flag
                        if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                        if (tmp == 0) {f |= UInt8(flagZ)}
                        if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                        regF = f
                    }
                case 0x30:      // SRL or SLL
                    if (modeLow > 0x07) {
                        modeName = "SRL"
                        bitNum = 255 // for logger display
                        var oldtmp: UInt8
                        var tmp: UInt8
                        if (reg == 0x06) {  // (HL)
                            let adr = getHL()
                            tmp = system.fetchFrom(adr: adr)
                            oldtmp = tmp
                            tmp = tmp >> 1
                            system.setMem(adr: adr, val: tmp)
                            tStates = 15
                        } else {    // reg
                            let tmpreg = getAddressModeReg(code: reg)
                            tmp = tmpreg.value
                            oldtmp = tmp
                            tmp = tmp >> 1
                            setRegByte(name: tmpreg.name, value: tmp)
                            modeString = tmpreg.name
                            tStates = 8
                        }
                        var f = regF & 0x28 // erase all relevant bits
                        f |= oldtmp & 0x01  // bit 0 sets carry flag
                        if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                        if (tmp == 0) {f |= UInt8(flagZ)}
                        if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                        regF = f
                    } else {
                        modeName = "SLL"
                        bitNum = 255 // for logger display
                        var oldtmp: UInt8
                        var tmp: UInt8
                        if (reg == 0x06) {  // (HL)
                            let adr = getHL()
                            tmp = system.fetchFrom(adr: adr)
                            oldtmp = tmp
                            tmp = tmp << 1
                            tmp |= 0x01
                            system.setMem(adr: adr, val: tmp)
                            tStates = 15
                        } else {    // reg
                            let tmpreg = getAddressModeReg(code: reg)
                            tmp = tmpreg.value
                            oldtmp = tmp
                            tmp = tmp << 1
                            tmp |= 0x01
                            setRegByte(name: tmpreg.name, value: tmp)
                            modeString = tmpreg.name
                            tStates = 8
                        }
                        var f = regF & 0x28 // erase all relevant bits
                        f |= ((oldtmp & 0x80) >> 7)   // bit 7 sets carry flag
                        if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                        if (tmp == 0) {f |= UInt8(flagZ)}
                        if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                        regF = f
                    }
                case 0x40, 0x50, 0x60, 0x70:   // RES
                    modeName = "BIT"
                    if (reg == 0x06) {  // (HL)
                        let adr = getHL()
                        var tmp = system.fetchFrom(adr: adr)
                        tmp = tmp & UInt8(0x01 << bitNum)
                        var f = regF & 0xBD
                        f |= UInt8(flagH)
                        if (tmp == 0) {
                            f |= UInt8(flagZ)
                        }
                        regF = f
                        tStates = 12
                    } else {    // reg
                        var tmp = getAddressModeReg(code: reg)
                        tmp.value = tmp.value & UInt8(0x01 << bitNum)
                        var f = regF & 0xBD
                        f |= UInt8(flagH)
                        if (tmp.value == 0) {
                            f |= UInt8(flagZ)
                        }
                        regF = f
                        modeString = tmp.name
                        tStates = 8
                    }
                case 0x80, 0x90, 0xa0, 0xb0:   // RES
                    modeName = "RES"
                    if (reg == 0x06) {  // (HL)
                        let adr = getHL()
                        var tmp = system.fetchFrom(adr: adr)
                        tmp = resBit(value: tmp, bit: Int(bitNum))
                        system.setMem(adr: adr, val: tmp)
                        tStates = 15
                    } else {    // reg
                        var tmp = getAddressModeReg(code: reg)
                        tmp.value = resBit(value: tmp.value, bit: Int(bitNum))
                        setRegByte(name: tmp.name, value: tmp.value)
                        modeString = tmp.name
                        tStates = 8
                    }
                default:   // SET
                    // TODO: make sure this is complete
                    if (reg == 0x06) {  // (HL)
                        let adr = getHL()
                        var tmp = system.fetchFrom(adr: adr)
                        tmp = setBit(value: tmp, bit: Int(bitNum))
                        system.setMem(adr: adr, val: tmp)
                        tStates = 15
                    } else {    // reg
                        var tmp = getAddressModeReg(code: reg)
                        tmp.value = setBit(value: tmp.value, bit: Int(bitNum))
                        setRegByte(name: tmp.name, value: tmp.value)
                        modeString = tmp.name
                        tStates = 8
                    }
                }
                if (bitNum == 255) {
                    setThisOpcode(opc: String(op.hex) + " "+modeName+" " + modeString, target: 0x0000)
                } else {
                    setThisOpcode(opc: String(op.hex) + " "+modeName+" " + String(bitNum) + ", " + modeString, target: 0x0000)
                }
                    
                // ============= END OF CB ===============
                // ====================================================================
                // ====================================================================

                
            case 0xC4:
                if (regF & UInt8(flagZ) == 0) {
                    regSP = regSP &- 2
                    let newPC = system.fetchWord()
                    system.setMemWord(adr: regSP, val: regPC)
                    regPC = newPC
                    tStates = 17
                    setThisOpcode(opc: String(op.hex) + " CALL NZ, $" + String(regPC.hex), target: regPC)
                } else {
                    let newPC = system.fetchWord()
                    tStates = 10
                    setThisOpcode(opc: String(op.hex) + " CALL NZ, $xxxx", target: newPC)
                }


            case 0xC9:
                regPC = system.fetchWordFrom(adr: regSP)
                regSP = regSP &+ 2
                setThisOpcode(opc: String(op.hex) + " RET", target: regPC)
                tStates = 10


            case 0xCC:
                if (regF & UInt8(flagZ) == flagZ) {
                    regSP = regSP &- 2
                    let newPC = system.fetchWord()
                    system.setMemWord(adr: regSP, val: regPC)
                    regPC = newPC
                    tStates = 17
                    setThisOpcode(opc: String(op.hex) + " CALL Z, $" + String(regPC.hex), target: regPC)
                } else {
                    let newPC = system.fetchWord()
                    tStates = 10
                    setThisOpcode(opc: String(op.hex) + " CALL Z, $xxxx", target: newPC)
                }


            case 0xCD:
                regSP = regSP &- 2
                let newPC = system.fetchWord()
                system.setMemWord(adr: regSP, val: regPC)
                regPC = newPC
                setThisOpcode(opc: String(op.hex) + " CALL $" + String(regPC.hex), target: regPC)
                tStates = 17

                
            case 0xCE:
                let val = system.fetch()
                let oldA = regA
                var res = Int(regA) + Int(val) + Int(regF & UInt8(flagC))
                var f = regF & 0x28 // erase all relevant bits
                if (res > 255) {
                    res = res & 0x00FF
                    f |= UInt8(flagC)
                }
                regA = UInt8(res)
                // deal with flags on a 8 bit result
                if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                if (res == 0) {f |= UInt8(flagZ)}
                if ((res & 0x0F) < (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
//                if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                if (oldA & 0x80 == val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                f |= UInt8(flagN)
                regF = f
                setThisOpcode(opc: String(op.hex) + " ADC A, " + String(val.hex), target: 0x0000)
                tStates = 7

                
            case 0xD0:
                if (regF & UInt8(flagC) == 0) {
                    regPC = system.fetchWordFrom(adr: regSP)
                    regSP = regSP &+ 2
                    tStates = 11
                } else {
                    tStates = 5
                }
                setThisOpcode(opc: String(op.hex) + " RET NC", target: regPC)


            case 0xD2:
                let adr = system.fetchWord()
                if (regF & UInt8(flagC) == 0) {
                    regPC = adr
                }
                setThisOpcode(opc: String(op.hex) + " JP NC, $" + String(adr.hex), target: regPC)
                tStates = 10

                
            case 0xD3:
                let adr = UInt16(system.fetch())
                system.setIOPort(adr: adr, val: regA)
                setThisOpcode(opc: String(op.hex) + " OUT (" + String(adr.hex) + "), A", target: adr)
//                logger(logmsg: "OUT (" + String(adr.hex) + "), A" + " value: "+String(regA)) // DEBUG ONLY
                tStates = 11

                
            case 0xD4:
                if (regF & UInt8(flagC) == 0) {
                    regSP = regSP &- 2
                    let newPC = system.fetchWord()
                    system.setMemWord(adr: regSP, val: regPC)
                    regPC = newPC
                    tStates = 17
                    setThisOpcode(opc: String(op.hex) + " CALL NC, $" + String(regPC.hex), target: regPC)
                } else {
                    let newPC = system.fetchWord()
                    tStates = 10
                    setThisOpcode(opc: String(op.hex) + " CALL NC, $xxxx", target: newPC)
                }


            case 0xD6:
                let val = system.fetch()
                let oldA = regA
                var res = Int(regA) - Int(val)
                var f = regF & 0x28 // erase all relevant bits
                if (res < 0) {
                    res = res & 0xff
                    f |= UInt8(flagC)
                }
                regA = UInt8(res)
                // deal with flags on a 8 bit result
                if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                if (res == 0) {f |= UInt8(flagZ)}
                if ((res & 0x0F) > (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been reduced
//                if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                if (oldA & 0x80 != val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                f |= UInt8(flagN)
                regF = f
                setThisOpcode(opc: String(op.hex) + " SUB A, $" + String(val.hex), target: 0x0000)
                tStates = 7

                
            case 0xD8:
                if (regF & UInt8(flagC) == flagC) {
                    regPC = system.fetchWordFrom(adr: regSP)
                    regSP = regSP &+ 2
                    tStates = 11
                } else {
                    tStates = 5
                }
                setThisOpcode(opc: String(op.hex) + " RET C", target: regPC)

                
            case 0xD9:
                let tmpB = regB
                let tmpC = regC
                let tmpD = regD
                let tmpE = regE
                let tmpH = regH
                let tmpL = regL
                regB = regBshadow
                regC = regCshadow
                regD = regDshadow
                regE = regEshadow
                regH = regHshadow
                regL = regLshadow
                regBshadow = tmpB
                regCshadow = tmpC
                regDshadow = tmpD
                regEshadow = tmpE
                regHshadow = tmpH
                regLshadow = tmpL
                setThisOpcode(opc: String(op.hex) + " EXX", target: 0x0000)
                tStates = 4


            case 0xDA:
                let adr = system.fetchWord()
                if (regF & UInt8(flagC) == flagC) {
                    regPC = adr
                }
                setThisOpcode(opc: String(op.hex) + " JP C, $" + String(adr.hex), target: regPC)
                tStates = 10

                
            case 0xDB:
                // The operand adr is placed on the bottom half (A0 through A7) of the address bus to select
                // the I/O device at one of 256 possible ports. The contents of the Accumulator (regA/tmp) also appear
                // on the top half (A8 through A15) of the address bus at this time.
                let adr = system.fetch()
                let tmp = regA
                regA = system.getIOPort(adr: UInt16(Int(adr) + 256 * Int(regA)))
                setThisOpcode(opc: String(op.hex) + " IN A, (0x" + String(tmp.hex) + String(adr.hex) + ")", target: Word((Int(tmp)*256+Int(adr))))
//                logger(logmsg: "IN A, (" + String(adr.hex) + ")" + " result: "+String(regA)) // DEBUG ONLY
                tStates = 11


            case 0xDC:
                if (regF & UInt8(flagC) == flagC) {
                    regSP = regSP &- 2
                    let newPC = system.fetchWord()
                    system.setMemWord(adr: regSP, val: regPC)
                    regPC = newPC
                    tStates = 17
                    setThisOpcode(opc: String(op.hex) + " CALL C, $" + String(regPC.hex), target: regPC)
                } else {
                    let newPC = system.fetchWord()
                    tStates = 10
                    setThisOpcode(opc: String(op.hex) + " CALL C, $xxxx", target: newPC)
                }


                // ====================================================================
                // ====================================================================
            case 0xDD:                              // ============= DD ===============
                let op2 = system.fetch()
                switch(op2){

                case 0x09, 0x19, 0x29, 0x39:
                    let rg = UInt8(op2 & 0x30) >> 4
                    let dblReg = getDoubleReg(regIndex: rg) // if this returns HL we use IX instead
                    let oldIX = regIX
                    var res = Int(oldIX)
                    switch (dblReg.name) {
                    case "BC":
                        res = res + Int(getBC())
                    case "DE":
                        res = res + Int(getDE())
                    case "HL":
                        res = res + Int(regIX)  // overwrite default HL
                    default:
                        res = res + Int(regSP)
                    }
                    var f = regF & 0xEC // erase all relevant bits
                    if (res > 0xFFFF) {
//                        res = res - 0x10000
                        res = res & 0xFFFF
                        f |= UInt8(flagC)
                    }
                    regIX = UInt16(res)
                    // deal with flags on a 16 bit result
                    if ((res & 0x0FFF) < (oldIX & 0x0FFF)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " ADD IX, " + dblReg.name, target: 0x0000)
                    tStates = 15

                    
                case 0x21:
                    let tmp = system.fetchWord()
                    regIX = tmp
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD IX, $" + String(toHexWord(num: tmp)), target: 0x0000)
                    tStates = 14

                    
                case 0x22:
                    let adr = system.fetchWord()
                    system.setMemWord(adr: adr, val: regIX)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD (" + String(toHexWord(num: adr)) + "), IX", target: adr)
                    tStates = 20


                case 0x23:
                    regIX = rollOverUp(val: Int(regIX)+1)
                    setThisOpcode(opc: String(op.hex) + " INC IX", target: 0x0000)
                    tStates = 10

                case 0x26:
                    let val = system.fetch()
                    regIX = (regIX & 0x00FF) | UInt16(Int(val) << 8)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD IXh, $" + String(toHex(num: val)), target: 0x0000)
                    tStates = 11

                case 0x2A:
                    let adr = system.fetchWord()
                    let tmp = system.fetchWordFrom(adr: adr)
                    regIX = tmp
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD IX, (" + String(toHexWord(num: adr)) + ")", target: adr)
                    tStates = 20

                case 0x2B:
                    regIX = rollOver(val: Int(regIX)-1)
                    setThisOpcode(opc: String(op.hex) + " DEC IX", target: 0x0000)
                    tStates = 10

                case 0x2C:
                    var val = UInt8(regIX & 0x00FF)
                    val = doINC(s: val)
                    if (val == 0) {
                        regF |= UInt8(flagPV)
                    }
                    regIX = (regIX & 0xFF00) | UInt16(val)
                    setThisOpcode(opc: String(op.hex) + " INC IXl", target: 0x0000)
                    tStates = 8

                case 0x2D:
                    var val = UInt8(regIX & 0x00FF)
                    val = doDEC(s: val)
                    if (val == 255) {
                        regF |= UInt8(flagPV)
                    }
                    regIX = (regIX & 0xFF00) | UInt16(val)
                    setThisOpcode(opc: String(op.hex) + " DEC IXl", target: 0x0000)
                    tStates = 8

                case 0x2E:
                    let val = system.fetch()
                    regIX = (regIX & 0xFF00) | UInt16(Int(val))
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD IXl, $" + String(toHex(num: val)), target: 0x0000)
                    tStates = 11

                case 0x34:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIX, offset: offset)
                    let tmp = system.fetchFrom(adr: adr)
                    let res = doINC(s: tmp)
                    system.setMem(adr: adr, val: res)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " INC (IX+$"+String(toHex(num: offset))+")", target: 0x0000)
                    tStates = 23


                case 0x35:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIX, offset: offset)
                    let tmp = system.fetchFrom(adr: adr)
                    let res = doDEC(s: tmp)
                    system.setMem(adr: adr, val: res)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " DEC (IX+$"+String(toHex(num: offset))+")", target: 0x0000)
                    tStates = 23

                case 0x36:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIX, offset: offset)
                    let tmp = system.fetch()
                    system.setMem(adr: adr, val: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD (IX+$"+String(toHex(num: offset)) + "), $" + String(toHex(num: tmp)), target: 0x0000)
                    tStates = 19

                case 0x46, 0x4E, 0x56, 0x5E, 0x66, 0x6E, 0x7E:
                    let aMode = getAddressModeReg(code: op2 >> 3)
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIX, offset: offset)
                    setRegByte(name: aMode.name, value: system.fetchFrom(adr: adr))
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD "+aMode.name+", (IX+$"+String(toHex(num: offset)) + ")", target: 0x0000)
                    tStates = 19

                case 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x47:
                    let aMode = getUndocumentedAddressModeReg(code: op2, xory: "x")
                    regB = aMode.value
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD B, " + aMode.name, target: 0x0000)
                    tStates = 8

                case 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4F:
                    let aMode = getUndocumentedAddressModeReg(code: op2, xory: "x")
                    regC = aMode.value
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD C, " + aMode.name, target: 0x0000)
                    tStates = 8

                case 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x57:
                    let aMode = getUndocumentedAddressModeReg(code: op2, xory: "x")
                    regD = aMode.value
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD D, " + aMode.name, target: 0x0000)
                    tStates = 8

                case 0x58, 0x59, 0x5A, 0x5B, 0x5C, 0x5D, 0x5F:
                    let aMode = getUndocumentedAddressModeReg(code: op2, xory: "x")
                    regE = aMode.value
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD E, " + aMode.name, target: 0x0000)
                    tStates = 8

                case 0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x67:
                    let aMode = getUndocumentedAddressModeReg(code: op2, xory: "x")
                    regIX = (regIX & 0x00FF) | UInt16(Int(aMode.value) << 8)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD IXh, " + aMode.name, target: 0x0000)
                    tStates = 8

                case 0x68, 0x69, 0x6A, 0x6B, 0x6C, 0x6D, 0x6F:
                    let aMode = getUndocumentedAddressModeReg(code: op2, xory: "x")
                    regIX = (regIX & 0xFF00) | UInt16(aMode.value)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD IXl, " + aMode.name, target: 0x0000)
                    tStates = 8

                case 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x77:
                    let aMode = getAddressModeReg(code: op2)
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIX, offset: offset)
                    let tmp = aMode.value
                    system.setMem(adr: adr, val: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD (IX+$"+String(toHex(num: offset)) + "), " + aMode.name, target: 0x0000)
                    tStates = 19

                case 0x78, 0x79, 0x7A, 0x7B, 0x7F:
                    let aMode = getAddressModeReg(code: op2)
                    regA = aMode.value
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD a, " + aMode.name, target: 0x0000)
                    tStates = 8

                case 0x7c:
                    let aMode = getAddressModeReg(code: op2)
                    regA = Byte((regIX >> 8) & 0x00FF)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD a, IXh", target: 0x0000)
                    tStates = 8

                case 0x7d:
                    let aMode = getAddressModeReg(code: op2)
                    regA = Byte(regIX & 0x00FF)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD a, IXl", target: 0x0000)
                    tStates = 8

                case 0x86:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIX, offset: offset)
                    let val = system.fetchFrom(adr: adr)
                    let oldA = regA
                    var res = Int(regA) + Int(val)
                    var f = regF & 0x28 // erase all relevant bits
                    if (res > 255) {
                        res = res & 0x00FF
                        f |= UInt8(flagC)
                    }
                    regA = UInt8(res)
                    // deal with flags on a 8 bit result
                    if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if ((res & 0x0F) < (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
//                    if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                    if (oldA & 0x80 == val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " ADD A, (IX+$"+String(toHex(num: offset)) + ")", target: 0x0000)
                    tStates = 19


                case 0x8E:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIX, offset: offset)
                    let val = system.fetchFrom(adr: adr)
                    let oldA = regA
                    var res = Int(regA) + Int(val) + Int(regF & UInt8(flagC))
                    var f = regF & 0x28 // erase all relevant bits
                    if (res > 255) {
                        res = res & 0x00FF
                        f |= UInt8(flagC)
                    }
                    regA = UInt8(res)
                    // deal with flags on a 8 bit result
                    if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if ((res & 0x0F) < (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
//                    if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                    if (oldA & 0x80 == val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " ADC A, (IX+$"+String(toHex(num: offset))+")", target: 0x0000)
                    tStates = 19


                case 0x96:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIX, offset: offset)
                    let val = system.fetchFrom(adr: adr)
                    let oldA = regA
                    var res = Int(regA) - Int(val)
                    var f = regF & 0x28 // erase all relevant bits
                    if (res < 0) {
                        res = res & 0xff
                        f |= UInt8(flagC)
                    }
                    regA = UInt8(res)
                    // deal with flags on a 8 bit result
                    if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if ((res & 0x0F) > (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been reduced
//                    if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                    if (oldA & 0x80 != val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " SUB A, (IX+$"+String(toHex(num: offset)) + ")", target: 0x0000)
                    tStates = 19

                case 0x9E:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIX, offset: offset)
                    let val = system.fetchFrom(adr: adr)
                    let oldA = regA
                    var res = Int(regA) - Int(val) - Int(regF & 0x01)
                    var f = regF & 0x28 // erase all relevant bits
                    if (res < 0) {
                        res = res & 0xff
                        f |= UInt8(flagC)
                    }
                    regA = UInt8(res)
                    // deal with flags on a 8 bit result
                    if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if ((res & 0x0F) > (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been reduced
//                    if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                    if (oldA & 0x80 != val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " SBC A, (IX+$"+String(toHex(num: offset)) + ")", target: 0x0000)
                    tStates = 19

                case 0xA6:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIX, offset: offset)
                    let tmp = system.fetchFrom(adr: adr)
                    doAND(s: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " AND (IX+$"+String(toHex(num: offset))+")", target: 0x0000)
                    tStates = 19

                case 0xAC:
                    let tmp = Byte((regIX >> 8) & 0x00FF)
                    doXOR(s: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " XOR IXh", target: 0x0000)
                    tStates = 8

                case 0xAD:
                    let tmp = Byte(regIX & 0x00FF)
                    doXOR(s: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " XOR IXl", target: 0x0000)
                    tStates = 8

                case 0xAE:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIX, offset: offset)
                    let tmp = system.fetchFrom(adr: adr)
                    doXOR(s: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " XOR (IX+$"+String(toHex(num: offset))+")", target: 0x0000)
                    tStates = 19

                case 0xB6:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIX, offset: offset)
                    let tmp = system.fetchFrom(adr: adr)
                    doOR(s: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " OR (IX+$"+String(toHex(num: offset))+")", target: 0x0000)
                    tStates = 19

                case 0xBC:
                    let tmp = Byte((regIX >> 8) & 0x00FF)
                    doCP(s: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " CP IXh", target: 0x0000)
                    tStates = 8

                case 0xBD:
                    let tmp = Byte(regIX & 0x00FF)
                    doCP(s: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " CP IXl", target: 0x0000)
                    tStates = 8

                case 0xBE:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIX, offset: offset)
                    let tmp = system.fetchFrom(adr: adr)
                    doCP(s: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " CP (IX+$"+String(toHex(num: offset))+")", target: 0x0000)
                    tStates = 19

                case 0xCB:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIX, offset: offset)
                    var tmp = system.fetchFrom(adr: adr)
                    let mode = system.fetch()
//                    let reg = mode & 0x07
                    let bitNum = (mode & 0x38) >> 3
                    var modeName = "SET"
//                    var modeString = "(IX + "+String(offset)+")"
                    let modeLow = mode & 0x0F
                    switch(mode & 0xF0) {
                    case 0x00:      // RRC or RLC
                        if (modeLow > 0x07) {
                            modeName = "RRC"
                            var oldtmp: UInt8
                            oldtmp = tmp
                            tmp = tmp >> 1
                            tmp |= ((oldtmp & 0x01) << 7) // bit 0 moves into bit 7
                            system.setMem(adr: adr, val: tmp)
                            tStates = 23
                            var f = regF & 0x28 // erase all relevant bits
                            f |= oldtmp & 0x01  // bit 0 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        } else {
                            modeName = "RLC"
                            var oldtmp: UInt8
                            oldtmp = tmp
                            tmp = tmp << 1
                            tmp |= ((oldtmp & 0x80) >> 7) // bit 7 moves into bit 0
                            system.setMem(adr: adr, val: tmp)
                            tStates = 23
                            var f = regF & 0x28 // erase all relevant bits
                            f |= ((oldtmp & 0x80) >> 7)   // bit 7 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        }
                    case 0x10:      // RR or RL
                        if (modeLow > 0x07) {
                            modeName = "RR"
                            var oldtmp: UInt8
                            oldtmp = tmp
                            tmp = tmp >> 1
                            tmp |= ((regF & 0x01) << 7) // carry moves into bit 7
                            system.setMem(adr: adr, val: tmp)
                            tStates = 23
                            var f = regF & 0x28 // erase all relevant bits
                            f |= oldtmp & 0x01  // bit 0 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        } else {
                            modeName = "RL"
                            var oldtmp: UInt8
                            oldtmp = tmp
                            tmp = tmp << 1
                            tmp |= regF & 0x01 // carry moves into bit 0
                            system.setMem(adr: adr, val: tmp)
                            tStates = 23
                            var f = regF & 0x28 // erase all relevant bits
                            f |= ((oldtmp & 0x80) >> 7)   // bit 7 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        }
                    case 0x20:      // SRA or SLA
                        if (modeLow > 0x07) {
                            modeName = "SRA"
                            var oldtmp: UInt8
                            oldtmp = tmp
                            tmp = tmp >> 1
                            tmp |= (oldtmp & 0x80)
                            system.setMem(adr: adr, val: tmp)
                            tStates = 23
                            var f = regF & 0x28 // erase all relevant bits
                            f |= oldtmp & 0x01  // bit 0 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        } else {
                            modeName = "SLA"
                            var oldtmp: UInt8
                            oldtmp = tmp
                            tmp = tmp << 1
                            tmp &= 0xfe
                            system.setMem(adr: adr, val: tmp)
                            tStates = 23
                            var f = regF & 0x28 // erase all relevant bits
                            f |= ((oldtmp & 0x80) >> 7)   // bit 7 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        }
                    case 0x30:      // SRL or SLL
                        if (modeLow > 0x07) {
                            modeName = "SRL"
                            var oldtmp: UInt8
                            oldtmp = tmp
                            tmp = tmp >> 1
                            system.setMem(adr: adr, val: tmp)
                            tStates = 23
                            var f = regF & 0x28 // erase all relevant bits
                            f |= oldtmp & 0x01  // bit 0 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        } else {
                            modeName = "SLL"
                            // SLL is undefined behavior
                            setThisOpcode(opc: String(op.hex) + String(op2.hex) + " ===== Undefined Behavior: SLL opcode is unknown ", target: 0x0000)
                            emulationStop = true
                            logger(logmsg: "=== emulationStop SLL")
                        }
                    case 0x40, 0x50, 0x60, 0x70:   // BIT
                        modeName = "BIT"
                        tmp = tmp & UInt8(0x01 << bitNum)
                        var f = regF & 0xBD
                        f |= UInt8(flagH)
                        if (tmp == 0) {
                            f |= UInt8(flagZ)
                        }
                        regF = f
                        tStates = 20
                    case 0x80, 0x90, 0xa0, 0xb0:   // RES
                        modeName = "RES"
                        tmp = resBit(value: tmp, bit: Int(bitNum))
                        system.setMem(adr: adr, val: tmp)
                        tStates = 23
                    default:   // SET
                        // TODO: make sure this is complete
                        tmp = setBit(value: tmp, bit: Int(bitNum))
                        system.setMem(adr: adr, val: tmp)
                        tStates = 23
                    }
                    
                case 0xE1:
                    regIX = system.fetchWordFrom(adr: regSP)
                    regSP = regSP &+ 2
                    setThisOpcode(opc: String(op.hex) + " POP IX", target: 0x0000)
                    tStates = 14
                    
                case 0xE3:
                    let tmpIXLow = system.fetchFrom(adr: regSP)
                    let tmpIXHigh = system.fetchFrom(adr: regSP+1)
                    system.setMem(adr: regSP+1, val: UInt8(regIX >> 8))
                    system.setMem(adr: regSP, val: UInt8(regIX & 0x00ff))
                    regIX = UInt16(UInt16(tmpIXHigh) * 256 + UInt16(tmpIXLow))
                    setThisOpcode(opc: String(op.hex) + " EX (SP), IX", target: 0x0000)
                    tStates = 23

                case 0xE5:
                    regSP = regSP &- 2
                    system.setMemWord(adr: regSP, val: regIX)
                    setThisOpcode(opc: String(op.hex) + " PUSH IX", target: 0x0000)
                    tStates = 15

                case 0xE9:
                    regPC = regIX
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " JP (IX)", target: regPC)
                    tStates = 8

                case 0xF9:
                    regSP = regIX
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD SP, IX", target: regPC)
                    tStates = 10

                default:
                    setThisOpcode(opc: String(op.hex) + " ===== Parser Overrun in DD branch: DD "+String(op2.hex), target: 0x0000)
                    emulationStop = true
                    logger(logmsg: "=== emulationStop DD branch")
                } // end of DD switch

                // ============= END OF DD ===============
                // ====================================================================
                // ====================================================================

            case 0xDE:
                let val = system.fetch()
                let oldA = regA
                var res = Int(regA) - Int(val) - Int(regF & 0x01)
                var f = regF & 0x28 // erase all relevant bits
                if (res < 0) {
                    res = res & 0xff
                    f |= UInt8(flagC)
                }
                regA = UInt8(res)
                // deal with flags on a 8 bit result
                if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                if (res == 0) {f |= UInt8(flagZ)}
                if ((res & 0x0F) > (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been reduced
//                if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                if (oldA & 0x80 != val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                f |= UInt8(flagN)
                regF = f
                setThisOpcode(opc: String(op.hex) + " SBC A, $" + String(val.hex), target: 0x0000)
                tStates = 7

                
            case 0xE0:
                if (regF & UInt8(flagPV) == 0) {
                    regPC = system.fetchWordFrom(adr: regSP)
                    regSP = regSP &+ 2
                    tStates = 11
                } else {
                    tStates = 5
                }
                setThisOpcode(opc: String(op.hex) + " RET PO", target: regPC)


            case 0xE2:
                let adr = system.fetchWord()
                if (regF & UInt8(flagPV) == 0) {
                    regPC = adr
                }
                setThisOpcode(opc: String(op.hex) + " JP PO, $" + String(adr.hex), target: regPC)
                tStates = 10

                
            case 0xE3:
                let tmpH = system.fetchFrom(adr: regSP + 1)
                let tmpL = system.fetchFrom(adr: regSP)
                system.setMem(adr: regSP+1, val: regH)
                system.setMem(adr: regSP, val: regL)
                regH = tmpH
                regL = tmpL
                setThisOpcode(opc: String(op.hex) + " EX (SP), HL", target:0x0000)
                tStates = 19

                
            case 0xE4:
                if (regF & UInt8(flagPV) == 0) {
                    regSP = regSP &- 2
                    let newPC = system.fetchWord()
                    system.setMemWord(adr: regSP, val: regPC)
                    regPC = newPC
                    tStates = 17
                    setThisOpcode(opc: String(op.hex) + " CALL PO, $" + String(regPC.hex), target: regPC)
                } else {
                    let newPC = system.fetchWord()
                    tStates = 10
                    setThisOpcode(opc: String(op.hex) + " CALL PO, $xxxx", target: newPC)
                }

                
            case 0xE6:
                let tmp = system.fetch()
                doAND(s: tmp)
                setThisOpcode(opc: String(op.hex) + " AND $" + String(toHex(num: tmp)), target: 0x0000)
                tStates = 7

                
            case 0xE8:
                if (regF & UInt8(flagPV) == flagPV) {
                    regPC = system.fetchWordFrom(adr: regSP)
                    regSP = regSP &+ 2
                    tStates = 11
                } else {
                    tStates = 5
                }
                setThisOpcode(opc: String(op.hex) + " RET PE", target: regPC)

                
            case 0xE9:
                regPC = getHL()
                setThisOpcode(opc: String(op.hex) + " JP (HL)", target: regPC)
                tStates = 4

                
            case 0xEA:
                let adr = system.fetchWord()
                if (regF & UInt8(flagPV) == flagPV) {
                    regPC = adr
                }
                setThisOpcode(opc: String(op.hex) + " JP PE, $" + String(adr.hex), target: regPC)
                tStates = 10

                
            case 0xEB:
                let tmpD = regD
                let tmpE = regE
                regD = regH
                regE = regL
                regH = tmpD
                regL = tmpE
                setThisOpcode(opc: String(op.hex) + " EX DE, HL", target: 0x0000)
                tStates = 4


            case 0xEC:
                if (regF & UInt8(flagPV) == flagPV) {
                    regSP = regSP &- 2
                    let newPC = system.fetchWord()
                    system.setMemWord(adr: regSP, val: regPC)
                    regPC = newPC
                    tStates = 17
                    setThisOpcode(opc: String(op.hex) + " CALL PE, $" + String(regPC.hex), target: regPC)
                } else {
                    let newPC = system.fetchWord()
                    tStates = 10
                    setThisOpcode(opc: String(op.hex) + " CALL PE, $xxxx", target: newPC)
                }

                
                // ====================================================================
                // ====================================================================
            case 0xED:                              // ============= ED ===============
                let op2 = system.fetch()
                switch(op2){
                case 0x44:
                    let tmp = regA
                    regA = UInt8((Int(~regA) + 1) & 0x00FF)
                    var f = regF & 0x28
                    if ((regA & 0x80) > 0) {f |= UInt8(flagS)}
                    if (regA == 0) {f |= UInt8(flagZ)}
                    if ((regA & 0x0F) < (tmp & 0x0F)) {f |= UInt8(flagH)}
                    if (tmp == 0x80) {f |= UInt8(flagPV)}
                    if (tmp == 0x00) {f |= UInt8(flagC)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " NEG", target: 0x0000)
                    tStates = 8

                case 0x46:
                    IMode = 0
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " IM 0", target: 0x0000)
                    tStates = 8

                case 0x47:
                    regI = regA
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD I, A", target: 0x0000)
                    tStates = 9

                case 0x4F:
                    regR = regA
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD R, A", target: 0x0000)
                    tStates = 9

                case 0x56:
                    IMode = 1
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " IM 1", target: 0x0000)
                    tStates = 8

                case 0x57:
                    regA = regI
                    var f = regF & 0x29 // erase all relevant bits
                    if ((regI & 0x80) > 0) {f |= UInt8(flagS)}
                    if (regI == 0) {f |= UInt8(flagZ)}
                    if (IFF) {f |= UInt8(flagPV)}
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD A, I", target: 0x0000)
                    tStates = 9

                case 0x5E:
                    IMode = 2
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " IM 2", target: 0x0000)
                    tStates = 8

                case 0x5F:
                    regA = regR
                    var f = regF & 0x29 // erase all relevant bits
                    if ((regR & 0x80) > 0) {f |= UInt8(flagS)}
                    if (regR == 0) {f |= UInt8(flagZ)}
                    if (IFF) {f |= UInt8(flagPV)}
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD A, R", target: 0x0000)
                    tStates = 9


                case 0x40, 0x48, 0x50, 0x58, 0x60, 0x68, 0x78:
                    // The contents of Register C are placed on the bottom half (A0 through A7) of the address
                    // bus to select the I/O device at one of 256 possible ports. The contents of Register B are
                    // placed on the top half (A8 through A15) of the address bus at this time
                    let tmp = op2 >> 3
                    let reg = getAddressModeReg(code: tmp)
                    let tmpB = regB
                    let res = system.getIOPort(adr: UInt16(UInt16(tmpB)*256 + UInt16(regC)))
                    setRegByte(name: reg.name, value: res)
                    // deal with flags on a 8 bit result
                    var f = regF & 0x3A // erase all relevant bits, IN forces N & H to 0
                    if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " IN "+reg.name+", (C)", target: 0x0000)
//                    logger(logmsg: "IN "+reg.name+", (C) [0x"+String(tmpB.hex)+String(regC.hex)+"]" + " result: "+String(res)) // DEBUG ONLY
                    tStates = 12

                    
                case 0x41, 0x49, 0x51, 0x59, 0x61, 0x69, 0x79:
                    let tmp = op2 >> 3
                    let reg = getAddressModeReg(code: tmp)
                    system.setIOPort(adr: UInt16(UInt16(regB)*256 + UInt16(regC)), val: reg.value)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " OUT (C), "+reg.name, target: 0x0000)
//                    logger(logmsg: "OUT (C), "+reg.name + " value: "+String(reg.value)) // DEBUG ONLY
                    tStates = 12

                    
                case 0x42, 0x52, 0x62, 0x72:
                    let rg = UInt8(op2 & 0x30) >> 4
                    let dblReg = getDoubleReg(regIndex: rg)
                    let oldHL = getHL()
                    var res = Int(oldHL)
                    res = res - Int(dblReg.value)
                    res = res - Int(regF & UInt8(flagC))
                    var f = regF & 0x28 // erase all relevant bits
                    if (res < 0) {
//                        res = 0xffff + res + 1
                        res = res & 0xffff
                        f |= UInt8(flagC)
                    }
                    setHL(newValue: UInt16(res))
                    // deal with flags on a 16 bit result
                    if ((res & 0x8000) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if ((res & 0x0FFF) > (oldHL & 0x0FFF)) {f |= UInt8(flagH)}  // set H if the top nibble has been reduced
//                    if (Parity(val: uint16(res))) {f |= UInt8(flagPV)}
                    if (oldHL & 0x8000 != dblReg.value & 0x8000 && oldHL & 0x8000 != res & 0x8000) {f |= UInt8(flagPV)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " SBC HL, " + dblReg.name, target: 0x0000)
                    tStates = 15

                case 0x43, 0x53, 0x63, 0x73:
                    let rg = UInt8(op2 & 0x30) >> 4
                    let dblReg = getDoubleReg(regIndex: rg)
                    let adr = system.fetchWord()
                    system.setMemWord(adr: adr, val: dblReg.value)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD (" + String(adr.hex) + "), " + dblReg.name, target: adr)
                    tStates = 20

                case 0x45:
                    regPC = system.fetchWordFrom(adr: regSP)
                    regSP = regSP &+ 2
                    IFF = false     // enable interrupts
                    setThisOpcode(opc: String(op.hex) + " RETN ", target: regPC)
                    tStates = 4
                    
                case 0x4A, 0x5A, 0x6A, 0x7A:
                    let rg = UInt8(op2 & 0x30) >> 4
                    let dblReg = getDoubleReg(regIndex: rg)
                    let oldHL = getHL()
                    var res = Int(oldHL)
                    var val = 0
                    switch (dblReg.name) {
                    case "BC":
                        val = Int(getBC())
                    case "DE":
                        val = Int(getDE())
                    case "HL":
                        val = Int(oldHL)
                    default:
                        val = Int(regSP)
                    }
                    res = res + val
                    res = res + Int(regF & UInt8(flagC))
                    var f = regF & 0x28 // erase all relevant bits
                    if (res > 0xFFFF) {
//                        res = res - 0x10000
                        res = res & 0xFFFF
                        f |= UInt8(flagC)
                    }
                    setHL(newValue: UInt16(res))
                    // deal with flags on a 16 bit result
                    if ((res & 0x8000) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if ((res & 0x0FFF) < (oldHL & 0x0FFF)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
//                    if (Parity(val: uint16(res))) {f |= UInt8(flagPV)}
                    if (oldHL & 0x8000 == val & 0x8000 && oldHL & 0x8000 != res & 0x8000) {f |= UInt8(flagPV)}
                    f &= UInt8(0xFD)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " ADC HL, " + dblReg.name, target: 0x0000)
                    tStates = 15
                    
                case 0x4B, 0x5B, 0x6B, 0x7B:
                    let val = system.fetchWord()
                    var regName: String
                    switch ((op2 & 0x30) >> 4) {
                    case 0:
                        setBC(newValue: system.fetchWordFrom(adr: val))
                        regName = "BC"
                    case 1:
                        setDE(newValue: system.fetchWordFrom(adr: val))
                        regName = "DE"
                    case 2:
                        setHL(newValue: system.fetchWordFrom(adr: val))
                        regName = "HL"
                    default:
                        regSP = system.fetchWordFrom(adr: val)
                        regName = "SP"
                    }
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD " + regName + ", (" + String(val) + ")", target: val)
                    tStates = 20

                case 0x67:
                    var tmpA = regA
                    var tmpMem = system.fetchFrom(adr: getHL())
                    // keep the high nibble of (HL) for later
                    let lowNibbleMem = tmpMem & 0x0f
                    // move high to low nibble in (HL)
                    tmpMem = tmpMem >> 4
                    // copy A low nibble to high (HL) nibble
                    tmpMem = tmpMem | (tmpA << 4)
                    // copy (HL) low nibble to A low nibble
                    tmpA = (tmpA & 0xf0) | lowNibbleMem
                    system.setMem(adr: getHL(), val: tmpMem)
                    regA = tmpA
                    var f = regF & 0x29 // erase all relevant bits - szXhXpnC
                    // deal with flags on a 8 bit result
                    if ((tmpA & 0x80) > 0) {f |= UInt8(flagS)}
                    if (tmpA == 0) {f |= UInt8(flagZ)}
                    if (Parity(val: UInt16(tmpA & 0x00ff))) {f |= UInt8(flagPV)}
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " RRD ", target: 0x0000)
                    tStates = 18

                case 0x6f:
                    var tmpA = regA
                    var tmpMem = system.fetchFrom(adr: getHL())
                    // keep the high nibble of (HL) for later
                    let highNibbleMem = tmpMem >> 4
                    // move low to high nibble in (HL)
                    tmpMem = tmpMem << 4
                    // copy A low nibble to (HL)
                    tmpMem = tmpMem | (tmpA & 0x0f)
                    // copy (HL) high nibble to A low nibble
                    tmpA = (tmpA & 0xf0) | highNibbleMem
                    system.setMem(adr: getHL(), val: tmpMem)
                    regA = tmpA
                    var f = regF & 0x29 // erase all relevant bits - szXhXpnC
                    // deal with flags on a 8 bit result
                    if ((tmpA & 0x80) > 0) {f |= UInt8(flagS)}
                    if (tmpA == 0) {f |= UInt8(flagZ)}
                    if (Parity(val: UInt16(tmpA & 0x00ff))) {f |= UInt8(flagPV)}
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " RLD ", target: 0x0000)
                    tStates = 18

                
                // this is an undocumented IN (C) that does not store the result, only influences flags
                case 0x70:
                    let res = system.getIOPort(adr: UInt16(UInt16(regB)*256 + UInt16(regC)))
                    // deal with flags on a 8 bit result
                    var f = regF & 0x3A // erase all relevant bits, IN forces N & H to 0
                    if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " IN (C)", target: 0x0000)
                    logger(logmsg: "IN (C) [0x"+String(regB.hex)+String(regC.hex)+"]" + " result: "+String(res)) // DEBUG ONLY
                    tStates = 12

                    
                case 0xA0:
                    var tmpBC = getBC()
                    var tmpDE = getDE()
                    var tmpHL = getHL()
                    let tmp = system.fetchFrom(adr: tmpHL)
                    system.setMem(adr: tmpDE, val: tmp)
                    tmpBC = tmpBC &- 1
                    tmpDE = tmpDE &+ 1
                    tmpHL = tmpHL &+ 1
                    setBC(newValue: tmpBC)
                    setDE(newValue: tmpDE)
                    setHL(newValue: tmpHL)
                    var f = regF & 0xE9
                    if (tmpBC != 0) {f |= UInt8(flagPV)}
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LDI", target: 0x0000)
                    tStates = 16
                    
                case 0xA1:
                    var adr = getHL()
                    let tmp = system.fetchFrom(adr: adr)
                    doCP(s: tmp)
                    adr = adr &+ 1
                    setHL(newValue: adr)
                    var tmpBC = getBC()
                    tmpBC = tmpBC &- 1
                    setBC(newValue: tmpBC)
                    var f = regF & 0xFB
                    if (tmpBC != 0) {f |= UInt8(flagPV)}
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " CPI", target: 0x0000)
                    tStates = 16


                case 0xA2:
                    var adr = getHL()
                    let tmpB = regB
                    let res = system.getIOPort(adr: UInt16(UInt16(tmpB)*256 + UInt16(regC)))
                    system.setMem(adr: adr, val: res)
                    adr = adr &+ 1
                    setHL(newValue: adr)
                    regB = regB &- 1
                    // deal with flags on a 8 bit result
                    var f = regF & 0xBD // erase all relevant bits, INI forces N to 1
                    if (res == 0) {f |= UInt8(flagZ)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " INI", target: 0x0000)
//                    logger(logmsg: String(op.hex) + " " + String(op2.hex) + " INI - result: "+String(res)) // DEBUG ONLY
                    tStates = 16
                    
                    
                case 0xA8:
                    var tmpBC = getBC()
                    var tmpDE = getDE()
                    var tmpHL = getHL()
                    let tmp = system.fetchFrom(adr: tmpHL)
                    system.setMem(adr: tmpDE, val: tmp)
                    tmpBC = tmpBC &- 1
                    tmpDE = tmpDE &- 1
                    tmpHL = tmpHL &- 1
                    setBC(newValue: tmpBC)
                    setDE(newValue: tmpDE)
                    setHL(newValue: tmpHL)
                    var f = regF & 0xE9
                    if (tmpBC != 0) {f |= UInt8(flagPV)}
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LDD", target: 0x0000)
                    tStates = 16

                case 0xA9:
                    var adr = getHL()
                    let tmp = system.fetchFrom(adr: adr)
                    doCP(s: tmp)
                    adr = adr &- 1
                    setHL(newValue: adr)
                    var tmpBC = getBC()
                    tmpBC = tmpBC &- 1
                    setBC(newValue: tmpBC)
                    var f = regF & 0xFB
                    if (tmpBC != 0) {f |= UInt8(flagPV)}
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " CPD", target: 0x0000)
                    tStates = 16

                    
                case 0xAA:
                    var adr = getHL()
                    let tmpB = regB
                    let res = system.getIOPort(adr: UInt16(UInt16(tmpB)*256 + UInt16(regC)))
                    system.setMem(adr: adr, val: res)
                    adr = adr &- 1
                    setHL(newValue: adr)
                    regB = regB &- 1
                    // deal with flags on a 8 bit result
                    var f = regF & 0xBD // erase all relevant bits, IND forces N to 1
                    if (res == 0) {f |= UInt8(flagZ)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " IND", target: 0x0000)
                    logger(logmsg: String(op.hex) + " " + String(op2.hex) + " IND - result: "+String(res)) // DEBUG ONLY
                    tStates = 16

                    
                case 0xB0:
                    // ##### fast implementation that does not return until the loop is done
                    // - this will inhibit any interrupts while the loop is running
                    // - the flags will only be approximately correct on exit
                    var tmpBC = getBC()
                    var tmpDE = getDE()
                    var tmpHL = getHL()
                    while tmpBC != 0 {
                        let tmp = system.fetchFrom(adr: tmpHL)
                        system.setMem(adr: tmpDE, val: tmp)
                        tmpBC = tmpBC &- 1
                        tmpDE = tmpDE &+ 1
                        tmpHL = tmpHL &+ 1
                        tStates += 21
                    }
                    setBC(newValue: tmpBC)
                    setDE(newValue: tmpDE)
                    setHL(newValue: tmpHL)
                    regF = regF & 0xE9
                    tStates -= 5    // fix tStates overcount from the last while loop iteration

                    // ##### below is the clean-but-slow implementation of LDIR
//                    var tmpBC = getBC()
//                    var tmpDE = getDE()
//                    var tmpHL = getHL()
//                    let tmp = system.fetchFrom(adr: tmpHL)
//                    system.setMem(adr: tmpDE, val: tmp)
//                    tmpBC = tmpBC &- 1
//                    tmpDE = tmpDE &+ 1
//                    tmpHL = tmpHL &+ 1
//                    setBC(newValue: tmpBC)
//                    setDE(newValue: tmpDE)
//                    setHL(newValue: tmpHL)
//                    var f = regF & 0xE9
//                    if (tmpBC == 0) {
//                        tStates = 16
//                    } else {
//                        f |= UInt8(flagPV)
//                        tStates = 21
//                        // set PC back to repeat this opcode
//                        regPC = regPC - 2
//                    }
//                    regF = f

                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LDIR", target: 0x0000)
                    
                // TODO: "If BC is set to 0 before instruction execution, the instruction loops through 64 KB if no match is found"
                case 0xB1:
                    // ##### fast implementation that does not return until the loop is done
//                    var adr = getHL()
//                    var tmpBC = getBC()
//                    var f = regF & 0xFB
//                    while tmpBC != 0 && (Int(f) & flagZ) == 0 {
//                        let tmp = system.fetchFrom(adr: adr)
//                        doCP(s: tmp)
//                        f = regF & 0xFB
//                        adr = adr &+ 1
//                        tmpBC = tmpBC &- 1
//                        tStates += 21
//                    }
//                    setHL(newValue: adr)
//                    setBC(newValue: tmpBC)
//                    regF = regF & 0xFB
//                    tStates -= 5    // fix tStates overcount from the last while loop iteration
//                    regF = f
                    
                    // ##### below is the clean-but-slow implementation of LDDR
                    var adr = getHL()
                    let tmp = system.fetchFrom(adr: adr)
                    doCP(s: tmp)
                    adr = adr &+ 1
                    setHL(newValue: adr)
                    var tmpBC = getBC()
                    tmpBC = tmpBC &- 1
                    setBC(newValue: tmpBC)
                    var f = regF & 0xFB
                    if (tmpBC == 0 || (Int(f) & flagZ) != 0) {
                        tStates = 16
                    } else {
                        f |= UInt8(flagPV)
                        tStates = 21
                        // set PC back to repeat this opcode
                        regPC = regPC - 2
                    }
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " CPIR", target: 0x0000)

                    
                case 0xB2:
                    var adr = getHL()
                    let tmpB = regB
                    let res = system.getIOPort(adr: UInt16(UInt16(tmpB)*256 + UInt16(regC)))
                    system.setMem(adr: adr, val: res)
                    adr = adr &+ 1
                    setHL(newValue: adr)
                    regB = regB &- 1
                    // deal with flags on a 8 bit result
                    var f = regF & 0xBD // erase all relevant bits, INI forces N to 1
                    if (res == 0) {f |= UInt8(flagZ)}
                    f |= UInt8(flagN)
                    regF = f
                    if (tmpB == 0) {
                        tStates = 16
                    } else {
                        tStates = 21
                        // set PC back to repeat this opcode
                        regPC = regPC - 2
                    }
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " INIR", target: 0x0000)
//                    logger(logmsg: String(op.hex) + " " + String(op2.hex) + " INIR - result: "+String(res)) // DEBUG ONLY

                    
                case 0xB8:
                    // ##### fast implementation that does not return until the loop is done
                    // - this will inhibit any interrupts while the loop is running
                    // - the flags will only be approcimately correct on exit
                    var tmpBC = getBC()
                    var tmpDE = getDE()
                    var tmpHL = getHL()
                    while tmpBC != 0 {
                        let tmp = system.fetchFrom(adr: tmpHL)
                        system.setMem(adr: tmpDE, val: tmp)
                        tmpBC = tmpBC &- 1
                        tmpDE = tmpDE &- 1
                        tmpHL = tmpHL &- 1
                        tStates += 21
                    }
                    setBC(newValue: tmpBC)
                    setDE(newValue: tmpDE)
                    setHL(newValue: tmpHL)
                    regF = regF & 0xE9
                    tStates -= 5    // fix tStates overcount from the last while loop iteration

                    // ##### below is the clean-but-slow implementation of LDDR
//                    var tmpBC = getBC()
//                    var tmpDE = getDE()
//                    var tmpHL = getHL()
//                    let tmp = system.fetchFrom(adr: tmpHL)
//                    system.setMem(adr: tmpDE, val: tmp)
//                    tmpBC = tmpBC &- 1
//                    tmpDE = tmpDE &- 1
//                    tmpHL = tmpHL &- 1
//                    setBC(newValue: tmpBC)
//                    setDE(newValue: tmpDE)
//                    setHL(newValue: tmpHL)
//                    var f = regF & 0xE9
//                    if (tmpBC == 0) {
//                        tStates = 16
//                    } else {
//                        f |= UInt8(flagPV)
//                        tStates = 21
//                        // set PC back to repeat this opcode
//                        regPC = regPC - 2
//                    }
//                    regF = f

                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LDDR", target: 0x0000)

                    
                // TODO: "If BC is set to 0 before instruction execution, the instruction loops through 64 KB if no match is found"
                case 0xB9:
                    // ##### fast implementation that does not return until the loop is done
//                    var adr = getHL()
//                    var tmpBC = getBC()
//                    var f = regF & 0xFB
//                    while tmpBC != 0 && (Int(f) & flagZ) == 0 {
//                        let tmp = system.fetchFrom(adr: adr)
//                        doCP(s: tmp)
//                        f = regF & 0xFB
//                        adr = adr &- 1
//                        tmpBC = tmpBC &- 1
//                        tStates += 21
//                    }
//                    setHL(newValue: adr)
//                    setBC(newValue: tmpBC)
//                    regF = regF & 0xFB
//                    tStates -= 5    // fix tStates overcount from the last while loop iteration
//                    regF = f
                    
                    // ##### below is the clean-but-slow implementation of CPDR
                    var adr = getHL()
                    let tmp = system.fetchFrom(adr: adr)
                    doCP(s: tmp)
                    adr = adr &- 1
                    setHL(newValue: adr)
                    var tmpBC = getBC()
                    tmpBC = tmpBC &- 1
                    setBC(newValue: tmpBC)
                    var f = regF & 0xFB
                    if (tmpBC == 0 || (Int(f) & flagZ) != 0) {
                        tStates = 16
                    } else {
                        f |= UInt8(flagPV)
                        tStates = 21
                        // set PC back to repeat this opcode
                        regPC = regPC - 2
                    }
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " CPDR", target: 0x0000)

                    
                case 0xBA:
                    var adr = getHL()
                    let tmpB = regB
                    let res = system.getIOPort(adr: UInt16(UInt16(tmpB)*256 + UInt16(regC)))
                    system.setMem(adr: adr, val: res)
                    adr = adr &- 1
                    setHL(newValue: adr)
                    regB = regB &- 1
                    // deal with flags on a 8 bit result
                    var f = regF & 0xBD // erase all relevant bits, INI forces N to 1
                    if (res == 0) {f |= UInt8(flagZ)}
                    f |= UInt8(flagN)
                    regF = f
                    if (tmpB == 0) {
                        tStates = 16
                    } else {
                        tStates = 21
                        // set PC back to repeat this opcode
                        regPC = regPC - 2
                    }
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " INDR", target: 0x0000)
//                    logger(logmsg: String(op.hex) + " " + String(op2.hex) + " INDR - result: "+String(res)) // DEBUG ONLY

                    
                default:
                    setThisOpcode(opc: String(op.hex) + " ===== Parser Overrun in ED branch: ED "+String(op2.hex), target: 0x0000)
                    emulationStop = true
                    logger(logmsg: "=== emulationStop ED branch")
                } // end of ED switch


                // ============= END OF ED ===============
                // ====================================================================
                // ====================================================================


            case 0xEE:
                let tmp = system.fetch()
                doXOR(s: tmp)
                setThisOpcode(opc: String(op.hex) + " XOR $" + String(toHex(num: tmp)), target: 0x0000)
                tStates = 7

                
            case 0xF0:
                if (regF & UInt8(flagS) == 0) {
                    regPC = system.fetchWordFrom(adr: regSP)
                    regSP = regSP &+ 2
                    tStates = 11
                } else {
                    tStates = 5
                }
                setThisOpcode(opc: String(op.hex) + " RET P", target: 0x0000)

                
            case 0xF2:
                let adr = system.fetchWord()
                if (regF & UInt8(flagS) == 0) {
                    regPC = adr
                }
                setThisOpcode(opc: String(op.hex) + " JP P, $" + String(adr.hex), target: regPC)
                tStates = 10

                
            case 0xF3:
                IFF = true
                setThisOpcode(opc: String(op.hex) + " DI", target: 0x0000)
                tStates = 4

                
            case 0xF4:
                if (regF & UInt8(flagS) == 0) {
                    regSP = regSP &- 2
                    let newPC = system.fetchWord()
                    system.setMemWord(adr: regSP, val: regPC)
                    regPC = newPC
                    tStates = 17
                    setThisOpcode(opc: String(op.hex) + " CALL P, $" + String(regPC.hex), target: regPC)
                } else {
                    let newPC = system.fetchWord()
                    tStates = 10
                    setThisOpcode(opc: String(op.hex) + " CALL P, $xxxx", target: newPC)
                }


            case 0xF6:
                let tmp = system.fetch()
                doOR(s: tmp)
                setThisOpcode(opc: String(op.hex) + " OR $" + String(toHex(num: tmp)), target: 0x0000)
                tStates = 7

                
            case 0xF8:
                if (regF & UInt8(flagS) == flagS) {
                    regPC = system.fetchWordFrom(adr: regSP)
                    regSP = regSP &+ 2
                    tStates = 11
                } else {
                    tStates = 5
                }
                setThisOpcode(opc: String(op.hex) + " RET M", target: regPC)

                
            case 0xF9:
                regSP = getHL()
                setThisOpcode(opc: String(op.hex) + " LD SP, HL", target: 0x0000)
                tStates = 6

                
            case 0xFA:
                let adr = system.fetchWord()
                if (regF & UInt8(flagS) == flagS) {
                    regPC = adr
                }
                setThisOpcode(opc: String(op.hex) + " JP M, $" + String(adr.hex), target: regPC)
                tStates = 10

                
            case 0xFB:
                IFF = false
                setThisOpcode(opc: String(op.hex) + " EI", target: 0x0000)
                tStates = 4


            case 0xFC:
                if (regF & UInt8(flagS) == flagS) {
                    regSP = regSP &- 2
                    let newPC = system.fetchWord()
                    system.setMemWord(adr: regSP, val: regPC)
                    regPC = newPC
                    tStates = 17
                    setThisOpcode(opc: String(op.hex) + " CALL M, $" + String(regPC.hex), target: regPC)
                } else {
                    let newPC = system.fetchWord()
                    tStates = 10
                    setThisOpcode(opc: String(op.hex) + " CALL M, $xxxx", target: newPC)
                }


                // ====================================================================
                // ====================================================================
            case 0xFD:                              // ============= FD ===============
                let op2 = system.fetch()
                switch(op2) {

                case 0x09, 0x19, 0x29, 0x39:
                    let rg = UInt8(op2 & 0x30) >> 4
                    let dblReg = getDoubleReg(regIndex: rg) // if this returns HL we we use IX instead
                    let oldIY = regIY
                    var res = Int(oldIY)
                    switch (dblReg.name) {
                    case "BC":
                        res = res + Int(getBC())
                    case "DE":
                        res = res + Int(getDE())
                    case "HL":
                        res = res + Int(regIY)
                    default:
                        res = res + Int(regSP)
                    }
                    var f = regF & 0xEC // erase all relevant bits
                    if (res > 0xFFFF) {
//                        res = res - 0x10000
                        res = res & 0xFFFF
                        f |= UInt8(flagC)
                    }
                    regIY = UInt16(res)
                    // deal with flags on a 16 bit result
                    if ((res & 0x0FFF) < (oldIY & 0x0FFF)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " ADD IY, " + dblReg.name, target: 0x0000)
                    tStates = 15

                case 0x21:
                    let tmp = system.fetchWord()
                    regIY = tmp
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD IY, $" + String(toHexWord(num: tmp)), target: 0x0000)
                    tStates = 14

                case 0x22:
                    let adr = system.fetchWord()
                    system.setMemWord(adr: adr, val: regIY)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD (" + String(toHexWord(num: adr)) + "), IY", target: adr)
                    tStates = 20

                case 0x23:
                    regIY = rollOverUp(val: Int(regIY)+1)
                    setThisOpcode(opc: String(op.hex) + " INC IY", target: 0x0000)
                    tStates = 10

                case 0x26:
                    let val = system.fetch()
                    regIY = (regIY & 0x00FF) | UInt16(Int(val) << 8)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD IYh, $" + String(toHex(num: val)), target: 0x0000)
                    tStates = 11

                case 0x2A:
                    let adr = system.fetchWord()
                    let tmp = system.fetchWordFrom(adr: adr)
                    regIY = tmp
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD IY, (" + String(toHexWord(num: adr)) + ")", target: adr)
                    tStates = 20
                    
                case 0x2B:
                    regIY = rollOver(val: Int(regIY)-1)
                    setThisOpcode(opc: String(op.hex) + " DEC IY", target: 0x0000)
                    tStates = 10

                case 0x2C:
                    var val = UInt8(regIY & 0x00FF)
                    val = doINC(s: val)
                    if (val == 0) {
                        regF |= UInt8(flagPV)
                    }
                    regIY = (regIY & 0xFF00) | UInt16(val)
                    setThisOpcode(opc: String(op.hex) + " INC IYl", target: 0x0000)
                    tStates = 8

                case 0x2D:
                    var val = UInt8(regIY & 0x00FF)
                    val = doDEC(s: val)
                    if (val == 255) {
                        regF |= UInt8(flagPV)
                    }
                    regIY = (regIY & 0xFF00) | UInt16(val)
                    setThisOpcode(opc: String(op.hex) + " DEC IYl", target: 0x0000)
                    tStates = 8

                case 0x2E:
                    let val = system.fetch()
                    regIY = (regIY & 0xFF00) | UInt16(Int(val))
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD IYl, $" + String(toHex(num: val)), target: 0x0000)
                    tStates = 11

                case 0x34:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIY, offset: offset)
                    let tmp = system.fetchFrom(adr: adr)
                    let res = doINC(s: tmp)
                    system.setMem(adr: adr, val: res)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " INC (IY+$"+String(toHex(num: offset))+")", target: 0x0000)
                    tStates = 23

                case 0x35:
                    let offset = system.fetch()
//                    logger(logmsg: "DEC (IY+offset) offset: " + String(offset.hex))
                    let adr = addOffset(adr: regIY, offset: offset)
                    let tmp = system.fetchFrom(adr: adr)
                    let res = doDEC(s: tmp)
                    system.setMem(adr: adr, val: res)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " DEC (IY+$"+String(toHex(num: offset))+")", target: 0x0000)
                    tStates = 23

                case 0x36:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIY, offset: offset)
                    let tmp = system.fetch()
                    system.setMem(adr: adr, val: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD (IY+$"+String(toHex(num: offset)) + "), $" + String(toHex(num: tmp)), target: 0x0000)
                    tStates = 19

                case 0x46, 0x4E, 0x56, 0x5E, 0x66, 0x6E, 0x7E:
                    let aMode = getAddressModeReg(code: op2 >> 3)
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIY, offset: offset)
                    setRegByte(name: aMode.name, value: system.fetchFrom(adr: adr))
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD "+aMode.name+", (IY+$"+String(toHex(num: offset)) + ")", target: 0x0000)
                    tStates = 19
                    
                case 0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x67:
                    let aMode = getUndocumentedAddressModeReg(code: op2, xory: "y")
                    regIY = (regIY & 0x00FF) | UInt16(Int(aMode.value) << 8)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD IYh, " + aMode.name, target: 0x0000)
                    tStates = 8
                    
                case 0x68, 0x69, 0x6A, 0x6B, 0x6C, 0x6D, 0x6F:
                    let aMode = getUndocumentedAddressModeReg(code: op2, xory: "y")
                    regIY = (regIY & 0xFF00) | UInt16(aMode.value)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD IYl, " + aMode.name, target: 0x0000)
                    tStates = 8

                case 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x77:
                    let aMode = getAddressModeReg(code: op2)
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIY, offset: offset)
                    let tmp = aMode.value
                    system.setMem(adr: adr, val: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD (IY+$"+String(toHex(num: offset)) + "), " + aMode.name, target: 0x0000)
                    tStates = 19

                case 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x87:
                    let aMode = getUndocumentedAddressModeReg(code: op2, xory: "y")
                    let oldA = regA
                    var res = Int(regA) + Int(aMode.value)
                    var f = regF & 0x28 // erase all relevant bits
                    if (res > 255) {
                        res = res & 0x00FF
                        f |= UInt8(flagC)
                    }
                    regA = UInt8(res)
                    // deal with flags on a 8 bit result
                    if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if ((res & 0x0F) < (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
                    if (oldA & 0x80 == aMode.value & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " ADD A, " + aMode.name, target: 0x0000)
                    tStates = 9

                case 0x86:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIY, offset: offset)
                    let val = system.fetchFrom(adr: adr)
                    let oldA = regA
                    var res = Int(regA) + Int(val)
                    var f = regF & 0x28 // erase all relevant bits
                    if (res > 255) {
                        res = res & 0x00FF
                        f |= UInt8(flagC)
                    }
                    regA = UInt8(res)
                    // deal with flags on a 8 bit result
                    if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if ((res & 0x0F) < (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
//                    if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                    if (oldA & 0x80 == val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " ADD A, (IY+$"+String(toHex(num: offset)) + ")", target: 0x0000)
                    tStates = 19
                    
                case 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8F:
                    let aMode = getUndocumentedAddressModeReg(code: op2, xory: "y")
                    let oldA = regA
                    var res = Int(regA) + Int(aMode.value) + Int(regF & UInt8(flagC))
                    var f = regF & 0x28 // erase all relevant bits
                    if (res > 255) {
                        res = res & 0x00FF
                        f |= UInt8(flagC)
                    }
                    regA = UInt8(res)
                    // deal with flags on a 8 bit result
                    if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if ((res & 0x0F) < (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
                    if (oldA & 0x80 == aMode.value & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " ADC A, " + aMode.name, target: 0x0000)
                    tStates = 8

                case 0x8E:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIY, offset: offset)
                    let val = system.fetchFrom(adr: adr)
                    let oldA = regA
                    var res = Int(regA) + Int(val) + Int(regF & UInt8(flagC))
                    var f = regF & 0x28 // erase all relevant bits
                    if (res > 255) {
                        res = res & 0x00FF
                        f |= UInt8(flagC)
                    }
                    regA = UInt8(res)
                    // deal with flags on a 8 bit result
                    if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if ((res & 0x0F) < (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been increased
//                    if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                    if (oldA & 0x80 == val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " ADC A, (IY+$"+String(toHex(num: offset))+")", target: 0x0000)
                    tStates = 19 

                case 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x97:
                    let aMode = getUndocumentedAddressModeReg(code: op2, xory: "y")
                    let oldA = regA
                    var res = Int(regA) - Int(aMode.value)
                    var f = regF & 0x28 // erase all relevant bits
                    if (res < 0) {
                        res = res & 0xff
                        f |= UInt8(flagC)
                    }
                    regA = UInt8(res)
                    // deal with flags on a 8 bit result
                    if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if ((res & 0x0F) > (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been reduced
                    if (oldA & 0x80 != aMode.value & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " SUB A, " + aMode.name, target: 0x0000)
                    tStates = 8
                    
                case 0x96:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIY, offset: offset)
                    let val = system.fetchFrom(adr: adr)
                    let oldA = regA
                    var res = Int(regA) - Int(val)
                    var f = regF & 0x28 // erase all relevant bits
                    if (res < 0) {
                        res = res & 0xff
                        f |= UInt8(flagC)
                    }
                    regA = UInt8(res)
                    // deal with flags on a 8 bit result
                    if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if ((res & 0x0F) > (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been reduced
//                    if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                    if (oldA & 0x80 != val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " SUB A, (IY+$"+String(toHex(num: offset)) + ")", target: 0x0000)
                    tStates = 19

                case 0x98, 0x99, 0x9A, 0x9B, 0x9C, 0x9D, 0x9F:
                    let aMode = getUndocumentedAddressModeReg(code: op2, xory: "y")
                    let oldA = regA
                    var res = Int(regA) - Int(aMode.value) - Int(regF & 0x01)
                    var f = regF & 0x28 // erase all relevant bits
                    if (res < 0) {
                        res = res & 0xff
                        f |= UInt8(flagC)
                    }
                    regA = UInt8(res)
                    // deal with flags on a 8 bit result
                    if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if ((res & 0x0F) > (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been reduced
                    if (oldA & 0x80 != aMode.value & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " SBC A, " + aMode.name, target: 0x0000)
                    tStates = 8

                case 0x9E:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIY, offset: offset)
                    let val = system.fetchFrom(adr: adr)
                    let oldA = regA
                    var res = Int(regA) - Int(val) - Int(regF & 0x01)
                    var f = regF & 0x28 // erase all relevant bits
                    if (res < 0) {
                        res = res & 0xff
                        f |= UInt8(flagC)
                    }
                    regA = UInt8(res)
                    // deal with flags on a 8 bit result
                    if ((res & 0x80) > 0) {f |= UInt8(flagS)}
                    if (res == 0) {f |= UInt8(flagZ)}
                    if ((res & 0x0F) > (oldA & 0x0F)) {f |= UInt8(flagH)}  // set H if the top nibble has been reduced
//                    if (Parity(val: UInt16(res & 0x00ff))) {f |= UInt8(flagPV)}
                    if (oldA & 0x80 != val & 0x80 && oldA & 0x80 != res & 0x80) {f |= UInt8(flagPV)}
                    f |= UInt8(flagN)
                    regF = f
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " SBC A, (IY+$"+String(toHex(num: offset)) + ")", target: 0x0000)
                    tStates = 19

                case 0xA6:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIY, offset: offset)
                    let tmp = system.fetchFrom(adr: adr)
                    doAND(s: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " AND (IY+$"+String(toHex(num: offset))+")", target: 0x0000)
                    tStates = 19

                case 0xAE:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIY, offset: offset)
                    let tmp = system.fetchFrom(adr: adr)
                    doXOR(s: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " XOR (IY+$"+String(toHex(num: offset))+")", target: 0x0000)
                    tStates = 19

                case 0xB6:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIY, offset: offset)
                    let tmp = system.fetchFrom(adr: adr)
                    doOR(s: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " OR (IY+$"+String(toHex(num: offset))+")", target: 0x0000)
                    tStates = 19

                case 0xBE:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIY, offset: offset)
                    let tmp = system.fetchFrom(adr: adr)
                    doCP(s: tmp)
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " CP (IY+$"+String(toHex(num: offset))+")", target: 0x0000)
                    tStates = 19

                case 0xCB:
                    let offset = system.fetch()
                    let adr = addOffset(adr: regIY, offset: offset)
                    var tmp = system.fetchFrom(adr: adr)
                    let mode = system.fetch()
                    let reg = mode & 0x07
                    let bitNum = (mode & 0x38) >> 3
                    var modeName = "SET"
                    var modeString = "(IY + "+String(offset)+")"
                    let modeLow = mode & 0x0F
                    switch(mode & 0xF0) {
                    case 0x00:      // RRC or RLC
                        if (modeLow > 0x07) {
                            modeName = "RRC"
                            var oldtmp: UInt8
                            oldtmp = tmp
                            tmp = tmp >> 1
                            tmp |= ((oldtmp & 0x01) << 7) // bit 0 moves into bit 7
                            system.setMem(adr: adr, val: tmp)
                            tStates = 23
                            var f = regF & 0x28 // erase all relevant bits
                            f |= oldtmp & 0x01  // bit 0 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        } else {
                            modeName = "RLC"
                            var oldtmp: UInt8
                            oldtmp = tmp
                            tmp = tmp << 1
                            tmp |= ((oldtmp & 0x80) >> 7) // bit 7 moves into bit 0
                            system.setMem(adr: adr, val: tmp)
                            tStates = 23
                            var f = regF & 0x28 // erase all relevant bits
                            f |= ((oldtmp & 0x80) >> 7)   // bit 7 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        }
                    case 0x10:      // RR or RL
                        if (modeLow > 0x07) {
                            modeName = "RR"
                            var oldtmp: UInt8
                            oldtmp = tmp
                            tmp = tmp >> 1
                            tmp |= ((regF & 0x01) << 7) // carry moves into bit 7
                            system.setMem(adr: adr, val: tmp)
                            tStates = 23
                            var f = regF & 0x28 // erase all relevant bits
                            f |= oldtmp & 0x01  // bit 0 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        } else {
                            modeName = "RL"
                            var oldtmp: UInt8
                            oldtmp = tmp
                            tmp = tmp << 1
                            tmp |= regF & 0x01 // carry moves into bit 0
                            system.setMem(adr: adr, val: tmp)
                            tStates = 23
                            var f = regF & 0x28 // erase all relevant bits
                            f |= ((oldtmp & 0x80) >> 7)   // bit 7 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        }
                    case 0x20:      // SRA or SLA
                        if (modeLow > 0x07) {
                            modeName = "SRA"
                            var oldtmp: UInt8
                            oldtmp = tmp
                            tmp = tmp >> 1
                            tmp |= (oldtmp & 0x80)
                            system.setMem(adr: adr, val: tmp)
                            tStates = 23
                            var f = regF & 0x28 // erase all relevant bits
                            f |= oldtmp & 0x01  // bit 0 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        } else {
                            modeName = "SLA"
                            var oldtmp: UInt8
                            oldtmp = tmp
                            tmp = tmp << 1
                            tmp &= 0xfe
                            system.setMem(adr: adr, val: tmp)
                            tStates = 23
                            var f = regF & 0x28 // erase all relevant bits
                            f |= ((oldtmp & 0x80) >> 7)   // bit 7 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        }
                    case 0x30:      // SRL or SLL
                        if (modeLow > 0x07) {
                            modeName = "SRL"
                            var oldtmp: UInt8
                            oldtmp = tmp
                            tmp = tmp >> 1
                            system.setMem(adr: adr, val: tmp)
                            tStates = 23
                            var f = regF & 0x28 // erase all relevant bits
                            f |= oldtmp & 0x01  // bit 0 sets carry flag
                            if ((tmp & 0x80) > 0) {f |= UInt8(flagS)}
                            if (tmp == 0) {f |= UInt8(flagZ)}
                            if (Parity(val: UInt16(tmp & 0x00ff))) {f |= UInt8(flagPV)}
                            regF = f
                        } else {
                            modeName = "SLL"
                            // TODO - finish SLL, SLA, SRA, RL, RR, RLC, RRC
                            emulationStop = true
                            logger(logmsg: "=== emulationStop CB SLL branch")
                        }
                    case 0x40, 0x50, 0x60, 0x70:   // BIT
//    S is unknown.
//    Z is set if specified bit is 0; otherwise, it is reset.
//    H is set.
//    P/V is unknown.
//    N is reset.
//    C is not affected.
//    SZXHXPNC - 10101101 -> 0xAD
                        modeName = "BIT "+String(bitNum)+","
                        tmp = tmp & UInt8(0x01 << bitNum)
                        var f = regF & 0xAD
                        f |= UInt8(flagH)
                        if (tmp == 0) {
                            f |= UInt8(flagZ)
                        }
                        regF = f
                        tStates = 20
                    case 0x80, 0x90, 0xa0, 0xb0:   // RES
                        modeName = "RES "+String(bitNum)+","
                        tmp = resBit(value: tmp, bit: Int(bitNum))
                        system.setMem(adr: adr, val: tmp)
                        tStates = 23
                    default:   // SET
                        // TODO: make sure this is complete
                        modeName = "SET "+String(bitNum)+","
                        tmp = setBit(value: tmp, bit: Int(bitNum))
                        system.setMem(adr: adr, val: tmp)
                        tStates = 23
                    }
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " " + modeName + " "+modeString+")", target: 0x0000)
                    
                case 0xE1:
                    regIY = system.fetchWordFrom(adr: regSP)
                    regSP = regSP &+ 2
                    setThisOpcode(opc: String(op.hex) + " POP IY", target: 0x0000)
                    tStates = 14

                case 0xE3:
                    let tmpIYLow = system.fetchFrom(adr: regSP)
                    let tmpIYHigh = system.fetchFrom(adr: regSP+1)
                    system.setMem(adr: regSP+1, val: UInt8(regIY >> 8))
                    system.setMem(adr: regSP, val: UInt8(regIY & 0x00ff))
                    regIY = UInt16(UInt16(tmpIYHigh) * 256 + UInt16(tmpIYLow))
                    setThisOpcode(opc: String(op.hex) + " EX (SP), IY", target: 0x0000)
                    tStates = 23

                case 0xE5:
                    regSP = regSP &- 2
                    system.setMemWord(adr: regSP, val: regIY)
                    setThisOpcode(opc: String(op.hex) + " PUSH IY", target: 0x0000)
                    tStates = 15

                case 0xE9:
                    regPC = regIY
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " JP (IY)", target: 0x0000)
                    tStates = 8

                case 0xF9:
                    regSP = regIY
                    setThisOpcode(opc: String(op.hex) + " " + String(op2.hex) + " LD SP, IY", target: 0x0000)
                    tStates = 10

                default:
                    setThisOpcode(opc: String(op.hex) + " ===== Parser Overrun in FD branch: FD "+String(op2.hex), target: 0x0000)
                    emulationStop = true
                    logger(logmsg: "=== emulationStop FD branch")
                } // end of FD switch

                // ============= END OF FD ===============
                // ====================================================================
                // ====================================================================

            case 0xFE:
                let tmp = system.fetch()
                doCP(s: tmp)
                setThisOpcode(opc: String(op.hex) + " CP $" + String(toHex(num: tmp)), target: 0x0000)
                tStates = 7
                
                

            default:
                setThisOpcode(opc: String(op.hex) + " ===== Parser Overrun main branch", target: 0x0000)
                emulationStop = true
                logger(logmsg: "=== emulationStop main branch")
            } // end of main switch statement
        }

        return tStates
    }
}
