//
//  Renderer.swift
//  animation-test-1
//
//  Created by Thomas Sturm on 2/24/22.
//  Distributed under Attribution-NonCommercial-ShareAlike 4.0 (CC BY-NC-SA 4.0)
//

import Foundation
import MetalKit
import simd


struct VertexUniforms {
    var viewProjectionMatrix: float4x4
    var modelMatrix: float4x4
    var normalMatrix: float3x3
}

struct FragmentUniforms {
    var cameraWorldPosition = SIMD3<Float>(0, 0, 0)
    var ambientLightColor = SIMD3<Float>(0, 0, 0)
    var specularColor = SIMD3<Float>(1, 1, 1)
    var specularPower = Float(1)
    var light0 = Light()
    var light1 = Light()
    var light2 = Light()
}

// allow powers on Int types
extension Int {
    func pow(toPower: Int) -> Int {
        guard toPower > 0 else { return 1 }
        return Array(repeating: self, count: toPower).reduce(1, *)
    }
}

// extract the ASCII value of a character
// usage: "abc".asciiValues  // [97, 98, 99]
extension StringProtocol {
    var asciiValues: [UInt8] { compactMap(\.asciiValue) }
}

struct Light {
    var worldPosition = SIMD3<Float>(0, 0, 0)
    var color = SIMD3<Float>(0, 0, 0)
}

class Material {
    var specularColor = SIMD3<Float>(1, 1, 1)
    var specularPower = Float(1)
    var baseColorTexture: MTLTexture?
}

class Node {
    var name: String
    weak var parent: Node?
    var children = [Node]()
    var modelMatrix = matrix_identity_float4x4
    var mesh: MTKMesh?
    var material = Material()
    init(name: String) {
        self.name = name
    }
    func nodeNamedRecursive(_ name: String) -> Node? {
        for node in children {
            if node.name == name {
                return node
            } else if let matchingGrandchild = node.nodeNamedRecursive(name) {
                return matchingGrandchild
            }
        }
        return nil
    }
}

class Scene {
    var rootNode = Node(name: "Root")
    var ambientLightColor = SIMD3<Float>(0, 0, 0)
    var lights = [Light]()
    func nodeNamed(_ name: String) -> Node? {
        if rootNode.name == name {
            return rootNode
        } else {
            return rootNode.nodeNamedRecursive(name)
        }
    }
}

var screen = Node(name: "Screen")
var voxel = Node(name: "Voxel")


class Renderer: NSObject, MTKViewDelegate {
    let device: MTLDevice
    var vertexDescriptor: MDLVertexDescriptor
    let scene: Scene
    var meshes: [MTKMesh] = []
    var renderPipelineFlat: MTLRenderPipelineState!
    var renderPipelineLighting: MTLRenderPipelineState!
    let commandQueue: MTLCommandQueue
    let depthStencilState: MTLDepthStencilState
    var baseColorTexture: MTLTexture?
    let samplerState: MTLSamplerState
    
    var viewMatrix = matrix_identity_float4x4
    var projectionMatrix = matrix_identity_float4x4
    var cameraWorldPosition = SIMD3<Float>(0, 0, 2)
    var aspectRatio: Float

    var colors = [CGColor](repeating: CGColor(red:0,green:0,blue:0,alpha:255), count: 16)
    var colorTriples = [UInt8](repeating: UInt8(0), count: 48)
    
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
    }
 
    var borderModified: Bool = false
    
    let spectrum = Spectrum()
    

    // create a new empty texture
    let textureSizeX:Int = 320
    let textureSizeY:Int = 256
    let bytesPerRow = 4 * Int(320)
    let bitsPerComponent = Int(8)
    var rawData = [UInt8](repeating: 0, count: Int(320) * Int(256) * 4)

    // voxel texture
    var rawTexture = [UInt8](repeating: 0, count: 16 * 16 * 4)
    var voxelAsset: MDLAsset

    var voxelProtoID:Int = 0
    
    init(view: MTKView, device: MTLDevice) {
        self.device = device
        
        self.aspectRatio = Float(view.drawableSize.width / view.drawableSize.height)
        
        self.commandQueue = device.makeCommandQueue()!
        self.depthStencilState = Renderer.buildDepthStencilState(device: device)
        self.samplerState = Renderer.buildSamplerState(device: device)

        self.vertexDescriptor = Renderer.buildVertexDescriptor()
        self.renderPipelineFlat = Renderer.buildPipeline(device: device, view: view, vertexDescriptor: vertexDescriptor, shaderName: "fragment_main_flat")
        self.renderPipelineLighting = Renderer.buildPipeline(device: device, view: view, vertexDescriptor: vertexDescriptor, shaderName: "fragment_main_lighting")

        self.scene = Renderer.buildScene(device: device, vertexDescriptor: vertexDescriptor)
        
        let bufferAllocator = MTKMeshBufferAllocator(device: device)
        let voxelURL = Bundle.main.url(forResource: "voxel", withExtension: "obj")!
        self.voxelAsset = MDLAsset(url: voxelURL, vertexDescriptor: vertexDescriptor, bufferAllocator: bufferAllocator)

        super.init()
        
        initColors()
        
        logger(logmsg: "Render init - load screen object")
        loadScreenMesh(device: device)
        scene.rootNode.children.append(screen)
    }


    func draw(in view: MTKView) {
        lastTimeRendererStarted = spectrum.getCurrentMillis()

        // #### if a voxel-clear has been issued, remove all the voxels
        if metaClearVoxels != 0 {
            // clear all voxels from list & screen
            for a in voxelList.indices {
                let delName = voxelList[0].node.name
                logger(logmsg: "deleting "+String(delName))
                voxelList.remove(at: 0)
                let i = scene.rootNode.children.firstIndex(where: {$0.name == delName}) ?? -1
                if (i != -1) {
                    scene.rootNode.children.remove(at: i)
                }
            }
            metaClearVoxels = 0
            voxelProtoID = 0    // reset the permanent names to 0
        }
        
        // #### manipulate voxels and add new voxels to a scene
        if voxelList.count > 0 {
            for a in voxelList.indices {
                if a < voxelList.count {

                    // #### this is a new voxel in the list, we need to set it up in the renderer as a scene node
                    if voxelList[a].new {
                        voxelList[a].new = false
                        let x = Float(-0.75 + Float(voxelList[a].x) / 255 * 1.5)
                        let y = Float(-0.464 + Float(voxelList[a].y) / 175 * 1.019)
                        let z = Float(0.02 + Float(voxelList[a].z) / 256 * 1.5)
                        
                        let vw = Float(0.002 + Float(voxelList[a].w) * 0.003)
                        let vh = Float(0.002 + Float(voxelList[a].h) * 0.003)
                        let vd = Float(0.002 + Float(voxelList[a].d) * 0.003)
                        
                        let newNode = Node(name: "V" + String(voxelProtoID))    // we now give out permanent names for nodes
                        voxelProtoID = voxelProtoID + 1
                        
                        let tmpNode = loadVoxelMesh(device: device)
                        newNode.mesh = tmpNode.mesh
                        newNode.material = createVoxelMaterial(spectrumColor: voxelList[a].color)
                        voxelList[a].node = newNode
                        
                        //    size: 0.002 is smallest, ~1 is biggest 0...255; factor is 0.003
                        //    bottom left, matching ZX plot 0,0: -0.75,-0.464
                        //    top right, matching ZX plot 0,0: 0.75,0.555
                        //    let x:Float = 0.75
                        //    let y:Float = 0.555
                        //    let z:Float = 0.02
                        voxelList[a].node.modelMatrix = float4x4(translationBy: SIMD3<Float>(x, y, z)) * float4x4(scaleBy: SIMD3<Float>(vw, vh, vd))
                        scene.rootNode.children.append(voxelList[a].node)
                    }
                    
                    // #### this is an existing voxel, but the PLOT command issued a change in the voxelList, find this node in the scene and apply changes
                    if voxelList[a].change {
                        voxelList[a].change = false
                        let x = Float(-0.75 + Float(voxelList[a].x) / 255 * 1.5)
                        let y = Float(-0.464 + Float(voxelList[a].y) / 175 * 1.019)
                        let z = Float(0.02 + Float(voxelList[a].z) / 256 * 1.5)
                        let vw = Float(0.002 + Float(voxelList[a].w) * 0.003)
                        let vh = Float(0.002 + Float(voxelList[a].h) * 0.003)
                        let vd = Float(0.002 + Float(voxelList[a].d) * 0.003)
                        let tmpName = voxelList[a].node.name
                        let i = scene.rootNode.children.firstIndex(where: {$0.name == tmpName}) ?? -1
                        if (i != -1) {
                            scene.rootNode.children[i].modelMatrix = float4x4(translationBy: SIMD3<Float>(x, y, z)) * float4x4(scaleBy: SIMD3<Float>(vw, vh, vd))
                        }
                    }
                    
                    // #### move the voxel by the vector
                    if voxelList[a].vx != 0 || voxelList[a].vy != 0 || voxelList[a].vz != 0 {
                        voxelList[a].x = voxelList[a].x + Float(Float(Float(voxelList[a].vx)/10) * Float(metaVectorHold))
                        voxelList[a].y = voxelList[a].y + Float(Float(Float(voxelList[a].vy)/10) * Float(metaVectorHold))
                        voxelList[a].z = voxelList[a].z + Float(Float(Float(voxelList[a].vz)/10) * Float(metaVectorHold))
                        let x = Float(-0.75 + Float(voxelList[a].x) / 255 * 1.5)
                        let y = Float(-0.464 + Float(voxelList[a].y) / 175 * 1.019)
                        let z = Float(0.02 + Float(voxelList[a].z) / 256 * 1.5)
                        let vw = Float(0.002 + Float(voxelList[a].w) * 0.003)
                        let vh = Float(0.002 + Float(voxelList[a].h) * 0.003)
                        let vd = Float(0.002 + Float(voxelList[a].d) * 0.003)
                        let tmpName = voxelList[a].node.name
                        let i = scene.rootNode.children.firstIndex(where: {$0.name == tmpName}) ?? -1
                        if (i != -1) {
                            scene.rootNode.children[i].modelMatrix = float4x4(translationBy: SIMD3<Float>(x, y, z)) * float4x4(scaleBy: SIMD3<Float>(vw, vh, vd))
                        }
                    }
                    
                    // #### if a voxel has a countdown timer, count and act on behavior
                    // don't count down if the global hold is set to 0
                    if voxelList[a].countdown != 0 && Float(metaVectorHold) != 0 {
                        voxelList[a].countdown = voxelList[a].countdown - 1
                        if voxelList[a].countdown == 0 {
                            if voxelList[a].behavior == 0 {
                                let tmpName = voxelList[a].node.name
                                let i = scene.rootNode.children.firstIndex(where: {$0.name == tmpName}) ?? -1
                                if (i != -1) {
                                    scene.rootNode.children.remove(at: i)
                                }
                                voxelList.remove(at: a)
                            }
                            if voxelList[a].behavior == 1 {
                                voxelList[a].countdown = voxelList[a].countdownCopy
                                voxelList[a].vx = -voxelList[a].vx
                            }
                            if voxelList[a].behavior == 2 {
                                voxelList[a].countdown = voxelList[a].countdownCopy
                                voxelList[a].vy = -voxelList[a].vy
                            }
                            if voxelList[a].behavior == 3 {
                                voxelList[a].countdown = voxelList[a].countdownCopy
                                voxelList[a].vz = -voxelList[a].vz
                            }
                        }
                    }
                    
                    
                }   // end of list overflow check
                
            }

        }

        
        
        
        // check if the border color has been modified
        let bc = Int((mem[0x5c48] >> 3) & 0x07)// border color is bits 3-5 at 0x5c48 BORCR
        if (borderColor != bc) {
            borderColor = bc
            borderModified = true
        }
        
        // if the border or the screen has been modified, we will need to copy Spectrum RAM to the Render buffer
        // we also need to rewrite the render buffer if an overlay message was generated
        if (screenModified || borderModified || overlayMessage != "" || !pixelPipeline.isEmpty) {
            if (borderModified) {   // a modified border triggers a full re-rendering of the screen, since the border change overwrites the complete buffer
                screenSection0 = true
                screenSection1 = true
                screenSection2 = true
            }
            copySpectrumScreen()
            screenModified = false
            borderModified = false
            screenSection0 = false
            screenSection1 = false
            screenSection2 = false
        }
        
        let commandBuffer = commandQueue.makeCommandBuffer()!
        
        if let renderPassDescriptor = view.currentRenderPassDescriptor, let drawable = view.currentDrawable {

            // deal with screen angles set via metaScreenAngleYTarget from the menu

            if (metaScreenAngleXTarget > metaScreenAngleX) {
                metaScreenAngleX = metaScreenAngleX + 100
            }
            if (metaScreenAngleXTarget < metaScreenAngleX) {
                metaScreenAngleX = metaScreenAngleX - 100
            }
            if (metaScreenAngleYTarget > metaScreenAngleY) {
                metaScreenAngleY = metaScreenAngleY + 100
            }
            if (metaScreenAngleYTarget < metaScreenAngleY) {
                metaScreenAngleY = metaScreenAngleY - 100
            }

            let sceneRootAngle = Float(0)
            var computedAngle = (Float.pi/65356 * Float(metaScreenAngleX - 32768))
            let cameraAngleX = Float( -1 * ((2*Float.pi) + computedAngle))
            computedAngle = (Float.pi/65356 * Float(metaScreenAngleY - 32768))
            let cameraAngleY = Float( (2*Float.pi) + computedAngle) // this depends on OUT value to port 153, value 128 faces forward
            let cameraAngleZ = Float(0)

            cameraWorldPosition = SIMD3<Float>(Float(metaCameraPosX-32768)/12800, Float(metaCameraPosY-32768)/12800, Float(metaCameraPosZ)/12800)

            viewMatrix = float4x4(translationBy: -cameraWorldPosition) * float4x4(rotationAbout: SIMD3<Float>(1, 0, 0), by: cameraAngleX) * float4x4(rotationAbout: SIMD3<Float>(0, 1, 0), by: cameraAngleY) * float4x4(rotationAbout: SIMD3<Float>(0, 0, 1), by: cameraAngleZ)

            screen.modelMatrix = float4x4(translationBy: SIMD3<Float>(0, Float(metaScreenPosY), 0)) * float4x4(rotationAbout: SIMD3<Float>(0, 0, 1), by: Float.pi * 1) * float4x4(scaleBy: SIMD3<Float>(0.6, 0.6, 0.6))

            let aspectRatio = Float(view.drawableSize.width / view.drawableSize.height)
            projectionMatrix = float4x4(perspectiveProjectionFov: Float.pi / 6, aspectRatio: aspectRatio, nearZ: 0.1, farZ: 100)

            scene.rootNode.modelMatrix = float4x4(rotationAbout: SIMD3<Float>(0, 1, 0), by: sceneRootAngle)

            let commandEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor)!
            commandEncoder.setFrontFacing(.counterClockwise)
            commandEncoder.setCullMode(.back)
            commandEncoder.setDepthStencilState(depthStencilState)

            if (metaShaderType == 0) {
                commandEncoder.setRenderPipelineState(renderPipelineFlat)
            } else {
                commandEncoder.setRenderPipelineState(renderPipelineLighting)
            }

            commandEncoder.setFragmentSamplerState(samplerState, index: 0)
  
            drawNodeRecursive(scene.rootNode, parentTransform: matrix_identity_float4x4, commandEncoder: commandEncoder)
            
            commandEncoder.endEncoding()
            commandBuffer.present(drawable)
            commandBuffer.commit()
        }

        if (emulationResetDone && !emulationStop && emulationRenderPause) {
           emulationRenderPause = false // unlock the CPU for another 50th of a sec worth of opcode cycles
        }

        lastTimeRendererFinished = spectrum.getCurrentMillis()
    }
    
    
    func drawNodeRecursive(_ node: Node, parentTransform: float4x4, commandEncoder: MTLRenderCommandEncoder) {
        let modelMatrix = parentTransform * node.modelMatrix

        if let mesh = node.mesh, let baseColorTexture = node.material.baseColorTexture {

            let viewProjectionMatrix = projectionMatrix * viewMatrix

            var vertexUniforms = VertexUniforms(viewProjectionMatrix: viewProjectionMatrix,
                modelMatrix: modelMatrix,
                normalMatrix: modelMatrix.normalMatrix)

            commandEncoder.setVertexBytes(&vertexUniforms, length: MemoryLayout<VertexUniforms>.size, index: 1)

//            var fragmentUniforms = FragmentUniforms(cameraWorldPosition: cameraWorldPosition,
//                ambientLightColor: scene.ambientLightColor,
//                specularColor: node.material.specularColor,
//                specularPower: node.material.specularPower,
//                light0: scene.lights[0],
//                light1: scene.lights[1],
//                light2: scene.lights[2])

            var fragmentUniforms = FragmentUniforms(cameraWorldPosition: cameraWorldPosition,
                ambientLightColor: scene.ambientLightColor,
                specularColor: node.material.specularColor,
                specularPower: node.material.specularPower,
                light0: scene.lights[0],
                light1: scene.lights[1])

            commandEncoder.setFragmentBytes(&fragmentUniforms, length: MemoryLayout<FragmentUniforms>.size, index: 0)

            commandEncoder.setFragmentTexture(baseColorTexture, index: 0)

            let vertexBuffer = mesh.vertexBuffers.first!
            commandEncoder.setVertexBuffer(vertexBuffer.buffer, offset: vertexBuffer.offset, index: 0)

            for submesh in mesh.submeshes {
                let indexBuffer = submesh.indexBuffer
                commandEncoder.drawIndexedPrimitives(type: submesh.primitiveType,
                                                     indexCount: submesh.indexCount,
                                                     indexType: submesh.indexType,
                                                     indexBuffer: indexBuffer.buffer,
                                                     indexBufferOffset: indexBuffer.offset)
            }
        }

        for child in node.children {
            drawNodeRecursive(child, parentTransform: modelMatrix, commandEncoder: commandEncoder)
        }
    }
    
    
    static func buildSamplerState(device: MTLDevice) -> MTLSamplerState {
        let samplerDescriptor = MTLSamplerDescriptor()
        samplerDescriptor.normalizedCoordinates = true
        samplerDescriptor.minFilter = .linear
        samplerDescriptor.magFilter = .linear
        samplerDescriptor.mipFilter = .linear
        return device.makeSamplerState(descriptor: samplerDescriptor)!
    }
    
    
    static func buildVertexDescriptor() -> MDLVertexDescriptor {
        let vertexDescriptor = MDLVertexDescriptor()
        vertexDescriptor.attributes[0] = MDLVertexAttribute(name: MDLVertexAttributePosition,
                                                            format: .float3,
                                                            offset: 0,
                                                            bufferIndex: 0)
        vertexDescriptor.attributes[1] = MDLVertexAttribute(name: MDLVertexAttributeNormal,
                                                            format: .float3,
                                                            offset: MemoryLayout<Float>.size * 3,
                                                            bufferIndex: 0)
        vertexDescriptor.attributes[2] = MDLVertexAttribute(name: MDLVertexAttributeTextureCoordinate,
                                                            format: .float2,
                                                            offset: MemoryLayout<Float>.size * 6,
                                                            bufferIndex: 0)
        vertexDescriptor.layouts[0] = MDLVertexBufferLayout(stride: MemoryLayout<Float>.size * 8)
        return vertexDescriptor
    }
    
    
    static func buildDepthStencilState(device: MTLDevice) -> MTLDepthStencilState {
        let depthStencilDescriptor = MTLDepthStencilDescriptor()
        depthStencilDescriptor.depthCompareFunction = .less
        depthStencilDescriptor.isDepthWriteEnabled = true
        return device.makeDepthStencilState(descriptor: depthStencilDescriptor)!
    }

    
    static func buildScene(device: MTLDevice, vertexDescriptor: MDLVertexDescriptor) -> Scene {
        let scene = Scene()

        scene.ambientLightColor = SIMD3<Float>(0.0, 0.0, 0.0)   // temp was before 0.1 x 3
        
        let light0 = Light(worldPosition: SIMD3<Float>( -2,  2, 5), color: SIMD3<Float>(0.7, 0.7, 0.65))
        let light1 = Light(worldPosition: SIMD3<Float>(4,  5, 2), color: SIMD3<Float>(0.2, 0.2, 0.2))
//        let light2 = Light(worldPosition: SIMD3<Float>( 0, -5, 0), color: SIMD3<Float>(0.3, 0.3, 0.3))
//        scene.lights = [ light0, light1, light2 ]
        scene.lights = [ light0, light1 ]

        return scene
    }
    
    
    static func buildPipeline(device: MTLDevice, view: MTKView, vertexDescriptor: MDLVertexDescriptor, shaderName: String) -> MTLRenderPipelineState {
        guard let library = device.makeDefaultLibrary() else {
            fatalError("Could not load default library from main bundle")
        }
        let vertexFunction = library.makeFunction(name: "vertex_main")
        let fragmentFunction = library.makeFunction(name: shaderName)
        
        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.vertexFunction = vertexFunction
        pipelineDescriptor.fragmentFunction = fragmentFunction
        
        pipelineDescriptor.colorAttachments[0].pixelFormat = view.colorPixelFormat
        pipelineDescriptor.depthAttachmentPixelFormat = view.depthStencilPixelFormat

        let mtlVertexDescriptor = MTKMetalVertexDescriptorFromModelIO(vertexDescriptor)
        pipelineDescriptor.vertexDescriptor = mtlVertexDescriptor
        
        do {
            return try device.makeRenderPipelineState(descriptor: pipelineDescriptor)
        } catch {
            fatalError("Could not create render pipeline state object: \(error)")
        }
    }


    // ZX Spectrum colors
    // the color triples are not the most elegant way to solve this, but it's high performance
    func initColors() {
        colors[0] = setColor(rgb: 0x000000) // the border colors are rendered in genericRGBLinear and need to be about 8% brighter to match the texture in rgba8Unorm
        colors[1] = setColor(rgb: 0x0000f0)
        colors[2] = setColor(rgb: 0xf00000)
        colors[3] = setColor(rgb: 0xf000f0)
        colors[4] = setColor(rgb: 0x00f000)
        colors[5] = setColor(rgb: 0x00f0f0)
        colors[6] = setColor(rgb: 0xf0f000)
        colors[7] = setColor(rgb: 0xf0f0f0)
        colors[8] = setColor(rgb: 0x000000) // "bright" black
        colors[9] = setColor(rgb: 0x0000ff)
        colors[10] = setColor(rgb: 0xff0000)
        colors[11] = setColor(rgb: 0xff00ff)
        colors[12] = setColor(rgb: 0x00ff00)
        colors[13] = setColor(rgb: 0x00ffff)
        colors[14] = setColor(rgb: 0xffff00)
        colors[15] = setColor(rgb: 0xffffff)
        colorTriples[0] = 0x00  // black    - these are the texture colors used for the pixel renderer in rgba8Unorm space
        colorTriples[1] = 0x00
        colorTriples[2] = 0x00
        colorTriples[3] = 0x00  // blue
        colorTriples[4] = 0x00
        colorTriples[5] = 0xe0
        colorTriples[6] = 0xe0  // red
        colorTriples[7] = 0x00
        colorTriples[8] = 0x00
        colorTriples[9] = 0xe0  // magenta
        colorTriples[10] = 0x00
        colorTriples[11] = 0xe0
        colorTriples[12] = 0x00 // green
        colorTriples[13] = 0xe0
        colorTriples[14] = 0x00
        colorTriples[15] = 0x00 // cyan
        colorTriples[16] = 0xe0
        colorTriples[17] = 0xe0
        colorTriples[18] = 0xe0 // yellow
        colorTriples[19] = 0xe0
        colorTriples[20] = 0x00
        colorTriples[21] = 0xe0 // white
        colorTriples[22] = 0xe0
        colorTriples[23] = 0xe0
        colorTriples[24] = 0x00 // bright black
        colorTriples[25] = 0x00
        colorTriples[26] = 0x00
        colorTriples[27] = 0x00 // bright blue
        colorTriples[28] = 0x00
        colorTriples[29] = 0xff
        colorTriples[30] = 0xff // bright red
        colorTriples[31] = 0x00
        colorTriples[32] = 0x00
        colorTriples[33] = 0xff // bright magenta
        colorTriples[34] = 0x00
        colorTriples[35] = 0xff
        colorTriples[36] = 0x00 // bright green
        colorTriples[37] = 0xff
        colorTriples[38] = 0x00
        colorTriples[39] = 0x00 // bright cyan
        colorTriples[40] = 0xff
        colorTriples[41] = 0xff
        colorTriples[42] = 0xff // bright yellow
        colorTriples[43] = 0xff
        colorTriples[44] = 0x00
        colorTriples[45] = 0xff // bright white
        colorTriples[46] = 0xff
        colorTriples[47] = 0xff
    }


    // create a CGColor from a 3-byte hex value
    func setColor(rgb: Int) -> CGColor {
        let newColor = CGColor(
            red: CGFloat(Float((rgb >> 16) & 0xFF)/255),
            green: CGFloat(Float((rgb >> 8) & 0xFF)/255),
            blue: CGFloat(Float(rgb & 0xFF)/255),
            alpha: 1
        )
        return newColor
    }
    
    
    // our mesh is a correctly proportioned rectangle along the Blender X and Z axis, facing Y
    func loadScreenMesh(device: MTLDevice) {
        let vertexDescriptor = MDLVertexDescriptor()
        vertexDescriptor.attributes[0] = MDLVertexAttribute(name: MDLVertexAttributePosition, format: .float3, offset: 0, bufferIndex: 0)
        vertexDescriptor.attributes[1] = MDLVertexAttribute(name: MDLVertexAttributeNormal, format: .float3, offset: MemoryLayout<Float>.size * 3, bufferIndex: 0)
        vertexDescriptor.attributes[2] = MDLVertexAttribute(name: MDLVertexAttributeTextureCoordinate, format: .float2, offset: MemoryLayout<Float>.size * 6, bufferIndex: 0)
        vertexDescriptor.layouts[0] = MDLVertexBufferLayout(stride: MemoryLayout<Float>.size * 8)
        
        let bufferAllocator = MTKMeshBufferAllocator(device: device)
        
        let screenMaterial = Material()
        screen.material = screenMaterial
        
        let modelURL = Bundle.main.url(forResource: "screen", withExtension: "obj")!
        let asset = MDLAsset(url: modelURL, vertexDescriptor: vertexDescriptor, bufferAllocator: bufferAllocator)
        screen.mesh = try! MTKMesh.newMeshes(asset: asset, device: device).metalKitMeshes.first!
    }

    func loadVoxelMesh(device: MTLDevice) -> Node {
        let vertexDescriptor = MDLVertexDescriptor()
        vertexDescriptor.attributes[0] = MDLVertexAttribute(name: MDLVertexAttributePosition, format: .float3, offset: 0, bufferIndex: 0)
        vertexDescriptor.attributes[1] = MDLVertexAttribute(name: MDLVertexAttributeNormal, format: .float3, offset: MemoryLayout<Float>.size * 3, bufferIndex: 0)
        vertexDescriptor.attributes[2] = MDLVertexAttribute(name: MDLVertexAttributeTextureCoordinate, format: .float2, offset: MemoryLayout<Float>.size * 6, bufferIndex: 0)
        vertexDescriptor.layouts[0] = MDLVertexBufferLayout(stride: MemoryLayout<Float>.size * 8)
        
//        let bufferAllocator = MTKMeshBufferAllocator(device: device)
        
        let tmpVoxel = Node(name: "tmp")
        
//        let voxelURL = Bundle.main.url(forResource: "voxel", withExtension: "obj")!
//        let voxelAsset = MDLAsset(url: voxelURL, vertexDescriptor: vertexDescriptor, bufferAllocator: bufferAllocator)
        tmpVoxel.mesh = try! MTKMesh.newMeshes(asset: voxelAsset, device: device).metalKitMeshes.first!
        return tmpVoxel
    }
    
    func createVoxelMaterial(spectrumColor: Int) -> Material {
        let rgbColorSpace = CGColorSpace(name: CGColorSpace.genericRGBLinear)!
        let bitmapInfo = CGBitmapInfo.byteOrder32Big.rawValue | CGImageAlphaInfo.premultipliedLast.rawValue
        let context = CGContext(data: &rawTexture, width: Int(16), height: Int(16), bitsPerComponent: bitsPerComponent, bytesPerRow: 16*4, space: rgbColorSpace, bitmapInfo: bitmapInfo)!
//logger(logmsg: "creating voxel with color "+String(spectrumColor))
        context.setFillColor(colors[spectrumColor]) // we get this from OUT to port 157
        context.fill(CGRect(x: 0, y: 0, width: CGFloat(16), height: CGFloat(16)))
        
        let textureDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: MTLPixelFormat.rgba8Unorm, width: Int(16), height: Int(16), mipmapped: false)

        let tmpMaterial = Material()
        
        tmpMaterial.baseColorTexture = device.makeTexture(descriptor: textureDescriptor)
        let region = MTLRegionMake2D(0, 0, Int(16), Int(16))
        tmpMaterial.baseColorTexture?.replace(region: region, mipmapLevel: 0, withBytes: &rawTexture, bytesPerRow: Int(16*4))

        tmpMaterial.specularPower = 100
        tmpMaterial.specularColor = SIMD3<Float>(0.1, 0.1, 0.1)
        return tmpMaterial
    }
    
    
    
    func copySpectrumScreen() {
        let rgbColorSpace = CGColorSpace(name: CGColorSpace.genericRGBLinear)!

        // set the border by painting the full screen in the background color
        if (borderModified) {
            let bitmapInfo = CGBitmapInfo.byteOrder32Big.rawValue | CGImageAlphaInfo.premultipliedLast.rawValue
            let context = CGContext(data: &rawData, width: Int(textureSizeX), height: Int(textureSizeY), bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: rgbColorSpace, bitmapInfo: bitmapInfo)!
            context.setFillColor(colors[borderColor])
            context.fill(CGRect(x: 0, y: 0, width: CGFloat(textureSizeX), height: CGFloat(textureSizeY)))
        }
        
        if (pixelPipeline.isEmpty) {
            // now deal with the main pixel display
            var rawPointer = 32 * 320 * 4 + 32 * 4 // top border is 32 pixels tall, left border is 32 pixels wide
            var lineCounter = 0
            var blockLineCounter = 0
            var blockCharLineCounter = 0
            
            let screenStart = 16384 // screen RAM
            
            var attributePointer = 6144 // the first color attribute
            
            var screenMemPointerStart = 0
            var screenMemPointerEnd = 6143
            
            // the spectrum screen has three "natural" sections and the system code keeps track which sections have been written to in the previous CPU cycle
            // we speed up the average render time by only re-drawing the modified sections
            if (!screenSection0 && screenSection1) {    // first block is unchanged, second block is changed
                screenMemPointerStart = 2048
                rawPointer += 64 * 320 * 4
                attributePointer += 8 * 32
            }
            
            if (!screenSection0 && !screenSection1 && screenSection2) {    // first block is unchanged, second block is unchanged, third block is changed
                screenMemPointerStart = 4096
                rawPointer += 128 * 320 * 4
                attributePointer += 16 * 32
            }
            
            if (screenSection0 && !screenSection1 && !screenSection2) {    // second block is unchanged, third block is unchanged
                screenMemPointerEnd = 2047
            }
            
            if (screenSection1 && !screenSection2) {    // second block is changed, third block is unchanged
                screenMemPointerEnd = 4095
            }
            
            rawPointer += (32 * 8 * 4 - 4)  // reverse line direction - move to the end of the raw output line
            
            for i in screenMemPointerStart...screenMemPointerEnd {
                
                var thisInk = (mem[screenStart + attributePointer] & 0x07) * 3
                var thisPaper = ((mem[screenStart + attributePointer] >> 3) & 0x07) * 3
                if (mem[screenStart + attributePointer] & 0x40  ==  0x40) {   // bright flag is set
                    thisInk += 24
                    thisPaper += 24
                }
                
                attributePointer += 1
                
                var target = 7
                for b in 0...7 {    // we write 4 bytes for each bit of the original speccy screen
                    let bitTarget = 2.pow(toPower: target)
                    target -= 1
                    if (Int(mem[screenStart + i]) & bitTarget == bitTarget)  {
                        rawData[rawPointer] = colorTriples[Int(thisInk)]
                        rawPointer += 1
                        rawData[rawPointer] = colorTriples[Int(thisInk)+1]
                        rawPointer += 1
                        rawData[rawPointer] = colorTriples[Int(thisInk)+2]
                        rawPointer += 1
                        rawData[rawPointer] = 255
                        rawPointer += 1
                    } else {
                        rawData[rawPointer] = colorTriples[Int(thisPaper)]
                        rawPointer += 1
                        rawData[rawPointer] = colorTriples[Int(thisPaper)+1]
                        rawPointer += 1
                        rawData[rawPointer] = colorTriples[Int(thisPaper)+2]
                        rawPointer += 1
                        rawData[rawPointer] = 255
                        rawPointer += 1
                    }
                    
                    rawPointer -= 8 // reverse line direction - step back by an output pixel
                }
                
                
                lineCounter += 1
                if (lineCounter == 32) {    // 8 pixels * 32 is one speccy line of 256, jump to next line
                    
                    rawPointer += (32 * 8 * 4 + 4) // reverse line direction - set forward to the end of the current raw output line
                    
                    lineCounter = 0
                    rawPointer += (64 * 4 + 320 * 4 * 7)  // 64 pixels for the border and 7 lines down in the block (1 char line)
                    blockLineCounter += 1
                    if (blockLineCounter == 8) {
                        blockLineCounter = 0
                        blockCharLineCounter += 1
                        if (blockCharLineCounter < 8) { // did we complete the last char line of the block?
                            rawPointer = rawPointer - 320 * 4 * 63 // if no, go back up by 63 lines
                            attributePointer = attributePointer - 32 * 8
                        } else {
                            blockCharLineCounter = 0    // rawPointer is already pointing at the last line of the first char line of the next block, start the next block
                            rawPointer = rawPointer - 320 * 4 * 7
                        }
                    }
                    
                    rawPointer += (32 * 8 * 4 - 4)  // reverse line direction - in the new output line step forward to the end
                }
                
            }   // end of scr pixel data loop
        }
        
        // ############################
        // ######### Overlay ##########
        // ############################
        // if an overlay message is set, we write the message into the rawData pixel buffer - this routine can access the full
        // texture of 320x256 and utilizes the character set in the ZX Rom at x3D00
        if (overlayMessage != "") {
            overlayBackground = borderColor
            if (overlayBackground == 0) {
                overlayColor = 7
            } else {
                overlayColor = 0
            }
            let overlayMessageArray = overlayMessage.asciiValues
            var charCounter = 0
            var messageAllSpaces = true
            for char in overlayMessageArray {
                if (char != 32) {
                    messageAllSpaces = false
                }
                let charPointer = Int(romCharSet) + (Int(char)-32) * 8
                for charY in 0...7 {
                    // go through the bytes of a character in the ROM table
                    var ovPointer = (overlayAtY * 8 * 1280) + (charY * 1280) + (1280 - ((overlayAtX + charCounter) * 8 * 4)) - 4
                    // 40chars across = 1280 bytes (40 * 8 * 4)
                    let thisCharByte = mem[charPointer + charY]
                    for charX in 0...7 {
                        // go through the bits left to right, so we need to subtract this loop value from 7 to go hi->lo
                        let charBit = thisCharByte & UInt8(0x01 << (7-charX))
                        if (charBit != 0) {
                            rawData[ovPointer] = colorTriples[Int(overlayColor*3)]
                            ovPointer += 1
                            rawData[ovPointer] = colorTriples[Int(overlayColor*3)+1]
                            ovPointer += 1
                            rawData[ovPointer] = colorTriples[Int(overlayColor*3)+2]
                            ovPointer += 1
                            rawData[ovPointer] = 255
                            ovPointer -= 7
                        } else {
                            rawData[ovPointer] = colorTriples[Int(overlayBackground*3)]
                            ovPointer += 1
                            rawData[ovPointer] = colorTriples[Int(overlayBackground*3)+1]
                            ovPointer += 1
                            rawData[ovPointer] = colorTriples[Int(overlayBackground*3)+2]
                            ovPointer += 1
                            rawData[ovPointer] = 255
                            ovPointer -= 7
                        }
                    } // charX
                } // charY
                charCounter += 1
            } // char
            if (messageAllSpaces) {
                overlayMessage = ""     // if it was all spaces, we've now cleared a previous message and can stop doing that repeatedly
            }
        }

        // ###########################################
        // ######### Extended Plot Renderer ##########
        // ###########################################
//        logger(logmsg: String(pixelPipeline.count))
        while (!pixelPipeline.isEmpty) {
            let plotX = pixelPipeline[0].x
            let plotY = pixelPipeline[0].y
            let plotColor = pixelPipeline[0].c
            pixelPipeline.remove(at: 0)

            var ovPointer = (plotY * 1280) + (1280 - (plotX * 4)) - 4

            rawData[ovPointer] = colorTriples[Int(plotColor*3)]
            ovPointer += 1
            rawData[ovPointer] = colorTriples[Int(plotColor*3)+1]
            ovPointer += 1
            rawData[ovPointer] = colorTriples[Int(plotColor*3)+2]
            ovPointer += 1
            rawData[ovPointer] = 255
        }

        
        // in part based on stackoverflow.com/questions/53973075/create-mtltexture-with-full-colour-depth-mtltexturedescriptor
        let textureDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: MTLPixelFormat.rgba8Unorm, width: Int(textureSizeX), height: Int(textureSizeY), mipmapped: false)

        screen.material.baseColorTexture = device.makeTexture(descriptor: textureDescriptor)
        
        let region = MTLRegionMake2D(0, 0, Int(textureSizeX), Int(textureSizeY))
        screen.material.baseColorTexture?.replace(region: region, mipmapLevel: 0, withBytes: &rawData, bytesPerRow: Int(bytesPerRow))
        
        voxel.material.specularPower = 100
        voxel.material.specularColor = SIMD3<Float>(0.1, 0.1, 0.1)
    }
}



