//
//  ViewController.swift
//  BitPrism
//
//  Created by Thomas Sturm on 3/21/22.
//  Distributed under Attribution-NonCommercial-ShareAlike 4.0 (CC BY-NC-SA 4.0)
//

import Cocoa
import MetalKit
import simd


class ViewController: NSViewController {
    var mtkView: MTKView!
    var renderer: Renderer!
    let spectrum = Spectrum()
    
    @IBAction func menuNew(_ openMenuItem: NSMenuItem) {
        logger(logmsg: "Menu Item: New")
        emulationStop = false
        emulationCounter = 0
        spectrum.startAtNew()
    }

    @IBAction func menuOpen(_ openMenuItem: NSMenuItem) {
        logger(logmsg: "Menu Item: Fastload...")
        emulationRenderPause = true
        emulationStop = true
        let openPanel = NSOpenPanel()
        openPanel.canChooseFiles = true
        openPanel.canChooseDirectories = false
        openPanel.canCreateDirectories = false
        let i = openPanel.runModal()
        if i == NSApplication.ModalResponse.OK {
            print("The directory selected is " + openPanel.url!.absoluteString)
            if openPanel.url!.absoluteString.lowercased().contains(".sna") {
                    loadSNA(thePath: openPanel.url!)
            }
            if openPanel.url!.absoluteString.lowercased().contains(".tzx") {
                    emulationTZXInstantMode = true
                    loadTZX(thePath: openPanel.url!)
            }
        }
    }

    @IBAction func menuCassetteOpen(_ openMenuItem: NSMenuItem) {
        logger(logmsg: "Menu Item: Cassette Load..")
        emulationRenderPause = true
        emulationStop = true
        let openPanel = NSOpenPanel()
        openPanel.canChooseFiles = true
        openPanel.canChooseDirectories = false
        openPanel.canCreateDirectories = false
        let i = openPanel.runModal()
        if i == NSApplication.ModalResponse.OK {
            print("The directory selected is " + openPanel.url!.absoluteString)
             if openPanel.url!.absoluteString.lowercased().contains(".tzx") {
                    emulationTZXInstantMode = false
                    loadTZX(thePath: openPanel.url!)
            }
        }
    }
    
    @IBAction func menuPerspectiveLeft(_ openMenuItem: NSMenuItem) {
        logger(logmsg: "Menu Item: Perspective Left")
        if (metaScreenAngleYTarget < (65535-2048)) {
            metaScreenAngleYTarget = metaScreenAngleYTarget + 2048
        }
    }

    @IBAction func menuPerspectiveRight(_ openMenuItem: NSMenuItem) {
        logger(logmsg: "Menu Item: Perspective Right")
        if (metaScreenAngleYTarget > 2048) {
            metaScreenAngleYTarget = metaScreenAngleYTarget - 2048
        }
    }

    @IBAction func menuResetView(_ openMenuItem: NSMenuItem) {
        logger(logmsg: "Menu Item: Reset View")
        metaScreenAngleXTarget = 32768
        metaScreenAngleYTarget = 32768
        metaCameraPosX = 32768
        metaCameraPosY = 32768
        metaCameraPosZ = 25600
        metaScreenPosY = 0
    }

    @IBAction func menuClearObjects(_ openMenuItem: NSMenuItem) {
        logger(logmsg: "Menu Item: Clear 3D Objects")
        metaClearVoxels = 1
    }

    @IBAction func menuHelpKeyboard(_ openMenuItem: NSMenuItem) {
        logger(logmsg: "Menu Item: Keyboard Help")
        let mainStoryBoard = NSStoryboard(name: "Main", bundle: nil)
        let windowController = mainStoryBoard.instantiateController(withIdentifier: "HelpWindow") as! NSWindowController
        windowController.showWindow(self)
    }

  
    @IBAction func menuViewDisassembler(_ openMenuItem: NSMenuItem) {
        logger(logmsg: "Menu Item: Disassembler")
        let mainStoryBoard = NSStoryboard(name: "Main", bundle: nil)
        let disassemblerWindowController = (mainStoryBoard.instantiateController(withIdentifier: "DSWindow") as! NSWindowController)
        disassemblerWindowController.showWindow(self)

        disassemblerOn = true

        disassemblerWindowViewController = (mainStoryBoard.instantiateController(withIdentifier: "DSViewController") as! DSViewControllerClass)
    }
    // ======================================================

        
    override func viewDidLoad() {
        super.viewDidLoad()

        
//        DisassemblerMainTextListGlobal = DisassemblerMainTextList

        mtkView = MTKView()
        mtkView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(mtkView)
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[mtkView]|", options: [], metrics: nil, views: ["mtkView" : mtkView as Any]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[mtkView]|", options: [], metrics: nil, views: ["mtkView" : mtkView as Any]))
        
        let device = MTLCreateSystemDefaultDevice()!
        mtkView.device = device

        // colors in order: blue, green, red, alpha
        mtkView.colorPixelFormat = .bgra8Unorm_srgb
        mtkView.depthStencilPixelFormat = .depth32Float
        
        renderer = Renderer(view: mtkView, device: device)
        mtkView.delegate = renderer
        
        logger(logmsg: "app start")

        initMagic()
        
        loadRom()

        logger(logmsg: "ROM loading done")
        
        emulationStop = false
        emulationCounter = 0
        
        spectrum.reset()

        emulationResetDone = true
        emulationRenderPause = true // the renderer will only attempt to run the emulator if it starts in a pause state
         
        spectrum.timer = Timer.scheduledTimer(timeInterval: 0.02, target: spectrum, selector: #selector(spectrum.run), userInfo: nil, repeats: true)
        
        NSEvent.addLocalMonitorForEvents(matching: .keyDown) {
            self.keyDown(with: $0)
            return nil  // if we return $0, the OS will ping and try to use the key press
        }
        NSEvent.addLocalMonitorForEvents(matching: .keyUp) {
            self.keyUp(with: $0)
            return nil  // if we return $0, the OS will ping and try to use the key press
        }
        NSEvent.addLocalMonitorForEvents(matching: .flagsChanged) {
            self.flagsChanged(with: $0)
            return $0
        }
                
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
}


extension ViewController {
    override var acceptsFirstResponder: Bool { return true }

    override func keyDown(with event: NSEvent) {
        Keyboard.SetKeyPressed(event.keyCode, isOn: true)
//        print(String(event.keyCode) + " Down")
    }

    override func keyUp(with event: NSEvent) {
        Keyboard.SetKeyPressed(event.keyCode, isOn: false)
//        print(String(event.keyCode) + " UP")
    }

    // this is based on the primary answer to https://stackoverflow.com/questions/32446978/swift-capture-keydown-from-nsviewcontroller
    override func flagsChanged(with event: NSEvent) {
        switch event.modifierFlags.intersection(.deviceIndependentFlagsMask) {
        case [.shift]:
//            print("shift key is pressed")
            Keyboard.SetKeyPressed(0x38, isOn: true)
            Keyboard.SetKeyPressed(0x3A, isOn: false)
        case [.option] :
//            print("option key is pressed")
            Keyboard.SetKeyPressed(0x3A, isOn: true)    // Symbol Shift is mapped to Option
            Keyboard.SetKeyPressed(0x38, isOn: false)
        case [.option, .shift]:
//            print("option-shift keys are pressed")
            Keyboard.SetKeyPressed(0x38, isOn: true)
            Keyboard.SetKeyPressed(0x3A, isOn: true)
        default:
//            print("no modifier keys are pressed")
            Keyboard.SetKeyPressed(0x38, isOn: false)
            Keyboard.SetKeyPressed(0x3A, isOn: false)
        }
    }
}


// ========================== Disassembler =============================

class DSViewControllerClass: NSViewController {
    
    @IBOutlet var disassemblerMainOutlet: NSTextView!
    
    @IBOutlet var disassemblerLabelOutlet: NSTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logger(logmsg: "DSViewControllerClass viewDidLoad executed")
        
        disassemblerListingCallback = { () -> Void in
            // #### display opcodes
            var myString = ""
            for item in disassemblerList.indices {
                if disassemblerList[item].itemText != "" {
                    myString += String(toHexWord(num: disassemblerList[item].itemAddress)) + " : " +  disassemblerList[item].itemText + "\n"
                }
            }

            // global color and font for the text in the outlet
            let listAttribute = [
                NSAttributedString.Key.foregroundColor: NSColor.blue,
                NSAttributedString.Key.font: NSFont.monospacedSystemFont(ofSize: 10, weight: NSFont.Weight.medium)
            ]
            // convert the full listing into an attributed string with the global style defined above
            let myAttrString = NSMutableAttributedString(string: myString, attributes: listAttribute)
            //            let myAttrString = NSAttributedString(string: myString, attributes: listAttribute)

            // mark all labels
            var stringOneRegex = try! NSRegularExpression(pattern: "(LABEL[\\d]++)", options: NSRegularExpression.Options.caseInsensitive)
            var stringOneMatches = stringOneRegex.matches(in: myString, options: [], range: NSMakeRange(0, myAttrString.length))
            for stringOneMatch in stringOneMatches {
                let wordRange = stringOneMatch.range(at: 0)
                myAttrString.addAttribute(NSAttributedString.Key.foregroundColor, value: NSColor.red, range: wordRange)
            }

            // mark all system variables
            stringOneRegex = try! NSRegularExpression(pattern: "((System|Memory|Routine)( [\\S]++)++)", options: NSRegularExpression.Options.caseInsensitive)
            stringOneMatches = stringOneRegex.matches(in: myString, options: [], range: NSMakeRange(0, myAttrString.length))
            for stringOneMatch in stringOneMatches {
                let wordRange = stringOneMatch.range(at: 0)
                myAttrString.addAttribute(NSAttributedString.Key.foregroundColor, value: NSColor.init(calibratedRed: 0, green: 0.5, blue: 0.1, alpha: 1), range: wordRange)
            }

            // mark all IO operations
            stringOneRegex = try! NSRegularExpression(pattern: "( IO)( )*([0-9A-F])*+", options: [])
            stringOneMatches = stringOneRegex.matches(in: myString, options: [], range: NSMakeRange(0, myAttrString.length))
            for stringOneMatch in stringOneMatches {
                let wordRange = stringOneMatch.range(at: 0)
                myAttrString.addAttribute(NSAttributedString.Key.foregroundColor, value: NSColor.init(calibratedRed: 0, green: 0.7, blue: 0.6, alpha: 1), range: wordRange)
            }

            // delete the existing text in the outlet
            let len = self.disassemblerMainOutlet.textStorage?.characters.count
            let myNSRange = NSRange(location: 0, length: len ?? 1)
            self.disassemblerMainOutlet.textStorage?.deleteCharacters(in: myNSRange)
            
            // write out the latest version of the text to the outlet
            self.disassemblerMainOutlet.textStorage?.insert(myAttrString, at: 0)
        }
            
        disassemblerLabelCallback = { () -> Void in
           // #### display labels
            var labelString = ""
            
            labelString = compileLabelString()
            
            let labelAttribute = [
                NSAttributedString.Key.foregroundColor: NSColor.blue,
                NSAttributedString.Key.font: NSFont.monospacedSystemFont(ofSize: 9, weight: NSFont.Weight.medium)
            ]
            let labelAttrString = NSAttributedString(string: labelString, attributes: labelAttribute)
            
            let labelLen = self.disassemblerLabelOutlet.textStorage?.characters.count
            let labelNSRange = NSRange(location: 0, length: labelLen ?? 1)
            self.disassemblerLabelOutlet.textStorage?.deleteCharacters(in: labelNSRange)
            
            self.disassemblerLabelOutlet.textStorage?.insert(labelAttrString, at: 0)
        }
        
        func compileLabelString() -> String {
            var tmpString = ""
            for item in disassemblerLabels.indices {
                tmpString += String(disassemblerLabels[item].labelID)
                tmpString += " : " +  disassemblerLabels[item].labelName
                tmpString += " : " + String(toHexWord(num: disassemblerLabels[item].labelAddress)) + "\n"
            }
            return tmpString
        }
        
    }
}



// based on https://github.com/twohyjr/NSView-Keyboard-and-Mouse-Input/blob/master/KeyCodes.swift
class Keyboard {
    private static var KEY_COUNT: Int = 256
    private static var keys = [Bool].init(repeating: false, count: KEY_COUNT)
    
    public static func SetKeyPressed(_ keyCode: UInt16, isOn: Bool) {
        keys[Int(keyCode)] = isOn

        // key press down

        if (keyCode == KeyCodes.delete.rawValue && isOn) { // Mac delete key
            keys[Int(0x38)] = true  // shift
            keys[Int(0x3A)] = false // option
            keys[Int(0x1D)] = true // 0
        }

        if (keyCode == KeyCodes.singleQuote.rawValue && isOn  &&  !keys[Int(0x38)]) { // Mac "'" key -> "
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = true // option
            keys[Int(0x1A)] = true // 7
        }

        if (keyCode == KeyCodes.singleQuote.rawValue && isOn  &&  keys[Int(0x38)]) { // Mac "'" key plus shift -> "
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = true // option
            keys[Int(0x23)] = true // p
        }

        if (keyCode == KeyCodes.semicolon.rawValue && isOn  &&  !keys[Int(0x38)]) { // Mac ";" key
            keys[Int(0x3A)] = true // option
            keys[Int(0x1F)] = true // o
        }

        if (keyCode == KeyCodes.semicolon.rawValue && isOn  &&  keys[Int(0x38)]) { // Mac ";" key plus shift -> :
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = true // option
            keys[Int(0x06)] = true // z
        }

        if (keyCode == KeyCodes.squareOpen.rawValue && isOn  &&  !keys[Int(0x38)]) { // Mac "[" key
            keys[Int(0x3A)] = true // option
            keys[Int(0x1C)] = true // 8
        }

        if (keyCode == KeyCodes.squareClose.rawValue && isOn  &&  !keys[Int(0x38)]) { // Mac "]" key
            keys[Int(0x3A)] = true // option
            keys[Int(0x19)] = true // 9
        }

        if (keyCode == KeyCodes.equalKey.rawValue && isOn  &&  !keys[Int(0x38)]) { // Mac "=" key
            keys[Int(0x3A)] = true // option
            keys[Int(0x25)] = true // l
        }
        
        if (keyCode == KeyCodes.equalKey.rawValue && isOn  &&  keys[Int(0x38)]) { // Mac "=" key plus shift -> +
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = true // option
            keys[Int(0x28)] = true // k
        }

        if (keyCode == KeyCodes.minus.rawValue && isOn  &&  !keys[Int(0x38)]) { // Mac "-" key
            keys[Int(0x3A)] = true // option
            keys[Int(0x26)] = true // j
        }

        if (keyCode == KeyCodes.minus.rawValue && isOn  &&  keys[Int(0x38)]) { // Mac "-" key plus shift -> _
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = true // option
            keys[Int(0x1D)] = true // 0
        }

        if (keyCode == KeyCodes.comma.rawValue && isOn  &&  !keys[Int(0x38)]) { // Mac "," key
            keys[Int(0x3A)] = true // option
            keys[Int(0x2d)] = true // n
        }

        if (keyCode == KeyCodes.comma.rawValue && isOn  &&  keys[Int(0x38)]) { // Mac "," key plus shift -> <
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = true // option
            keys[Int(0x0f)] = true // r
        }

        if (keyCode == KeyCodes.slash.rawValue && isOn  &&  !keys[Int(0x38)]) { // Mac "/"
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = true // option
            keys[Int(0x09)] = true // v
        }
        if (keyCode == KeyCodes.slash.rawValue && isOn  &&  keys[Int(0x38)]) { // Mac "/" key plus shift -> ?
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = true // option
            keys[Int(0x08)] = true // c
        }

        if (keyCode == KeyCodes.period.rawValue && isOn  &&  !keys[Int(0x38)]) { // Mac "." key
            keys[Int(0x3A)] = true // option
            keys[Int(0x2e)] = true // m
        }

        if (keyCode == KeyCodes.period.rawValue && isOn  &&  keys[Int(0x38)]) { // Mac "." key plus shift -> >
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = true // option
            keys[Int(0x11)] = true // t
        }

        if (keyCode == KeyCodes.tab.rawValue && isOn) { // Mac "TAB" key -> EDIT
            keys[Int(0x38)] = true // shift
            keys[Int(0x12)] = true // 1
        }

        if (keyCode == KeyCodes.leftArrow.rawValue && isOn) { // Mac "LEFT CURSOR" key -> shift-5
            keys[Int(0x38)] = true // shift
            keys[Int(0x17)] = true // 5
        }

        if (keyCode == KeyCodes.downArrow.rawValue && isOn) { // Mac "DOWN CURSOR" key -> shift-6
            keys[Int(0x38)] = true // shift
            keys[Int(0x16)] = true // 6
        }

        if (keyCode == KeyCodes.upArrow.rawValue && isOn) { // Mac "UP CURSOR" key -> shift-7
            keys[Int(0x38)] = true // shift
            keys[Int(0x1a)] = true // 7
        }

        if (keyCode == KeyCodes.rightArrow.rawValue && isOn) { // Mac "RIGHT CURSOR" key -> shift-8
            keys[Int(0x38)] = true // shift
            keys[Int(0x1c)] = true // 8
        }


        // =========================== key press up

        if (keyCode == KeyCodes.squareClose.rawValue && !isOn) {
            keys[Int(0x3A)] = false // remove shift
            keys[Int(0x19)] = false // 9
        }

        if (keyCode == KeyCodes.squareOpen.rawValue && !isOn) {
            keys[Int(0x3A)] = false // remove shift
            keys[Int(0x1C)] = false // 8
        }

        if (keyCode == KeyCodes.equalKey.rawValue && !isOn) {
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = false // option
            keys[Int(0x25)] = false // l
            keys[Int(0x28)] = false // k
        }

        if (keyCode == KeyCodes.minus.rawValue && !isOn) {
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = false // option
            keys[Int(0x1D)] = false // 0
            keys[Int(0x26)] = false // j
        }

        if (keyCode == KeyCodes.singleQuote.rawValue && !isOn) {
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = false // option
            keys[Int(0x23)] = false // p
            keys[Int(0x1A)] = false // 7
        }

        if (keyCode == KeyCodes.semicolon.rawValue && !isOn) {
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = false // option
            keys[Int(0x06)] = false // z
            keys[Int(0x1F)] = false // o
        }

        if (keyCode == KeyCodes.comma.rawValue && !isOn) {
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = false // option
            keys[Int(0x2d)] = false // n
            keys[Int(0x0f)] = false // r
        }

        if (keyCode == KeyCodes.slash.rawValue && !isOn) {
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = false // option
            keys[Int(0x08)] = false // c
            keys[Int(0x09)] = false // v
        }

        if (keyCode == KeyCodes.period.rawValue && !isOn) {
            keys[Int(0x38)] = false // remove shift
            keys[Int(0x3A)] = false // option
            keys[Int(0x2e)] = false // m
            keys[Int(0x11)] = false // t
        }

        if (keyCode == KeyCodes.delete.rawValue && !isOn) {
            keys[Int(0x38)] = false  // shift
            keys[Int(0x1D)] = false // 0
        }

        if (keyCode == KeyCodes.tab.rawValue && !isOn) { // Mac "TAB" key
            keys[Int(0x38)] = false // shift
            keys[Int(0x12)] = false // 1
        }

        if (keyCode == KeyCodes.leftArrow.rawValue && !isOn) { // Mac "LEFT CURSOR" key -> shift-5
            keys[Int(0x38)] = false // shift
            keys[Int(0x17)] = false // 5
        }

        if (keyCode == KeyCodes.downArrow.rawValue && !isOn) { // Mac "DOWN CURSOR" key -> shift-6
            keys[Int(0x38)] = false // shift
            keys[Int(0x16)] = false // 6
        }

        if (keyCode == KeyCodes.upArrow.rawValue && !isOn) { // Mac "UP CURSOR" key -> shift-7
            keys[Int(0x38)] = false // shift
            keys[Int(0x1a)] = false // 7
        }

        if (keyCode == KeyCodes.rightArrow.rawValue && !isOn) { // Mac "RIGHT CURSOR" key -> shift-8
            keys[Int(0x38)] = false // shift
            keys[Int(0x1c)] = false // 8
        }
        
    }
    
    public static func IsKeyPressed(_ keyCode: KeyCodes) -> Bool {
        return keys[Int(keyCode.rawValue)]
    }
    
    // when the key row is polled (set of 5 kyes), the bits for pressed keys are lowered (bits 0-4)
    public static func getSpectrumKeyRow(row: UInt8) -> UInt8 {
        var val: UInt8 = 0xff
        switch (row) {
        case 0xFE:
            if (IsKeyPressed(KeyCodes.shift)) {val &= 0xFE}
            if (IsKeyPressed(KeyCodes.z)) {val &= 0xFD}
            if (IsKeyPressed(KeyCodes.x)) {val &= 0xFB}
            if (IsKeyPressed(KeyCodes.c)) {val &= 0xF7}
            if (IsKeyPressed(KeyCodes.v)) {val &= 0xEF}
        case 0xFD:
            if (IsKeyPressed(KeyCodes.a)) {val &= 0xFE}
            if (IsKeyPressed(KeyCodes.s)) {val &= 0xFD}
            if (IsKeyPressed(KeyCodes.d)) {val &= 0xFB}
            if (IsKeyPressed(KeyCodes.f)) {val &= 0xF7}
            if (IsKeyPressed(KeyCodes.g)) {val &= 0xEF}
        case 0xFB:
            if (IsKeyPressed(KeyCodes.q)) {val &= 0xFE}
            if (IsKeyPressed(KeyCodes.w)) {val &= 0xFD}
            if (IsKeyPressed(KeyCodes.e)) {val &= 0xFB}
            if (IsKeyPressed(KeyCodes.r)) {val &= 0xF7}
            if (IsKeyPressed(KeyCodes.t)) {val &= 0xEF}
        case 0xF7:
            if (IsKeyPressed(KeyCodes.one)) {val &= 0xFE}
            if (IsKeyPressed(KeyCodes.two)) {val &= 0xFD}
            if (IsKeyPressed(KeyCodes.three)) {val &= 0xFB}
            if (IsKeyPressed(KeyCodes.four)) {val &= 0xF7}
            if (IsKeyPressed(KeyCodes.five)) {val &= 0xEF}
        case 0xEF:
            if (IsKeyPressed(KeyCodes.zero)) {val &= 0xFE}
            if (IsKeyPressed(KeyCodes.nine)) {val &= 0xFD}
            if (IsKeyPressed(KeyCodes.eight)) {val &= 0xFB}
            if (IsKeyPressed(KeyCodes.seven)) {val &= 0xF7}
            if (IsKeyPressed(KeyCodes.six)) {val &= 0xEF}
        case 0xDF:
            if (IsKeyPressed(KeyCodes.p)) {val &= 0xFE}
            if (IsKeyPressed(KeyCodes.o)) {val &= 0xFD}
            if (IsKeyPressed(KeyCodes.i)) {val &= 0xFB}
            if (IsKeyPressed(KeyCodes.u)) {val &= 0xF7}
            if (IsKeyPressed(KeyCodes.y)) {val &= 0xEF}
        case 0xBF:
            if (IsKeyPressed(KeyCodes.returnKey)) {val &= 0xFE}
            if (IsKeyPressed(KeyCodes.l)) {val &= 0xFD}
            if (IsKeyPressed(KeyCodes.k)) {val &= 0xFB}
            if (IsKeyPressed(KeyCodes.j)) {val &= 0xF7}
            if (IsKeyPressed(KeyCodes.h)) {val &= 0xEF}
        case 0x7F:
            if (IsKeyPressed(KeyCodes.space)) {val &= 0xFE}
            if (IsKeyPressed(KeyCodes.option)) {val &= 0xFD}
            if (IsKeyPressed(KeyCodes.m)) {val &= 0xFB}
            if (IsKeyPressed(KeyCodes.n)) {val &= 0xF7}
            if (IsKeyPressed(KeyCodes.b)) {val &= 0xEF}
        default:
            val &= 0xFF
        }
        return val
    }
    
}

enum KeyCodes: UInt16 {
    //Special Chars
    case space             = 0x31
    case returnKey         = 0x24
    case enterKey          = 0x4C
    case escape            = 0x35
    case shift             = 0x38
    case command           = 0x37
    case control           = 0x3B
    case option            = 0x3A
    case delete            = 0x33
    case singleQuote       = 0x27
    case semicolon         = 0x29
    case comma             = 0x2b
    case period            = 0x2f
    case slash             = 0x2c
    case squareOpen        = 0x21
    case squareClose       = 0x1e
    case backslash         = 0x2a
    case minus             = 0x1b
    case equalKey          = 0x18
    case tab               = 0x30

    
    //DPad Keys
    case leftArrow         = 0x7B
    case rightArrow        = 0x7C
    case downArrow         = 0x7D
    case upArrow           = 0x7E
    
    //Alphabet
    case a                 = 0x00
    case b                 = 0x0B
    case c                 = 0x08
    case d                 = 0x02
    case e                 = 0x0E
    case f                 = 0x03
    case g                 = 0x05
    case h                 = 0x04
    case i                 = 0x22
    case j                 = 0x26
    case k                 = 0x28
    case l                 = 0x25
    case m                 = 0x2E
    case n                 = 0x2D
    case o                 = 0x1F
    case p                 = 0x23
    case q                 = 0x0C
    case r                 = 0x0F
    case s                 = 0x01
    case t                 = 0x11
    case u                 = 0x20
    case v                 = 0x09
    case w                 = 0x0D
    case x                 = 0x07
    case y                 = 0x10
    case z                 = 0x06
    
    //Top Numbers
    case zero              = 0x1D
    case one               = 0x12
    case two               = 0x13
    case three             = 0x14
    case four              = 0x15
    case five              = 0x17
    case six               = 0x16
    case seven             = 0x1A
    case eight             = 0x1C
    case nine              = 0x19
    
    //Keypad Numbers
    case keypad0           = 0x52
    case keypad1           = 0x53
    case keypad2           = 0x54
    case keypad3           = 0x55
    case keypad4           = 0x56
    case keypad5           = 0x57
    case keypad6           = 0x58
    case keypad7           = 0x59
    case keypad8           = 0x5B
    case keypad9           = 0x5C
}



