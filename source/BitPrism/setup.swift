//
//  setup.swift
//  OSXZX
//
//  Created by Thomas Sturm on 1/8/22.
//  Distributed under Attribution-NonCommercial-ShareAlike 4.0 (CC BY-NC-SA 4.0)
//

import Cocoa
import AppKit
import Foundation

typealias Byte = UInt8
typealias Word = UInt16
typealias Address = Word

var logging : Bool = false

// level 1: only show system logs
// level 2: show opcodes and CPU logs
var loggingLevel : Int = 0

var disassemblerWindowViewController: DSViewControllerClass?

var disassemblerOn: Bool = false

struct disassemblerLabel {
    var labelID: Int = 0
    var labelAddress: Word = 0
    var labelName: String
    var labelType: Int = 0
}

var disassemblerLabels = [disassemblerLabel]()

var labelCounter:Int = 0

struct disassemblerItem {
    var itemAddress: Word = 0
    var itemText: String
}

let blankLine = disassemblerItem (
    itemAddress: Word(0),
    itemText: String("")
)

var disassemblerList = [disassemblerItem](repeating: blankLine, count: 65356)

var disassemblerListingCallback: (() -> Void)?
var disassemblerLabelCallback: (() -> Void)?

var disassemblerCount: Int = 0
let disassemblerFrequency: Int = 500

struct magicNumber {
    var itemAddress: Word = 0
    var itemType: Int = 0
    var itemName: String
    var itemText: String
}

var magicNumbers = [magicNumber]()

var thisOpcode : String = ""
var thisTarget : Word = 0x0000

let tStateLength : Float = 0.000000143  // each tState at 3.5 MHz is 0.143 microseconds

let frameStates : Int = 69888  // how many tStates fit into 1/50 of a second (one frame interrupt)
// the true number is 69888 tStates per frame: https://worldofspectrum.org/faq/reference/48kreference.htm#PortFE
let msStates : Int = 3494   // t-states per ms

var lastTimeRendererFinished: Int64 = 0  // temp temp temp
var lastTimeRendererStarted: Int64 = 0

var emulationResetDone = false
var emulationRenderPause: Bool = false
var emulationStop: Bool = false
var emulationCounter: Int = 0
var emulationBreakpoint: Int = -1

// status flags for loading files and storage area for TZX "tape"
var TZXdata: [UInt8] = []
var emulationTZXInstantMode: Bool = true
var emulationTZXReady: Bool = false
var emulationTZXPosition: Int = 0

struct cassetteBlock {
    var lengthPilot: Int = 0    // in t-states; default 2168
    var lengthSync1: Int = 0    // default 667
    var lengthSync2: Int = 1    // default 735
    var lengthBitZero: Int = 0  // default 855
    var lengthBitOne: Int = 0   // default 1710
    var lengthLeader: Int = 0   // number of pilot pulses
    var lengthPause: Int = 0    // in ms, default 1000
    var lengthData: Int = 0     // in bytes
    var startData: Word = 0     // first byte position in the TZX data
}

var cassetteBlocks = [cassetteBlock]()
var cassetteHead: Int = 0
var cassetteBlockStatus = 0
var cassettePlayTone = 0
var cassettePlayToneReset = 0
var cassettePlayToneUp = true
var cassetteRepeatCount = 0
var cassetteCurrentByteOffset = 0
var cassetteCurrentByte: UInt8 = 0
var cassetteBitCount = 0
var cassettePauseCount = 0

var overlayMessage = ""         // Message to be written to pixel buffer after each screen refresh
var overlayAtY = 0              // overlay has access to full pixel texture of 320x256, so x: 0-39, y: 0-31
var overlayAtX = 0
var overlayColor = 0            // like speccy colors
var overlayBackground = 7       // like speccy colors
var romCharSet : Word = 0x3D00

// status flags and variables for Metal Bridge
var metaShaderType = 0          // 0: flat; 1: lighting
var metaClearVoxels = 0         // any value other than 0 leads to clearing all voxels on the next render loop, set with OUT 149,1
var metaScreenAngleX = 32768            // camera angle along X axis - 128 sets the rectangle with the Spectrum texture to face directly forward; can be changed with OUT to port 151 (0x97)
var metaScreenAngleY = 32768            // camera angle along Y axis - 128 sets the rectangle with the Spectrum texture to face directly forward; can be changed with OUT to port 153 (0x99)
var metaScreenAngleXTarget = 32768     // camera angle UI modifier
var metaScreenAngleYTarget = 32768     // camera angle UI modifier - this can be changed with the perspective menu commands
var metaScreenPosY = 0
var metaCameraPosX = 32768      // camera position (16-bit)
var metaCameraPosY = 32768      // camera position (16-bit)
var metaCameraPosZ = 25600      // camera position (16-bit) will be divided by Renderer - (0/0/2) is default position
var metaPlotDepth = 0           // depth of a new voxel that will be generated with the next PLOT command. OUT to port 155 (0x9B) with a value > 0 primes the PLOT redirect
var metaPlotColor = 0           // voxel color can be changed with OUT 157 (0x9D) - it stays the same for all subseuqent voxels until changed again
var metaNoZXPlot = 0            // if this is anything else than 0, prevent PLOT from painting to ZX canvas, change with OUT 159 (0x9F)
var metaDefaultWidth = 3        // voxel size - change with OUT 161 (0xA1)
var metaDefaultHeight = 3       // voxel size - change with OUT 163 (0xA3)
var metaDefaultDepth = 3        // voxel size - change with OUT 165 (0xA5)
var metaChangeVoxel = 0
var metaDefaultVectorX = 0      // change with OUT 171 (0xAB)
var metaDefaultVectorY = 0      // change with OUT 173 (0xAD)
var metaDefaultVectorZ = 0      // change with OUT 175 (0xAF)
var metaVoxelCountdown = 0      // change with OUT 177 (0xB1)
var metaVectorHold = 10         // inhibits all vector calculations if set to 0 with OUT 179 (0xB3), otherwise global vector speed control with 10 allowing a vector of 1 moving a coordinate by 1 per 1/50 sec
var metaVoxelBehavior = 0       // determines what will happen at the end of the countdown
// 0: delete
// 2: reverse vector X, reset countdown
// 3: reverse vector Y, reset countdown
// 4: reverse vector Z, reset countdown
var metaPlotCanvas = 0           // Spectrum canvas mode
// 0: 256x192 standard canvas with default border
// 1: 320x256 canvas
// 2: TBD large size canvas
var metaXcoords16bit = 0        // used to pass 16-bit values for the plot and draw X coordinates around the 8-bit syntax barrier



struct Pixel {                  // pixels to plot on extended renderer canvas
    var x: Int
    var y: Int
    var c: Int
}
var pixelPipeline = [Pixel]()   // carry plot coords to the renderer

struct Voxel {  // x and y are the original Spectrum pixel coordinates; z is the distance to the screen in Metal vector space
    var x: Float
    var y: Float
    var z: Float
    var w = Int(3)
    var h = Int(3)
    var d = Int(3)
    var vx = Int(0)
    var vy = Int(0)
    var vz = Int(0)
    var color = Int(0)
    var node = Node(name: "")
    var new = Bool(true)
    var change = Bool(false)
    var countdown = Int(0)
    var countdownCopy = Int(0)
    var behavior = Int(0)
}
var voxelList = [Voxel]()

// keep track of which section of the screen had been modified - we only redraw changed sections
var screenModified: Bool = false
var screenSection0: Bool = true
var screenSection1: Bool = true
var screenSection2: Bool = true

var borderColor: Int = 0

// Speccy main memory space (ROM & RAM)
var mem = Array(repeating: UInt8(0), count : 65536)

// Speccy registers
var regB : Byte = 0
var regC : Byte = 0
var regD : Byte = 0
var regE : Byte = 0
var regH : Byte = 0
var regL : Byte = 0
var regA : Byte = 0
var regF : Byte = 0
var regR : Byte = 0
var regI : Byte = 0
var regIX : Word = 0
var regIY : Word = 0
var regPC : Word = 0
var regSP : Word = 0

// shadow registers
var regBshadow : Byte = 0
var regCshadow : Byte = 0
var regDshadow : Byte = 0
var regEshadow : Byte = 0
var regHshadow : Byte = 0
var regLshadow : Byte = 0
var regAshadow : Byte = 0
var regFshadow : Byte = 0

// CPU pins
var MI : Bool = false
var NMI : Bool = false

// internal flags
var IFF : Bool = false      // true: interrupts are disabled; false: enabled
var IMode : Int = 1            // Z80 Interrrupt Mode - the ROM sets it to 1, too and the Spectrum never uses a different mode(?)

var CPUHalt : Bool = false

// public flag positions
let flagC = 0x01
let flagN = 0x02
let flagPV = 0x04
let flagH = 0x10
let flagZ = 0x40
let flagS = 0x80
let flagAll = 0xD7

// IO Ports - this is essentially another 64KB memory space
var IOPorts = Array(repeating: UInt8(0), count : 65536)
var outFlag:Bool = false
var out16bit:UInt16 = 0     // we use this to buffer 16-bit values for our extended Basic OUT command

// CPU register helper methods
func getHL() -> Word { return  UInt16(Int(regL) + Int(regH) * 256)}
func setHL(newValue : UInt16) {
    regH = UInt8(newValue >> 8)
    regL = UInt8(newValue & 0xFF)
}


func getBC() -> Word { return  UInt16(Int(regC) + Int(regB) * 256)}
func setBC(newValue : UInt16) {
    regB = UInt8(newValue >> 8)
    regC = UInt8(newValue & 0xFF)
}


func getDE() -> Word {return  UInt16(Int(regE) + Int(regD) * 256)}
func setDE(newValue : UInt16) {
    regD = UInt8(newValue >> 8)
    regE = UInt8(newValue & 0xFF)
}


fileprivate extension String {
    func leftPad(with character: Character, length: UInt) -> String {
        let maxLength = Int(length) - count
        guard maxLength > 0 else {
            return self
        }
        return String(repeating: String(character), count: maxLength) + self
    }
}


extension Byte {
    var hex: String {
        String(format:"%02X", self)
    }

    var bin: String {
        String(self, radix: 2).leftPad(with: "0", length: 8)
    }
}


extension Word {
    var hex: String {
        String(format:"%04X", self)
    }
    
    var bin: String {
        String(self, radix: 2).leftPad(with: "0", length: 16)
    }
}


func logger(logmsg : String) {
    if (loggingLevel > 0) {
        print(logmsg)
    }
}


func logCode(address : String, token : String) {
    if (loggingLevel >= 2) {
        var registers : String = " "
        registers += "A:" + String(regA.hex)
        registers += " BC:" + String(toHexWord(num: getBC()))
        registers += " DE:" + String(toHexWord(num: getDE()))
        registers += " HL:" + String(toHexWord(num: getHL()))
        registers += " IX:" + String(toHexWord(num: regIX))
        registers += " IY:" + String(toHexWord(num: regIY))
        registers += " SP:" + String(toHexWord(num: regSP))
        registers += " F:" + String(regF.hex)
        var flags = " Flag S: " + String(regF & UInt8(flagS))
        flags += " Z: " + String(regF & UInt8(flagZ))
        flags += " H: " + String(regF & UInt8(flagH))
        flags += " P: " + String(regF & UInt8(flagPV))
        flags += " N: " + String(regF & UInt8(flagN))
        flags += " C: " + String(regF & UInt8(flagC))
        logger(logmsg: address + ":" + token + registers + flags)
    }
}


func setThisOpcode (opc : String, target : Word) {
    thisOpcode = opc
    thisTarget = target
}


func toHex(num : UInt8) -> String {
    let formatString = "%02X"
    return String(format:formatString, num)
}


func toHexWord(num : UInt16) -> String {
    let formatString = "%04X"
    return String(format:formatString, num)
}



func setColor(rgb: Int) -> CGColor {
    let newColor = CGColor(
        red: CGFloat(Float((rgb >> 16) & 0xFF)/255),
        green: CGFloat(Float((rgb >> 8) & 0xFF)/255),
        blue: CGFloat(Float(rgb & 0xFF)/255),
        alpha: 1
    )
    return newColor
}



// =================================== Loading Speccy ROM ======================================

//Read a rom at a given path and return a byte array or nil
func RomRead() -> [UInt8]?{
    //Create a blank byte array
    var romByteArray = [UInt8]()
    
    let fileName = "48"
    let fileExt = "rom"

    let filePath = Bundle.main.path(forResource: fileName, ofType: fileExt)
    
    //Read contents of the file at the given path
    guard let data = NSData(contentsOfFile: filePath!) else {
        print("file error")
        return nil
    }
    
    //Create a buffer array
    var buffer = [UInt8](repeating: 0, count: data.length)
    
    //Read Bytes
    data.getBytes(&buffer, length : data.length)
    
    //Copy buffer to the romByteArray
    romByteArray = buffer
    
    //Read was successful, return the byteArray
    return romByteArray
}


func loadRom() {
    let x : [uint8]! = RomRead()
    // copy the ROM into the Speccy memory space, starting at 0
    let z = x.count
    for i in 0..<z{
        mem[i] = x[i]
    }
}


// =================================== Loading SNA and TZX Files ======================================


func FileRead(filePath: URL) -> [UInt8]?{
    var array = [UInt8]()
    do {
        let nsData = try Data(contentsOf:filePath, options: [.alwaysMapped , .uncached ] )
        array = [UInt8](nsData as Data)
        return array
    } catch {
        logger(logmsg: "Fatal File Error: \(error).")
        return nil
    }
}

func loadSNA(thePath: URL) {
    let x : [uint8]! = FileRead(filePath: thePath)
    var z = x.count

    logger(logmsg: "Received SNA File, size: "+String(z))
    
    z = z - 27  // copy SNA without the first 27 bytes (raw memory snapshot of 48K RAM)
    
    for i in 0..<z {
        mem[i + 16384] = x[i + 27]
    }

    // read out the stored register values from the .SNA
    regI = x[0]
    regLshadow = x[1]
    regHshadow = x[2]
    regEshadow = x[3]
    regDshadow = x[4]
    regCshadow = x[5]
    regBshadow = x[6]
    regFshadow = x[7]
    regAshadow = x[8]
    regL = x[9]
    regH = x[10]
    regE = x[11]
    regD = x[12]
    regC = x[13]
    regB = x[14]
    regIX = UInt16(Int(x[15]) + Int(x[16]) * 255)
    regIY = UInt16(Int(x[17]) + Int(x[18]) * 255)
    if (x[19] & 0x04 == 1) {
        IFF = true
    } else {
        IFF = false
    }
    regR = x[20]
    regF = x[21]
    regA = x[22]
    regSP = UInt16(Int(x[23]) + Int(x[24]) * 256)
    logger(logmsg: "regSP: "+String(toHexWord(num: regSP)))
    IMode = Int(x[25])

    // After loading a .SNA, execute RETN functionality
    let lo = mem[Int(regSP)]
    let tmpAdr = regSP + 1
    let hi = mem[Int(tmpAdr)]
    let tmp = Int(lo) + (Int(hi) * 256)
    regPC = UInt16(tmp)
    logger(logmsg: ".SNA loaded, execute RETN with new PC: "+String(toHexWord(num: regPC)))
    regSP = regSP + 2

//    logging = true      // DEBUG ONLY - TURN ON FULL LOGGING AFTER LOAD
    
    emulationStop = false
    screenModified = true
}


func loadTZX(thePath: URL) {
    TZXdata = FileRead(filePath: thePath)!
    let TZXlength = TZXdata.count
    logger(logmsg: "Received TZX File, size: "+String(TZXlength))

    // in case we need an extract of the tzx we just snarfed in
//    for i in 0..<256 {
//        logger(logmsg: String(TZXdata[i].hex))
//    }

    emulationTZXReady = true
    emulationTZXPosition = 10   // we ignore the TZX header
    emulationStop = false
    screenModified = true
    
    // in case we are not in instant mode, we scan all blocks of the TZX and prepare a pulse table based on t-state
    // counts, which is then used to set the EAR bit during main loop iterations
    // Note: for slow mode to work smoothly, we might need to introduce a short pause before the real pulses begin, since the user has
    // to load the TZX and then needs to type LOAD "" into the BASIC interpreter
    if (!emulationTZXInstantMode) {
        cassetteSetup()
    }
}


func TZXGetByte() -> Byte {
    let tmp = TZXdata[emulationTZXPosition]
    emulationTZXPosition = emulationTZXPosition + 1
    return tmp
}

// if the targetAdr is 0, the data is not being saved into ZX RAM, but is stored as pulses in the cassetteBlocks array
func TZXcopyNextBlock(targetAdr: Word) -> Word {
    // first check for TZX header
    var TZXBlockHeader = TZXGetByte()
    var pauseLen: Int = 0
    var len: Int = 0

    logger(logmsg: "TZX Block Header: "+String(toHex(num: TZXBlockHeader)))
    
    // if there is a text block (ID 30) or group block (ID 21), we process it, log it to the console and then pull the next block
    if (TZXBlockHeader == 0x30  ||  TZXBlockHeader == 0x21) {           // TZX text block
        if (TZXBlockHeader == 0x30) {
            logger(logmsg: "TZX text block:")
        } else {
            logger(logmsg: "TZX group block:")
            // todo: remember that a group started (http://k1.spdns.de/Develop/Projects/zasm/Info/TZX%20format.html#GRPSTART)
        }
        let txtlen = TZXGetByte()
        var msg: String = ""
        for _ in 0..<txtlen {
            msg = msg + String(UnicodeScalar(UInt8(TZXGetByte())))
        }
        logger(logmsg: msg)
        TZXBlockHeader = TZXGetByte()
    }
    
    // if there is a archive block (ID 32), we pull the next block
    if (TZXBlockHeader == 0x32) {                   // TZX archive block
        logger(logmsg: "TZX archive block")
        let arclen = Int(Int(TZXGetByte()) + Int(256) * Int(TZXGetByte()))
        emulationTZXPosition = emulationTZXPosition + arclen
        TZXBlockHeader = TZXGetByte()
    }
    
    // process data blocks (ID 10)
    if (TZXBlockHeader == 0x10) {                   // TZX data block
        logger(logmsg: "TZX data block:")
        pauseLen = Int(TZXGetByte()) + 256 * Int(TZXGetByte())  // default for a regular data block is 1000ms
        len = Int(TZXGetByte()) + 256 * Int(TZXGetByte()) - 2   // we subtract 2 since we won't copy the TAP header and checksum bytes
        logger(logmsg: String(len) + " bytes")
        logger(logmsg: "block pause: " + String(pauseLen) + "ms")
        let tapHeader = TZXGetByte()                // TAP header is one byte - for fast mode this advances the read position beyond the TAP header
        if (targetAdr == 0) {                       // slow loader mode, create the cassette block
            var leaderLength = 8063                 // header block
            if (tapHeader >= 128) {
                logger(logmsg: "...create data block")
                leaderLength = 3223                 // data block
            } else {
                logger(logmsg: "...create header block")
            }
            setPulse(lp: 2168, ls1: 667, ls2: 735, b0: 855, b1: 1710, ll: leaderLength, pa: (pauseLen * msStates), ld: len+1, st: Word(emulationTZXPosition-1))
            emulationTZXPosition = emulationTZXPosition + len
        } else {                                    // fast loader mode, we copy the data block straight into the emulation memory
            logger(logmsg: "fast mode: write TZX data to memory")
            for i in 0..<len {
                let nxtByte = TZXGetByte()
                mem[Int(targetAdr) + i] = nxtByte
            }
        }
        emulationTZXPosition = emulationTZXPosition + 1 // skip TAP checksum
    }

    if (TZXBlockHeader == 0x11) {                   // TZX data block
        logger(logmsg: "TZX turbo speed data block:")
        let turboPilotPulse = Int(TZXGetByte()) + 256 * Int(TZXGetByte())   // default 2168
        let turboSyncFirst = Int(TZXGetByte()) + 256 * Int(TZXGetByte())   // default 667
        let turboSyncSecond = Int(TZXGetByte()) + 256 * Int(TZXGetByte())   // default 735
        let turboZeroBit = Int(TZXGetByte()) + 256 * Int(TZXGetByte())   // default 855
        let turboOneBit = Int(TZXGetByte()) + 256 * Int(TZXGetByte())   // default 1710
        let leaderLength = Int(TZXGetByte()) + 256 * Int(TZXGetByte())   // default 3223 for data block
        let usedBits = Int(TZXGetByte())    // TODO: don't know yet what to do with this info - number of bits used in last byte of data block; see http://k1.spdns.de/Develop/Projects/zasm/Info/TZX%20format.html#TURBOSPEED
        pauseLen = Int(TZXGetByte()) + 256 * Int(TZXGetByte())  // default for a regular data block is 1000ms
        len = Int(TZXGetByte()) + 256 * Int(TZXGetByte()) - 2   // we subtract 2 since we won't copy the TAP header and checksum bytes
        logger(logmsg: String(len) + " bytes")
        logger(logmsg: "block pause: " + String(pauseLen) + "ms")
        let tapHeader = TZXGetByte()                // TAP header is one byte - for fast mode this advances the read position beyond the TAP header
        if (targetAdr == 0) {                       // slow loader mode, create the cassette block
            setPulse(lp: turboPilotPulse, ls1: turboSyncFirst, ls2: turboSyncSecond, b0: turboZeroBit, b1: turboOneBit, ll: leaderLength, pa: (pauseLen * msStates), ld: len+1, st: Word(emulationTZXPosition-1))
            emulationTZXPosition = emulationTZXPosition + len
        } else {                                    // fast loader mode, we copy the data block straight into the emulation memory
            logger(logmsg: "fast mode: write TZX turbo data block to memory")
            for i in 0..<len {
                let nxtByte = TZXGetByte()
                mem[Int(targetAdr) + i] = nxtByte
            }
        }
        emulationTZXPosition = emulationTZXPosition + 1 // skip TAP checksum
    }

    return Word(len)
}


// =================================== Saving TZX Files ======================================


func saveTZX(ZXHeaderStart: Word, ZXBASICStart: Word, ZXName: String, ZXLength: Int) {
    // add the correct file name extension
    let fullName = ZXName + ".tzx"
    if let url = showSavePanel(filename: fullName) {
        logger(logmsg: "new url: "+String(url.description))
        // length:
        // 10 bytes: TZX header
        // 24 bytes: ZX header (17 bytes) in TAP and TZX wrapper (ZX header reports data length as ZXLength)
        // data: raw bytes (ZXLength) + 2 bytes TAP header + 5 bytes TZX header (tzx header reports ZXLength+3)
        let tzxLength: Int = 34 + ZXLength + 7
        var data = Data.init(capacity:tzxLength)
        // add the TZX header
        data.append(0x5A)
        data.append(0x58)
        data.append(0x54)
        data.append(0x61)
        data.append(0x70)
        data.append(0x65)
        data.append(0x21)
        data.append(0x1A)
        data.append(0x01)     // v1.0
        data.append(0x00)
        // add the ZX header as a TZX data block with enclosed TAP data
        data.append(0x10)    // TZX data block ID
        data.append(0x00)    // pause
        data.append(0x00)    // pause
        data.append(0x13)    // length (19 bytes)
        data.append(0x00)    // length
        data.append(0x00)    // TAP 00 header / FF data
        for i in 0...16 {  // 17 bytes original ZX header
            let ZXHeaderByte = mem[Int(Int(ZXHeaderStart)+i)]
            data.append(ZXHeaderByte)
        }
        data.append(0x00)    // TAP checksum (invalid since we don't care during loading later)
        data.append(0x10)    // TZX data block ID
        data.append(0x00)    // pause
        data.append(0x00)    // pause
        let TAPDataLength = UInt16(ZXLength + 2)
        data.append(Byte(TAPDataLength & 0x00FF))
        data.append(Byte((TAPDataLength >> 8) & 0x00FF))  // ZX Length plus TAP flag and checksum
        data.append(0xFF)    // TAP 00 header / FF data
        for i in 0...(ZXLength-1) {  // now copy the ZX BASIC program with its variables
            let ZXDataByte = mem[Int(Int(ZXBASICStart)+i)]
            data.append(ZXDataByte)
        }
        data.append(0x00)    // TAP checksum (invalid since we don't care during loading later)
        // OK, the file should be complete, lets save this to disk
        saveFile(data: data, path: url)
    }
}


func showSavePanel(filename: String) -> URL? {
    let savePanel = NSSavePanel()
    savePanel.nameFieldStringValue = filename
    savePanel.canCreateDirectories = true
    savePanel.isExtensionHidden = false
    savePanel.title = "Save ZX Basic as TZX"
    savePanel.message = "Choose a folder and a name to store the file."
    savePanel.nameFieldLabel = "File name:"
    let response = savePanel.runModal()
    return response == .OK ? savePanel.url : nil
}


func saveFile(data: Data, path: URL) {
    do {
        try data.write(to: path)
    } catch {
        print(error)
    }
}


// =================================== Cassette Routines ======================================


// if we've been loading a TZX in slow mode, we first need to create the array with the list of pulses
func cassetteSetup() {
    while (TZXdata.count > emulationTZXPosition){
        logger(logmsg: "TZX loop position: "+String(emulationTZXPosition))
        let len = TZXcopyNextBlock(targetAdr: 0)
//        logger(logmsg: "TZX loop previous block length: "+String(len))
        logger(logmsg: "TZX loop new position: "+String(emulationTZXPosition))
        if (TZXdata.count > emulationTZXPosition) {
            // pull header of the next block for lookahaed
            let tmp = TZXdata[emulationTZXPosition]
            logger(logmsg: "TZX loop next block header: "+String(toHex(num: tmp)))
            if (tmp != 0x10 && tmp != 0x11 && tmp != 0x21 && tmp != 0x30 && tmp != 0x32) {
                // this is necessary since we don't recognize all TZX blocks yet and we could end up in an endless loop
                break;
            }
        }
    }
    cassetteHead = 0
    cassetteBlockStatus = 0
    cassettePauseCount = 0
}


//var lengthPilot: Int = 0    // in t-states; default 2168
//var lengthSync1: Int = 0    // default 667
//var lengthSync2: Int = 1    // default 735
//var lengthBitZero: Int = 0  // default 855
//var lengthBitOne: Int = 0   // default 1710
//var lengthLeader: Int = 0   // number of pilot pulses
//var lengthPause: Int = 0    // in tStates
//var lengthData: Int = 0     // in bytes
//var startData: Word = 0     // first byte position in the TZX data

func setPulse(lp: Int, ls1: Int, ls2: Int, b0: Int, b1: Int, ll: Int, pa: Int, ld: Int, st: Word) -> Void {
    let pulse = cassetteBlock (
        lengthPilot: lp,
        lengthSync1: ls1,
        lengthSync2: ls2,
        lengthBitZero: b0,
        lengthBitOne: b1,
        lengthLeader: ll,
        lengthPause: pa,
        lengthData: ld,
        startData: st
    )
    cassetteBlocks.append(pulse)
}

