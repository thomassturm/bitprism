BitPrism 


=========================================================================================
ZX BASIC to OSX Metal Bridge
=========================================================================================
OUT 139 (0x8B) 0 -> screen in view; 1 -> screen out of view (achieved by moving it far up)

OUT 141 (0x8D) 0 -> shader with flat lighting; 1 -> shader with realistic lighting

OUT 143 (0x8F) camera position x, default is 32768

OUT 145 (0x91) camera position y, default is 32768

OUT 147 (0x93) camera position z, default is 25600

OUT 149 (0x95) with value > 0 clears all voxels

OUT 151 (0x97) camera angle along X axis, 32768 is centered towards main texture rectangle (the "screen")

OUT 153 (0x99) camera angle along Y axis, 32768 is centered towards main texture rectangle (the "screen")

OUT 155 (0x9B) with a value > 0 primes the PLOT redirect and sets plot z-axis

OUT 157 (0x9D) with a value 0...15 for voxel color

OUT 159 (0x9F) with a value 0 (allow) or 1 to block PLOT from using ULA canvas

OUT 161 (0xA1) voxel width (default is 3)

OUT 163 (0xA3) voxel height (default is 3)

OUT 165 (0xA5) voxel depth (default is 3)

IN 167  (0xA7) returns the ID of the last PLOTed voxel

OUT 169 (0xA9) voxel ID, the next PLOT will change the values for this voxel instead of creating a new one
		(note: we count voxel IDs in BASIC from 1...255) while internally voxels are addressed from 0...

OUT 171 (0xAB) voxel vector x (0-255, 128 is no movement)

OUT 173 (0xAD) voxel vector y (0-255, 128 is no movement)

OUT 175 (0xAF) voxel vector z (0-255, 128 is no movement)

OUT 177 (0xB1) voxel countdown (0 is no countdown)

OUT 179 (0xB3) vector control (default is 10, 0 inhibits all vectors, <10 slow down, >10 speed up)

OUT 181 (0xB5) voxel countdown behavior (0: delete; 1-3: reverse x,y,z vector)

=========================================================================================
Keyboard:
=========================================================================================
SHIFT -> CAPS SHIFT

OPTION -> SYMBOL SHIFT

TAB -> CAPS SHIFT-1 EDIT



