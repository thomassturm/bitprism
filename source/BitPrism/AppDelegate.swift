//
//  AppDelegate.swift
//  BitPrism
//
//  Created by Thomas Sturm on 3/21/22.
//  Distributed under Attribution-NonCommercial-ShareAlike 4.0 (CC BY-NC-SA 4.0)
//

import Cocoa

@main
class AppDelegate: NSObject, NSApplicationDelegate {
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func applicationSupportsSecureRestorableState(_ app: NSApplication) -> Bool {
        return true
    }
}

