//
//  z80-control.swift
//  OSXZX
//
//  Created by Thomas Sturm on 1/9/22.
//  Distributed under Attribution-NonCommercial-ShareAlike 4.0 (CC BY-NC-SA 4.0)
//

import Foundation

var tStatesTotal : Int = 0      // total tStates since the start of the emulation
var tStatesCounter : Int = 0    // resets every 50th of a second
var opcodeCounter : Int = 0

public final class Spectrum {
    var timer = Timer()

    func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    var lastTimeRunStart: Int64 = 0
    var lastTimeRunEnd: Int64 = 0

    func startAtNew() {
        regPC = 0x11B7
    }
    
    func reset() {
        logger(logmsg: "system reset")
        logger(logmsg: "logging: "+String(logging))
        logger(logmsg: "loggingLevel: "+String(loggingLevel))
             
        metaScreenAngleX = 32768
        metaScreenAngleY = 32768

        // set all RAM to 0, but leave ROM alone
        let z = mem.count
        for i in 0x4000..<z{
            mem[i] = 0
        }
        
        IFF = false     // allow interrupts
        
        // set all registers to their defaults
        regB = 0
        regC = 0
        regD = 0
        regE = 0
        regH = 0
        regL = 0
        regA = 0xff             // todo: need to confirm this
        regF = 0
        regI = 0
        regR = 0
        regIX = 0
        regIY = 0
        regPC = 0
        regSP = 0xffff
        regBshadow = 0
        regCshadow = 0
        regDshadow = 0
        regEshadow = 0
        regHshadow = 0
        regLshadow = 0
        regAshadow = 0
        regFshadow = 0
        
        // set IO Ports to sane defaults
        IOPorts[0x00FE] = 0xFF   // ULA Control Port
        
        // preset keyboard to all keys unpressed
        IOPorts[0xFEFE] = 0xFF
        IOPorts[0xFDFE] = 0xFF
        IOPorts[0xFBFE] = 0xFF
        IOPorts[0xF7FE] = 0xFF
        IOPorts[0xEFFE] = 0xFF
        IOPorts[0xDFFE] = 0xFF
        IOPorts[0xBFFE] = 0xFF
        IOPorts[0x7FFE] = 0xFF
    }
    
    
    func manageKeyboard() {
        // poll keyboard state
        IOPorts[0xFEFE] = Keyboard.getSpectrumKeyRow(row: 0xFE)
        IOPorts[0xFDFE] = Keyboard.getSpectrumKeyRow(row: 0xFD)
        IOPorts[0xFBFE] = Keyboard.getSpectrumKeyRow(row: 0xFB)
        IOPorts[0xF7FE] = Keyboard.getSpectrumKeyRow(row: 0xF7)
        IOPorts[0xEFFE] = Keyboard.getSpectrumKeyRow(row: 0xEF)
        IOPorts[0xDFFE] = Keyboard.getSpectrumKeyRow(row: 0xDF)
        IOPorts[0xBFFE] = Keyboard.getSpectrumKeyRow(row: 0xBF)
        IOPorts[0x7FFE] = Keyboard.getSpectrumKeyRow(row: 0x7F)
    }
  
    
    @objc func run() {  // @objc needed to allow for timer call

        if emulationRenderPause == false {

            // timing CPU/Render comparison
            let currentTime = getCurrentMillis()
            let runCycleDiff = currentTime - lastTimeRunStart
            lastTimeRunStart = currentTime
            let timeEndDiff = currentTime - lastTimeRunEnd
            let renderStartDiff = currentTime - lastTimeRendererStarted
            let renderDoneDiff = currentTime - lastTimeRendererFinished
            if (logging) {
                logger(logmsg: String(runCycleDiff) + " : " + String(timeEndDiff) + " : CPU cycle: " + String(runCycleDiff-timeEndDiff) + " : " + String(renderStartDiff) + " : " + String(renderDoneDiff) + " : Render: " + String(renderStartDiff-renderDoneDiff))
            }
                
            let system = System()
            let z80 = Z80()
            
            var thisPC : UInt16 = 0
            var tStates : Int
            
            tStatesCounter = 0
            opcodeCounter = 0
            
            emulationRenderPause = false    // we remove the render pause as soon as the next timer triggers run() - this is in case the timer triggers before the renderer gets to the next 50Hz cycle (the renderer also resets emulationRenderPause)
            
            while emulationStop == false && emulationRenderPause == false {

                // ################# deal with a potential timer interrupt
                if (MI) {                                           // MI pin is high on the chip
                    MI = false
                    if (!IFF  &&  IMode == 1) {                     // interrupts are not masked and we are in the correct mode for the spectrum hardware
                        IFF = true                                  // mask further interrupts until the CPU hits EI in the code
                        regSP = regSP &- 2
                        if (CPUHalt) {
                            CPUHalt = false
                            regPC += 1                              // if we were in HALT, need to advance PC to break out of HALT after interrupt
                        }
                        system.setMemWord(adr: regSP, val: regPC)   // put PC on Stack
                        regPC = 0x0038                              // set PC to 0x0038
                    } else {
                        //                    logger(logmsg: "Interrupt Masked")
                    }
                } // end of MI timer interrupt handler
                
                // possibly deal with NMI here (see https://github.com/hmtinc/Z80-Swift/blob/master/Source/z80.swift)
                
                thisPC = regPC
                
                // only 7 bits are incremented in R after instruction fetch, bit 8 is untouched
                regR = (regR & 0x80) | (((regR & 0x7F) + 1) & 0x7F)
                
                tStates = z80.opcode() // execute the next opcode at PC
                
                tStatesTotal += tStates
                tStatesCounter += tStates
                opcodeCounter += 1

                if (tStatesCounter > frameStates) {                 // 1/50th of a second worth of tStates have passed
                    tStatesCounter = 0
                    MI = true                                       // trigger timer/keyboard interrupt
                    emulationRenderPause = true
                    manageKeyboard()
                }

                // ##############################################################
                //  ################# TZX slow loader
                // ##############################################################
                if (!emulationTZXInstantMode && emulationTZXReady) {
                    // cassetteBlockStatus:
                    // 0 - start of a new block, need to initialize block sequence
                    // 1 - leader
                    // 2 - sync tone 1
                    // 3 - sync tone 2
                    // 4 - data

                    if (cassetteBlockStatus == 0 && cassettePauseCount == 0) {     // start a new block
                        logger(logmsg: "LOADER --- cassetteBlockStatus 0")
                        cassettePlayTone = 0
                        cassetteBlockStatus = 1 // switch to leader
                        cassetteCurrentByteOffset = 0
                        cassetteBitCount = 7
                    }

                    if (cassetteBlockStatus == 1 && cassettePlayTone == 0) {                        // start the leader
                        logger(logmsg: "LOADER --- cassetteBlockStatus 1 - leader - "+String(tStatesTotal)+" ("+String(tStatesTotal/msStates)+"ms)")
//                        cassettePlayTone = cassetteBlocks[cassetteHead].lengthPilot/2
                        cassettePlayTone = cassetteBlocks[cassetteHead].lengthPilot
                        logger(logmsg: "LOADER --- cassettePlayTone: "+String(cassettePlayTone))
                        cassettePlayToneReset = cassettePlayTone
                        cassetteRepeatCount = cassetteBlocks[cassetteHead].lengthLeader             // the leader tone repeats the pulse
// temp
//                        cassettePlayTone = cassettePlayTone * cassetteRepeatCount

                        cassettePlayToneUp = true
                        cassettePauseCount = cassetteBlocks[cassetteHead].lengthPause
                    }

                    if (cassetteBlockStatus == 2 && cassettePlayTone == 0) {                        // start sync 1
                        logger(logmsg: "LOADER --- cassetteBlockStatus 2 - sync 1 - "+String(tStatesTotal)+" ("+String(tStatesTotal/msStates)+"ms)")
                        cassettePlayTone = cassetteBlocks[cassetteHead].lengthSync1/2
//                        cassettePlayTone = cassetteBlocks[cassetteHead].lengthSync1
                        logger(logmsg: "LOADER --- cassettePlayTone: "+String(cassettePlayTone))
                        cassettePlayToneReset = cassettePlayTone
                        cassetteRepeatCount = 0                                                     // single pulse
                        cassettePlayToneUp = true
                    }

                    if (cassetteBlockStatus == 3 && cassettePlayTone == 0) {                        // start sync 2
                        logger(logmsg: "LOADER --- cassetteBlockStatus 3 - sync 2 - "+String(tStatesTotal)+" ("+String(tStatesTotal/msStates)+"ms)")
                        cassettePlayTone = cassetteBlocks[cassetteHead].lengthSync2/2
//                        cassettePlayTone = cassetteBlocks[cassetteHead].lengthSync2
                        logger(logmsg: "LOADER --- cassettePlayTone: "+String(cassettePlayTone))
                        cassettePlayToneReset = cassettePlayTone
                        cassetteRepeatCount = 0                                                     // single pulse
                        cassettePlayToneUp = true
                    }

                    if (cassetteBlockStatus == 4 && cassettePlayTone == 0) {                        // set new data bit
                        overlayMessage = "Loading Byte " + String(cassetteCurrentByteOffset) + "          "

                        //                        logger(logmsg: "cassetteBlockStatus 4 - data - "+String(tStatesTotal)+" ("+String(tStatesTotal/msStates)+"ms)")
//                        logger(logmsg: "Byte: "+String(cassetteCurrentByteOffset)+" Bit: "+String(cassetteBitCount))
                        if (cassetteCurrentByteOffset <= cassetteBlocks[cassetteHead].lengthData) {
                            cassetteCurrentByte = TZXdata[Int(cassetteBlocks[cassetteHead].startData) + cassetteCurrentByteOffset]

                            
                            let thisBit = cassetteCurrentByte & UInt8(0x01 << cassetteBitCount)         // thisBit will be 0 if bit was 0
                            
                            if (thisBit == 0) {
                                cassettePlayTone = Int(cassetteBlocks[cassetteHead].lengthBitZero)
                            } else {
                                cassettePlayTone = Int(cassetteBlocks[cassetteHead].lengthBitOne)
                            }
                            cassettePlayToneReset = cassettePlayTone
                            cassetteRepeatCount = 0                                                     // for data bits always single pulse

                            // temp - this might be unneccesary
                            if (cassetteBitCount == 0 && cassetteCurrentByteOffset == cassetteBlocks[cassetteHead].lengthData) {
                                cassetteRepeatCount = 1
                                logger(logmsg: "LOADER --- repeat last bit")
                            }
                        // ######### temp repeat last bit this way
//                            if (cassetteBitCount == 0 && cassetteCurrentByteOffset == cassetteBlocks[cassetteHead].lengthData) {
//                                cassettePlayTone = cassettePlayToneReset
//                                cassettePlayToneUp = !cassettePlayToneUp
//                            }
                            
                        
//                        if (cassetteCurrentByteOffset <= cassetteBlocks[cassetteHead].lengthData) {  // TZX block is ongoing
                            cassettePlayToneUp = true
                        } else {                                                                    // TZX block has ended
                            overlayMessage = "                    "
                            cassettePlayTone = 0
                            cassetteHead += 1
                            if (cassetteHead < cassetteBlocks.count) {
                                cassetteBlockStatus = 0                                             // start the next block
                                logger(logmsg: "LOADER --- start next block, pause: "+String(cassettePauseCount)+" ("+String(tStatesTotal/msStates)+"ms)")

                                // add extra edge at the end of block to finish the last bit
//                                if (!cassettePlayToneUp) {
//                                    IOPorts[0x7FFE] = UInt8(IOPorts[0x7FFE] | 0x40)
//                                } else {
//                                    IOPorts[0x7FFE] = UInt8(IOPorts[0x7FFE] & 0xBF)
//                                }

                            } else {
                                // todo: what else do we need to reset
                                emulationTZXReady = false                                           // end TZX slow load
                                logger(logmsg: "LOADER --- cassetteHead exceeds block count - end slow load")
                                overlayMessage = "                              "
                            }
                        }
                        
                        cassetteBitCount -= 1
                        if (cassetteBitCount == -1) {
                            cassetteBitCount = 7
                            cassetteCurrentByteOffset += 1
                        }
                    }

                    // if the previous block had a pause specified, count it down after the
                    if (cassetteBlockStatus == 0 && cassettePauseCount != 0) {
                        cassettePauseCount = cassettePauseCount - tStates
                        if (cassettePauseCount < 0) {
                            cassettePauseCount = 0
                        }
                    }
                    
                    if (cassetteBlockStatus == 1 && cassettePlayTone > 0) {
                        overlayMessage = "Cassette Leader Tone " + String(cassetteRepeatCount) + "  "
                    }
                    
                    
                    if (cassettePlayTone != 0) {
                        // ROM loader routine (here: 0x05F1) checks IO at 0x7ffe
                        // EAR interface flips bit 6 based on edge of incoming wave signal
                        if (cassettePlayToneUp) {
                            IOPorts[0x7FFE] = UInt8(IOPorts[0x7FFE] | 0x40)
                        } else {
                            IOPorts[0x7FFE] = UInt8(IOPorts[0x7FFE] & 0xBF)
                        }
                        
                        cassettePlayTone = cassettePlayTone - tStates


                        if (cassettePlayTone <= 0) {                    // one tone length (one pulse half) has passed
                            cassettePlayTone = cassettePlayToneReset    // reset tone length for the next up/down pulse cycle
                            cassettePlayToneUp = !cassettePlayToneUp    // reverse the next pulse
                            if (cassettePlayToneUp || cassetteBlockStatus == 3) {    // if cassettePlayToneUp is true here, it means we went up->down->up, one pulse cycle is done; block 3 means we are in the second sync tone which is a single edge and then the first bit
                                if (cassetteRepeatCount > 0) {          // a leader tone, need to count down the repeat count for these up-down cycles
                                    cassetteRepeatCount -= 1
                                } else {                                // repeat count for pulses is 0, so data tone or leader is over
                                    cassettePlayTone = 0                // make sure the counter didn't overrun to negative values since it's also our flag to advance bits
                                    if (cassetteBlockStatus >= 1  &&  cassetteBlockStatus < 4) {
                                        cassetteBlockStatus += 1
                                    }
                                }
                            }
                        }

                    }
                                        
                }   // end TZX slow loader

                
                if (logging) {
                    logCode(address: toHexWord(num: thisPC), token: thisOpcode)
                }
                
                // ##############################################################################################
                // ######################################## Disassembler ########################################
                // ##############################################################################################
                // ### Feed new opcodes into disassembler
                if (disassemblerOn) {
                    if disassemblerList[Int(thisPC)].itemText == "" {
                        var labelName = ""
                        
                        // see if this is IN / OUT
                        let ioRegex = try! NSRegularExpression(pattern: "((IN )|(INI)|(IND)|(OUT)|(OTIR)|(OTDR))", options: NSRegularExpression.Options.caseInsensitive)
                        let ioMatches = ioRegex.matches(in: thisOpcode, options: [], range: NSMakeRange(0, thisOpcode.count))
                        if ioMatches.count != 0 {
                            labelName = "IO "
                            if thisTarget != 0x0000 {
                                labelName += String(toHexWord(num: thisTarget))
                            }
                        }

                        // the current opcode is a jump of some kind or we are directly loading/writing to a memory address
                        if thisTarget != 0x0000 && labelName == "" {
                            for i in 0..<disassemblerLabels.count {
                                if disassemblerLabels[i].labelAddress == thisTarget {
                                    labelName = disassemblerLabels[i].labelName
                                }
                            }
                            // at this point we have a interesting address and if labelName is not set this is a new label (could be system, IO or jump)
                            
                            // see if this is a label with a magic name, like a system variable
                            if labelName == "" {
                                for i in 0..<magicNumbers.count {
                                    if magicNumbers[i].itemAddress == thisTarget {
                                        var magicType = "System"
                                        if magicNumbers[i].itemType == 1 {
                                            magicType = "Memory"
                                        }
                                        if magicNumbers[i].itemType == 2 {
                                            magicType = "Routine"
                                        }
                                        if magicNumbers[i].itemName != "" {
                                            labelName = magicType + " " + magicNumbers[i].itemName
                                        } else {
                                            labelName = magicType + " " + magicNumbers[i].itemText
                                        }
                                    }
                                }
                            }
                            
                            // create a new label name
                            if labelName == "" {
                                // create a new label
                                labelName = "LABEL" + String(labelCounter)
                                let l = disassemblerLabel(
                                    labelID: labelCounter,
                                    labelAddress: Word(thisTarget),
                                    labelName: labelName,
                                    labelType: 0
                                )
                                labelCounter += 1
                                disassemblerLabels.append(l)
                            }
                        }
                        
                        // thisOpcode comes with the first byte in hex plus a space in the beginning. cut off that part of the string
                        // range up to the first space
                        let range = thisOpcode.range(of: " ")
                        let justOpCode = thisOpcode[(range?.upperBound...)!]
                        
                        var labelSeparator = ""
                        if labelName != "" {
                            labelSeparator = " : "
                        }
                        
                        let d = disassemblerItem(
                            itemAddress: Word(thisPC),
                            itemText: String(justOpCode) + labelSeparator + labelName
                        )

                        disassemblerList[Int(d.itemAddress)].itemAddress = d.itemAddress
                        disassemblerList[Int(d.itemAddress)].itemText = d.itemText
                    }
                    
                    // display current list (or section of list that fits on screen?)
                    if tStatesCounter == 0 {    // every 50th of a second
                        disassemblerCount += 1
                        if disassemblerCount == disassemblerFrequency {
                            disassemblerListingCallback?()
                            disassemblerLabelCallback?()
                            disassemblerCount = 0
                        }
                    }
                }
                // #################################################################################################
                // ##############################################################################################
                // ##############################################################################################
 
                
                emulationCounter += 1
                
                if (emulationCounter == emulationBreakpoint) {
                    emulationStop = true
                }
                
                
                
                // ################################ Trap OUT Routine ##################################
                // This allows us to capture 16-bit values - OUT uses TWO_PARAM which then calls FP_TO_A, which returns a 16-bit result
                // and OUT would then error out if the result is >255
                if (thisPC == 0x1e7a) {    // OUT_CMD, call to TWO_PARAM in progress
                    outFlag = true
                }
                if (thisPC == 0x1e88 && outFlag == true) {    // TWO_PARAM returns from FP_TO_A with Carry set to trigger error if
                    // result in BC is 16 bit - we need to buffer BC and jump to 1E8E to skip error handling
                    out16bit = getBC()  // we'll use this 16 bit value for some of our custom OUT calls to the Metal renderer
                    outFlag = false
                    regPC = 0x1E8E  // jump to TWO_P_1 to get the IO address for the OUT call
                }

                
                
                // ################################ Trap "B - Integer out of range" error for PLOT commands ##################################
                if (thisPC == 0x24F9) {    // B - Integer out of range error in progress
                    // result in BC is 16 bit
//emulationStop = true
                    let charAdd = system.fetchWordFrom(adr: 0x5C5D)
                    // the PLOT token 0xF6 can be a variable distance from the CHAR-ADD pointer, we need to scan for it
                    // positions CHAR-ADD-11 and 4 are for variables as coords, 18, 19, 20 depend on size of y coord value
                    if (system.fetchFrom(adr: charAdd-18) == 0xF6 || system.fetchFrom(adr: charAdd-19) == 0xF6 || system.fetchFrom(adr: charAdd-20) == 0xF6 || system.fetchFrom(adr: charAdd-11) == 0xF6 || system.fetchFrom(adr: charAdd-4) == 0xF6) {   // it's a PLOT command with the overflow in the X coordinate
                        metaXcoords16bit = Int(getBC())  // get the 16 bit value and store it for later in the PLOT routine override below
                        regSP = regSP &+ 2  // the RST for the error had already been called, fix the stack
                        regA = 0x00         // clean up A so the ROM thinks x = 0
                        regPC = 0x231a      // return to the STK_TO_A ROM routine as if nothing has happened
                    }
                }

                
                
                // ################################ Trap PLOT Routine ##################################
                if (thisPC == 0x22df) {         // BASIC enters PLOT
                    if (metaPlotDepth != 0) {         // voxel depth had been set, deal with Metal voxels
                        // y-coordinate to B, x to C
                        if (metaChangeVoxel == 0) {
                            // create a new voxel
                            let v = Voxel(
                                x: Float(regC),
                                y: Float(regB),
                                z: Float(metaPlotDepth),
                                w: Int(metaDefaultWidth),
                                h: Int(metaDefaultHeight),
                                d: Int(metaDefaultDepth),
                                vx: Int(metaDefaultVectorX),
                                vy: Int(metaDefaultVectorY),
                                vz: Int(metaDefaultVectorZ),
                                color: metaPlotColor,
                                countdown: Int(metaVoxelCountdown),
                                countdownCopy: Int(metaVoxelCountdown),
                                behavior: Int(metaVoxelBehavior)
                            )
                            voxelList.append(v)
                            // return new voxel ID to BASIC via Port 167
                            if (voxelList.count < 256) {    // we only return IDs for the first 255 voxels
                                IOPorts[Int(0x00a7)] = UInt8(voxelList.count)  // Port 167 - we return the (voxel id + 1), since 0 is the default for no change in Port 169
                            }
                        } else {
                            // change an existing voxel
                            voxelList[metaChangeVoxel-1].change = true
                            voxelList[metaChangeVoxel-1].x = Float(regC)
                            voxelList[metaChangeVoxel-1].y = Float(regB)
                            voxelList[metaChangeVoxel-1].z = Float(metaPlotDepth)
                            voxelList[metaChangeVoxel-1].w = Int(metaDefaultWidth)
                            voxelList[metaChangeVoxel-1].h = Int(metaDefaultWidth)
                            voxelList[metaChangeVoxel-1].d = Int(metaDefaultDepth)
                            voxelList[metaChangeVoxel-1].vx = Int(metaDefaultVectorX)
                            voxelList[metaChangeVoxel-1].vy = Int(metaDefaultVectorX)
                            voxelList[metaChangeVoxel-1].vz = Int(metaDefaultVectorZ)
                            metaChangeVoxel = 0
                        }
                    }
                    if (metaNoZXPlot == 1) {    // if this is set, we skip the regular ZX plot mechanism
                        regPC = 0x22E2          // jump to beyond the call to PLOT_SUB
                    } else {
                        if (metaPlotCanvas == 1) {  // we'll plot to the extended canvas including border
//                            logger(logmsg: "Plotting to extended canvas")
                            var tmpX = Int(regC)
                            if (metaXcoords16bit != 0) {
                                tmpX = metaXcoords16bit
                                metaXcoords16bit = 0
                            }
                            let p = Pixel(
                                x: tmpX,
                                y: Int(regB),
                                c: metaPlotColor
                            )
                            pixelPipeline.append(p)
                            regPC = 0x22E2          // extended plot always skips ROM routine, jump to beyond the call to PLOT_SUB
                        }
                    }
                }
                
                
                

                
                // ################################ Trap SAVE Routine ##################################
                // we wait for the user to press a key after "Start tape, then press any key."
                if (thisPC == 0x0984) {    // after SA_CNTRL and key press wait
                    logger(logmsg: "SA_CNTRL encountered")
                    var saveHeaderName: String = ""
                    // remove spaces from the end of the ZX file name
                    for i in 1...10 {
                        let charCode = mem[Int(z80.addOffset(adr: regIX, offset: Byte(i)))]
                        if (charCode != 0x20) {
                            saveHeaderName = saveHeaderName + String(UnicodeScalar(UInt8(charCode)))
                        }
                    }
                    let saveHeaderLength = UInt16(UInt16(mem[Int(z80.addOffset(adr: regIX, offset: 11))]) + UInt16(256) * UInt16(mem[Int(z80.addOffset(adr: regIX, offset: 12))]))
                    regSP = regSP &+ 2  // move SP back to the start address of the program data to be saved
                    let saveStartAddress = system.fetchWordFrom(adr: regSP)
                    
                    saveTZX(ZXHeaderStart: regIX, ZXBASICStart: saveStartAddress, ZXName: saveHeaderName, ZXLength: Int(saveHeaderLength))
                    
                    // at this point SP is one word off the call return address
                    regSP = regSP &+ 2
                    regPC = 0x053E  // end of SA_BYTES
                }
                
                

                // ################################ Trap LOAD Routine ##################################
                // return values based on Fuse source 1.6.0; tape.c:
                /* On exit:
                 *  A = calculated parity byte if parity checked, else 0 (CHECKME)
                 *  F : if parity checked, all flags are modified
                 *      else carry only is modified (FIXME)
                 *  B = 0xB0 (success) or 0x00 (failure)
                 *  C = 0x01 (confirmed), 0x21, 0xFE or 0xDE (CHECKME)
                 * DE : decremented by number of bytes loaded or verified
                 *  H = calculated parity byte or undefined
                 *  L = last byte read, or 1 if none
                 * IX : incremented by number of bytes loaded or verified
                 * A' = unchanged on error + no flag byte, else 0x01
                 * F' = 0x01      on error + no flag byte, else 0x45
                 *  R = no point in altering it :-)
                 * Other registers unchanged.
                 */
                // if emulationTZXInstantMode is true we intercept the ROM load routine and feed TZX data into memory. This will only work with code
                // that uses the standard ROM loader. Any custom loader code will wait forever for its data and will never finish.
                if (thisPC == 0x056c && TZXdata.count > emulationTZXPosition && emulationTZXInstantMode) {
                    logger(logmsg: "LD_START encountered")
                    if (emulationTZXReady) {
                        // we first need to fix the stack since the call at 56c has already been executed
                        regSP = regSP &+ 2
                        
                        logger(logmsg: "LD_START - TZX is in memory, copy next block")
                        let len = TZXcopyNextBlock(targetAdr: regIX)

                        regC = 0x01
                        setDE(newValue: z80.rollOver(val: Int(getDE())-Int(len)))
                        regIX = regIX &+ len

                        if (TZXdata.count > emulationTZXPosition) {
                            logger(logmsg: "Status: more data available")
                            // pull header of the next block for logging
                            let tmp = TZXdata[emulationTZXPosition]
                            logger(logmsg: "Next block header: "+String(toHex(num: tmp)))
                            if (tmp == 0x30  ||  tmp == 0x21) {
                                let len = TZXcopyNextBlock(targetAdr: regIX)
                            }
//                            regA = TZXdata[emulationTZXPosition-1]  // checksum of the block we just read
                            regB = 0xB0
                        } else {
                            logger(logmsg: "Status: last block - finish loading")
//                            regA = 0x00
                            regB = 0x00
                            emulationTZXReady = false
                        }
                        regF |= UInt8(flagC)
  
                        regAshadow = 0x01
                        regFshadow = 0x45
                        
                        regA = 0x00
                        regPC = 0x05e0  // we jump to cp 0x01 - if regA != 0 it indicates loading error since the carry flag will be reset by the compare
                        
//                        emulationTZXReady = false
                        
                        // temp temp temp
//                        logging = true
                    }
                }

                
                
                
                
                
                // ####################################################
                // division issues
                
                //            if (thisPC == 0x32c4  &&  getHL() != 0x5CE3) {         // end of re_stack on second number (dividend)
                //                // 8 -> 84 and 4 -> 83 in 0x5CDE
                //                emulationStop = true
                //            }
                
                //            if (thisPC == 0x32b1  &&  getHL() != 0x5CE6) {         // at RS_NRMLSE, before multiply/shift
                //                // 8 at 0xfce0 (0x5cde); 4 at 0c5ce5 (0x5ce3); HL holds 4 in H; DE points at 5ce6; B holds 0x89; A holds 0x04
                //                emulationStop = true
                //            }
                
                //            if (thisPC == 0x32b3  &&  getHL() != 0x5CE6) {         // first iteration of RSTK_LOOP
                //                // 8 at 0xfce0 (0x5cde); 4 at 0c5ce5 (0x5ce3); HL holds 4 in H; DE points at 5ce6; B holds 0x89; A holds 0x04
                //                emulationStop = true
                //            }
                
                //            if (thisPC == 0x3296  &&  getDE() == 0x5CE3) {         // first iteration of RSTK_LOOP
                //                //RESTK_SUB is done, 83 in 0x5Ce3 is the counter (B) of the restacking of 4; 0x5CE0 holds 8
                //                // DE:5CE3 HL:5CDE; HL points at sign byte of 8
                //                // this leads into second re_stack run for 8(?), RET will return to 0x31b2
                //                emulationStop = true
                //            }
                
                //            if (thisPC == 0x31c3) {         // end of re_stack on second number (dividend)
                //                //
                //                emulationStop = true
                //            }
                
                
                //            if (thisPC == 0x32c4  &&  system.fetchFrom(adr: 0x5ce3) == 0x83  &&  getHL() == 0x5cde) {         // first iteration of RSTK_LOOP
                //                //RESTK_SUB is done, 83 in 0x5Ce3 is the counter (B) of the restacking of 4; 0x5CE0 holds 8
                //                // DE:5CE3 HL:5CDE
                //                // this leads into second re_stack run for 8(?), RET will return to 0x31b2
                //                emulationStop = true
                //            }
                

                // ##########
                // ##########   Overwrite the RND routine - tricky, since the last_value on the calculator stack would have to be recreated
                // ##########
                // ##########
//                 if (thisPC == 0x25f8) {    // start of Basic RND
//                    regA = 0x87
//                    regB = 0x8e
//                    regC = 0x90
//                    regD = 0x5C
//                    regE = 0xD5
//                    regH = 0x5C
//                    regL = 0xD0
//                    regPC = 0x261a
//                }
//                if (thisPC == 0x261a) {    // start of Basic RND
//                    logging = true
//                }


                
                // ##########
                // ##########
                // ##########

                
            
            
            
            
            //
            
//            if (thisPC == 0x05F1 && regA != 0xff) {         // IN from IO port
//                emulationStop = true
//            }
            
//            if (thisPC == 0x05F3) {         // RRA after IN to check EAR and keyboard
//                IOPorts[0x7FFE] = UInt8(IOPorts[0x7FFE] & 0xBF)     // flip EAR bit for next loop
//            }
            
//            if (thisPC == 0x05FA) {         // coming out of LD_SAMPLE with an edge change
//                emulationStop = true
//            }
//
//            if (thisPC == 0x0564) {         // RRA in LD_BYTES after the first IN from port
//                emulationStop = true
//            }
//            if (thisPC == 0x056c) {         // LD_START
//                emulationStop = true
//            }
//                if (thisPC == 0x056f) {         // return with edge
//                    emulationStop = true
//                }
//                if (thisPC == 0x0571) {         // move on to pause and sync
//                    emulationStop = true
//                }
//                if (thisPC == 0x0580) {         // LD_LEADER
//                    emulationStop = true
//                }
                //                if (thisPC == 0x058A) {         // LD_LEADER check edge-2-edge 3000 T states
                //                    emulationStop = true
                //                }
//                if (thisPC == 0x058F) {         // LD_SYNC - 256 edge pairs found - header complete
//                  logger(logmsg: "LD_SYNC - 256 edge pairs found - header complete")
//                }

                //                if (thisPC == 0x05a2) {         // sync pulses done
                //                  emulationStop = true
                //                }
//                if (thisPC == 0x05c8) {         // LD_MARKER, begin loading data bits
//                  emulationStop = true
//                }

                
//                if (thisPC == 0x058F) {         // Speccy is now waiting for sync pulses
//                    logger(logmsg: "LD_SYNC start")
//                }

                
                
                //                if (thisPC == 0x05D0) {         // CP B the new bit into L
                //                    logger(logmsg: "ROM --- bit length, regB: "+String(regB))
                //                }
                //
                //                if (thisPC == 0x05D1) {         // RL L the new bit into L
                //                    logger(logmsg: "ROM --- new bit, regL: "+String(regL))
                //                }

/*
                if (thisPC == 0x0769) {         // LD_BYTES, begin loading data block
                    logger(logmsg: "ROM --- 0x0769 --- LD DE,$0011")
                    logger(logmsg: "regD: "+String(regD))
                    logger(logmsg: "regE: "+String(regE))
                }

                
                if (thisPC == 0x0556) {         // LD_BYTES, begin loading data block
                    logger(logmsg: "ROM --- 0x0556 --- start LD_BYTES")
                    logger(logmsg: "regA: "+String(regA))
                    logger(logmsg: "regF: "+String(regF))
                    logger(logmsg: "regD: "+String(regD))
                    logger(logmsg: "regE: "+String(regE))
                }
                
                if (thisPC == 0x059B) {
                    logger(logmsg: "ROM --- 0x059B --- LD_SYNC process LS_EDGE_1")
                }

                if (thisPC == 0x059F) {         // moving on to first data byte
                    logger(logmsg: "ROM --- 0x059F --- LD_SYNC end")
                }

                if (thisPC == 0x05A9) {         //
                    logger(logmsg: "ROM --- 0x05A9 --- LD_LOOP")
                    logger(logmsg: "regA: "+String(regA))
                    logger(logmsg: "regF: "+String(regF))
                    logger(logmsg: "regD: "+String(regD))
                    logger(logmsg: "regE: "+String(regE))
                }

                if (thisPC == 0x05B3) {         //
                    logger(logmsg: "ROM --- 0x05B3 --- LD_FLAG")
                    logger(logmsg: "regD: "+String(regD))
                    logger(logmsg: "regE: "+String(regE))
                }

                if (thisPC == 0x05C2) {         // decrease byte counter
                    logger(logmsg: "ROM --- 0x05C2 --- LD_NEXT")
                    logger(logmsg: "regD: "+String(regD))
                    logger(logmsg: "regE: "+String(regE))
                }

                if (thisPC == 0x05C4) {         // decrease byte counter
                    logger(logmsg: "ROM --- 0x05C4 --- LD_DEC")
                    logger(logmsg: "regD: "+String(regD))
                    logger(logmsg: "regE: "+String(regE))
                }

                if (thisPC == 0x05C8) {         // decrease byte counter
                    logger(logmsg: "ROM --- 0x05C8 --- LD_MARKER")
                    logger(logmsg: "regD: "+String(regD))
                    logger(logmsg: "regE: "+String(regE))
                }

                if (thisPC == 0x05d8) {         // XOR new byte in L to parity in A
                    logger(logmsg: "ROM --- 0x05d8 --- end LD_8_BITS, regL: "+String(regL))
                    logger(logmsg: "regA: "+String(regA))
                    logger(logmsg: "regD: "+String(regD))
                    logger(logmsg: "regE: "+String(regE))
                }

                if (thisPC == 0x05d9) {         // XOR new byte in L to parity in A
                    logger(logmsg: "ROM --- 0x05d9 --- parity updated with new value, regL: "+String(regL))
                    logger(logmsg: "regA: "+String(regA))
                    logger(logmsg: "regD: "+String(regD))
                    logger(logmsg: "regE: "+String(regE))
                }

                if (thisPC == 0x05e0) {         // end of LD_BYTES, CP $01 with A holding the final parity match byte
                    logger(logmsg: "ROM --- 0x05e0 --- CP $01, regA: "+String(regA))
                    logger(logmsg: "regH: "+String(regH))
                    logger(logmsg: "regL: "+String(regL))
                }
*/

//            if (thisPC == 0x060B) {         // entry into SAVE_ETC, setting T_ATTR to correct mode for load -> 0x01
//                emulationStop = true
//            }
//            if (thisPC == 0x061F) {         // prepare 30 bytes for header in SAVE_ETC
//                emulationStop = true
//            }

            
                
            }       // END OF 1/50 sec WHILE

            
            if (emulationStop == true) {
                loggingLevel = 2
                screenModified = true
                logger(logmsg: "=== Emulation Stop Signal Encountered ===")
                logCode(address: toHexWord(num: thisPC), token: String(system.fetchFrom(adr: thisPC).hex)+String(system.fetchFrom(adr: thisPC+1).hex))
                logger(logmsg: "Shadow BC:" + String(toHexWord(num: UInt16(Int(regCshadow) + Int(regBshadow) * 256))) + " DE:" + String(toHexWord(num: UInt16(Int(regEshadow) + Int(regDshadow) * 256))) + " HL:" + String(toHexWord(num: UInt16(Int(regLshadow) + Int(regHshadow) * 256))))
                logger(logmsg: "Flag S: " + String(regF & UInt8(flagS)))
                logger(logmsg: "Flag Z: " + String(regF & UInt8(flagZ)))
                logger(logmsg: "Flag H: " + String(regF & UInt8(flagH)))
                logger(logmsg: "Flag P: " + String(regF & UInt8(flagPV)))
                logger(logmsg: "Flag N: " + String(regF & UInt8(flagN)))
                logger(logmsg: "Flag C: " + String(regF & UInt8(flagC)))
                logger(logmsg: "(SP-4) " + String(toHexWord(num: system.fetchWordFrom(adr: (regSP-4)))))
                logger(logmsg: "(SP-2) " + String(toHexWord(num: system.fetchWordFrom(adr: (regSP-2)))))
                logger(logmsg: "(SP)   " + String(toHexWord(num: system.fetchWordFrom(adr: (regSP)))))
                logger(logmsg: "(SP+2) " + String(toHexWord(num: system.fetchWordFrom(adr: (regSP+2)))))
                logger(logmsg: "(SP+4) " + String(toHexWord(num: system.fetchWordFrom(adr: (regSP+4)))))
                logger(logmsg: "(IY+0) 0x5C3A: " + String(toHex(num: system.fetchFrom(adr: (regIY)))))
                logger(logmsg: "(IY+1) 0x5C3B: " + String(toHex(num: system.fetchFrom(adr: (regIY+1)))))
                logger(logmsg: "(IY+2) 0x5C3C: " + String(toHex(num: system.fetchFrom(adr: (regIY+2)))))
                logger(logmsg: "(IY+3) 0x5C3D: " + String(toHex(num: system.fetchFrom(adr: (regIY+3)))))
                logger(logmsg: "(IY+4) 0x5C3E: " + String(toHex(num: system.fetchFrom(adr: (regIY+4)))))
                logger(logmsg: "(IY+0x0A) NSPPC 0x5C44: " + String(toHex(num: system.fetchFrom(adr: (regIY+0x0A)))))
                logger(logmsg: "(IY+0x31) 0x5C6b: " + String(toHex(num: system.fetchFrom(adr: (regIY+0x31)))))
                logger(logmsg: "KSTATE 0x5C00: " + String(toHex(num: system.fetchFrom(adr: 0x5c00))))
                logger(logmsg: "KSTATE 0x5C01: " + String(toHex(num: system.fetchFrom(adr: 0x5c01))))
                logger(logmsg: "KSTATE 0x5C02: " + String(toHex(num: system.fetchFrom(adr: 0x5c02))))
                logger(logmsg: "KSTATE 0x5C03: " + String(toHex(num: system.fetchFrom(adr: 0x5c03))))
                logger(logmsg: "KSTATE 0x5C04: " + String(toHex(num: system.fetchFrom(adr: 0x5c04))))
                logger(logmsg: "KSTATE 0x5C05: " + String(toHex(num: system.fetchFrom(adr: 0x5c05))))
                logger(logmsg: "KSTATE 0x5C06: " + String(toHex(num: system.fetchFrom(adr: 0x5c06))))
                logger(logmsg: "KSTATE 0x5C07: " + String(toHex(num: system.fetchFrom(adr: 0x5c07))))
                logger(logmsg: "LAST-K 0x5C08: " + String(toHex(num: system.fetchFrom(adr: 0x5c08))))
                logger(logmsg: "ERR-NR 0x5C3A: " + String(toHex(num: system.fetchFrom(adr: 0x5c3A))))
                logger(logmsg: "SUBPPC 0x5C47: " + String(toHex(num: system.fetchFrom(adr: 0x5c47))))
                logger(logmsg: "BORDER 0x5C48: " + String(toHex(num: system.fetchFrom(adr: 0x5c48))))
                logger(logmsg: "DATADD 0x5C57: " + String(toHex(num: system.fetchFrom(adr: 0x5c57))))
                logger(logmsg: "DATADD 0x5C58: " + String(toHex(num: system.fetchFrom(adr: 0x5c58))))
                logger(logmsg: "E-LINE 0x5C59: " + String(toHex(num: system.fetchFrom(adr: 0x5c59))))
                logger(logmsg: "E-LINE 0x5C5A: " + String(toHex(num: system.fetchFrom(adr: 0x5c5A))))
                logger(logmsg: "CH-ADD 0x5C5D: " + String(toHex(num: system.fetchFrom(adr: 0x5c5D))))
                logger(logmsg: "CH-ADD 0x5C5E: " + String(toHex(num: system.fetchFrom(adr: 0x5c5E))))
                logger(logmsg: "STKEND 0x5C65: " + String(toHex(num: system.fetchFrom(adr: 0x5c65))))
                logger(logmsg: "STKEND 0x5C66: " + String(toHex(num: system.fetchFrom(adr: 0x5c66))))
                logger(logmsg: "BREG   0x5C67: " + String(toHex(num: system.fetchFrom(adr: 0x5c67))))
                logger(logmsg: "MEM    0x5C68: " + String(toHex(num: system.fetchFrom(adr: 0x5c68))))
                logger(logmsg: "MEM    0x5C69: " + String(toHex(num: system.fetchFrom(adr: 0x5c69))))
                logger(logmsg: "IFF: " + String(IFF))
                logger(logmsg: "IMode: " + String(IMode))
                logger(logmsg: "regPC: " + String(regPC.hex))
                logger(logmsg: "tStatesCounter: " + String(tStatesCounter))
                logger(logmsg: "emulationCounter: " + String(emulationCounter))
                logger(logmsg: "MI: " + String(MI))
                logger(logmsg: "NMI: " + String(NMI))
                logger(logmsg: "Stack 0x5Ca1: " + String(toHex(num: system.fetchFrom(adr: 0x5ca1))))
                logger(logmsg: "Stack 0x5Ca2: " + String(toHex(num: system.fetchFrom(adr: 0x5ca2))))
                logger(logmsg: "Stack 0x5Ca3: " + String(toHex(num: system.fetchFrom(adr: 0x5ca3))))
                logger(logmsg: "Stack 0x5Ca4: " + String(toHex(num: system.fetchFrom(adr: 0x5ca4))))
                logger(logmsg: "Stack 0x5Ca5: " + String(toHex(num: system.fetchFrom(adr: 0x5ca5))))
                logger(logmsg: "Stack 0x5Ca6: " + String(toHex(num: system.fetchFrom(adr: 0x5ca6))))
                logger(logmsg: "Stack 0x5Ca7: " + String(toHex(num: system.fetchFrom(adr: 0x5ca7))))
                logger(logmsg: "Stack 0x5Ca8: " + String(toHex(num: system.fetchFrom(adr: 0x5ca8))))
                logger(logmsg: "Stack 0x5Ca9: " + String(toHex(num: system.fetchFrom(adr: 0x5ca9))))
                logger(logmsg: "Stack 0x5Caa: " + String(toHex(num: system.fetchFrom(adr: 0x5caa))))
                logger(logmsg: "Stack 0x5Cab: " + String(toHex(num: system.fetchFrom(adr: 0x5cab))))
                logger(logmsg: "---")
                logger(logmsg: "Basic 0x5CCA: " + String(toHex(num: system.fetchFrom(adr: 0x5cCA))))
                logger(logmsg: "Basic 0x5CCB: " + String(toHex(num: system.fetchFrom(adr: 0x5cCB))))
                logger(logmsg: "Basic 0x5CCC: " + String(toHex(num: system.fetchFrom(adr: 0x5cCC))))
                logger(logmsg: "Basic 0x5CCD: " + String(toHex(num: system.fetchFrom(adr: 0x5cCD))))
                logger(logmsg: "Basic 0x5CCE: " + String(toHex(num: system.fetchFrom(adr: 0x5cCE))))
                logger(logmsg: "Basic 0x5CCF: " + String(toHex(num: system.fetchFrom(adr: 0x5cCF))))
                logger(logmsg: "---")
                logger(logmsg: "Basic 0x5CD0: " + String(toHex(num: system.fetchFrom(adr: 0x5cD0))))
                logger(logmsg: "Basic 0x5CD1: " + String(toHex(num: system.fetchFrom(adr: 0x5cD1))))
                logger(logmsg: "Basic 0x5CD2: " + String(toHex(num: system.fetchFrom(adr: 0x5cD2))))
                logger(logmsg: "Basic 0x5CD3: " + String(toHex(num: system.fetchFrom(adr: 0x5cD3))))
                logger(logmsg: "Basic 0x5CD4: " + String(toHex(num: system.fetchFrom(adr: 0x5cD4))))
                logger(logmsg: "Basic 0x5CD5: " + String(toHex(num: system.fetchFrom(adr: 0x5cD5))))
                logger(logmsg: "Basic 0x5CD6: " + String(toHex(num: system.fetchFrom(adr: 0x5cD6))))
                logger(logmsg: "Basic 0x5CD7: " + String(toHex(num: system.fetchFrom(adr: 0x5cD7))))
                logger(logmsg: "Basic 0x5CD8: " + String(toHex(num: system.fetchFrom(adr: 0x5cD8))))
                logger(logmsg: "Basic 0x5CD9: " + String(toHex(num: system.fetchFrom(adr: 0x5cD9))))
                logger(logmsg: "Basic 0x5CDA: " + String(toHex(num: system.fetchFrom(adr: 0x5cDA))))
                logger(logmsg: "Basic 0x5CDB: " + String(toHex(num: system.fetchFrom(adr: 0x5cDB))))
                logger(logmsg: "Basic 0x5CDC: " + String(toHex(num: system.fetchFrom(adr: 0x5cDC))))
                logger(logmsg: "Basic 0x5CDD: " + String(toHex(num: system.fetchFrom(adr: 0x5cDD))))
                logger(logmsg: "Basic 0x5CDE: " + String(toHex(num: system.fetchFrom(adr: 0x5cDE))))
                logger(logmsg: "Basic 0x5CDF: " + String(toHex(num: system.fetchFrom(adr: 0x5cDF))))
                logger(logmsg: "---")
                logger(logmsg: "Basic 0x5CE0: " + String(toHex(num: system.fetchFrom(adr: 0x5cE0))))
                logger(logmsg: "Basic 0x5CE1: " + String(toHex(num: system.fetchFrom(adr: 0x5cE1))))
                logger(logmsg: "Basic 0x5CE2: " + String(toHex(num: system.fetchFrom(adr: 0x5CE2))))
                logger(logmsg: "Basic 0x5CE3: " + String(toHex(num: system.fetchFrom(adr: 0x5CE3))))
                logger(logmsg: "Basic 0x5CE4: " + String(toHex(num: system.fetchFrom(adr: 0x5CE4))))
                logger(logmsg: "Basic 0x5CE5: " + String(toHex(num: system.fetchFrom(adr: 0x5CE5))))
                logger(logmsg: "Basic 0x5CE6: " + String(toHex(num: system.fetchFrom(adr: 0x5CE6))))
                logger(logmsg: "Basic 0x5CE7: " + String(toHex(num: system.fetchFrom(adr: 0x5CE7))))
                logger(logmsg: "Basic 0x5CE8: " + String(toHex(num: system.fetchFrom(adr: 0x5CE8))))
                logger(logmsg: "Basic 0x5CE9: " + String(toHex(num: system.fetchFrom(adr: 0x5CE9))))
                logger(logmsg: "Basic 0x5CEA: " + String(toHex(num: system.fetchFrom(adr: 0x5CEA))))
                logger(logmsg: "Basic 0x5CEB: " + String(toHex(num: system.fetchFrom(adr: 0x5CEB))))
                logger(logmsg: "Basic 0x5CEC: " + String(toHex(num: system.fetchFrom(adr: 0x5CEC))))
                logger(logmsg: "Basic 0x5CED: " + String(toHex(num: system.fetchFrom(adr: 0x5CED))))
                logger(logmsg: "Basic 0x5CEE: " + String(toHex(num: system.fetchFrom(adr: 0x5CEE))))
                logger(logmsg: "Basic 0x5CEF: " + String(toHex(num: system.fetchFrom(adr: 0x5CEF))))
                logger(logmsg: "---")
                logger(logmsg: "Basic 0x5CF0: " + String(toHex(num: system.fetchFrom(adr: 0x5cF0))))
                logger(logmsg: "Basic 0x5CF1: " + String(toHex(num: system.fetchFrom(adr: 0x5cF1))))
                logger(logmsg: "Basic 0x5CF2: " + String(toHex(num: system.fetchFrom(adr: 0x5cF2))))
                logger(logmsg: "Basic 0x5CF3: " + String(toHex(num: system.fetchFrom(adr: 0x5cF3))))
                logger(logmsg: "Basic 0x5CF4: " + String(toHex(num: system.fetchFrom(adr: 0x5cF4))))
                logger(logmsg: "Basic 0x5CF5: " + String(toHex(num: system.fetchFrom(adr: 0x5cF5))))
                logger(logmsg: "Basic 0x5CF6: " + String(toHex(num: system.fetchFrom(adr: 0x5cF6))))
                logger(logmsg: "Basic 0x5CF7: " + String(toHex(num: system.fetchFrom(adr: 0x5cF7))))
                logger(logmsg: "Basic 0x5CF8: " + String(toHex(num: system.fetchFrom(adr: 0x5cF8))))
                logger(logmsg: "Basic 0x5CF9: " + String(toHex(num: system.fetchFrom(adr: 0x5cF9))))
                logger(logmsg: "Basic 0x5CFA: " + String(toHex(num: system.fetchFrom(adr: 0x5cFA))))
                logger(logmsg: "Basic 0x5CFB: " + String(toHex(num: system.fetchFrom(adr: 0x5cFB))))
                logger(logmsg: "Basic 0x5CFC: " + String(toHex(num: system.fetchFrom(adr: 0x5cFC))))
                
            }
            
            if (emulationStop) {
                timer.invalidate()
            }
            
            let currentTime2 = getCurrentMillis()
            lastTimeRunEnd = currentTime2
            
        } else { // TEMP TEMP TEMP
//            logger(logmsg: "run() overlap")
        }
    }
    
}
